# Example showing creating a tree containing new data.
# Taken from the BNL jamboree talk.
import sys
sys.path.append ('.')

# First, define a minimal 4-vector class.
from pytuple.treeinfo import treeinfo
from math import *
class Fourvec (object):
    (px, py, pz, E) = 4*[treeinfo.float]
    def __add__ (self, other):
        f = Fourvec ()
        f.px = self.px + other.px
        f.py = self.py + other.py
        f.pz = self.pz + other.pz
        f.E  = self.E  + other.E
        return f
    def get_m (self):
        return sqrt(max(0,self.E**2 - self.px**2 -
                          self.py**2 - self.pz**2))
    m = property (get_m)



# Now set up the input tree, to read |el| objects as |Fourvec| instances.
import ROOT
import PyCintex
from pytuple.readtuple import make_wrapper

file = ROOT.TFile ('d3pd.root') # Put your input file here.
tree = file.Get ('egamma')

tt = make_wrapper (tree)
tt.add_list (Fourvec, 'el', 100)

# Now set up the output tree.
class Pair (Fourvec):
    (i, j) = 2*[treeinfo.int]
    def __init__ (self, sum=None, i=None, j=None):
        if sum == None: return
        self.px = sum.px
        self.py = sum.py
        self.pz = sum.pz
        self.E  = sum.E
        (self.i, self.j) = (i, j)
fnew = ROOT.TFile ('out.root', 'recreate')
tnew = ROOT.TTree ('tnew', 'tnew')
ttnew = make_wrapper (tnew)
ttnew.add_list (Pair, 'pair', 500)

# We'll be writing a new tree of |Pair| objects. 

# Now here's the function to process one event.  It takes as arguments
# the event index and the input tree.
def work (iev, tree):
    eles = tree.el
    for i in range (len (eles)-1):
        for j in range (i+1, len(eles)):
            pair = ttnew.pair.new (eles[i]+eles[j], i, j)
    ttnew.Fill()
    return

# Then run and clean up.
tt.loop (work)
fnew.Write()
fnew.Close()
