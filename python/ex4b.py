# Example showing creating a tree containing new data.
# Taken from the BNL jamboree talk.
#
# Here the new tree is read back along with the old one.

import sys
sys.path.append ('.')

# Define the data classes.
from pytuple.treeinfo import treeinfo
from math import *
class Fourvec (object):
    (px, py, pz, E) = 4*[treeinfo.float]
    def __add__ (self, other):
        f = Fourvec ()
        f.px = self.px + other.px
        f.py = self.py + other.py
        f.pz = self.pz + other.pz
        f.E  = self.E  + other.E
        return f
    def get_m (self):
        return sqrt(max(0,self.E**2 - self.px**2 -
                          self.py**2 - self.pz**2))
    m = property (get_m)

class Pair (Fourvec):
    (i, j) = 2*[treeinfo.int]
    def __init__ (self, sum=None, i=None, j=None):
        if sum == None: return
        self.px = sum.px
        self.py = sum.py
        self.pz = sum.pz
        self.E  = sum.E
        (self.i, self.j) = (i, j)



import ROOT
import PyCintex

# Open the old tree.
file = ROOT.TFile ('d3pd.root')
tree = file.Get ('egamma')

# Open the new one.  Make it a friend.
f2 = ROOT.TFile ('out.root')
t2 = f2.Get ('tnew')

# Set up the tree.
from pytuple.readtuple import make_wrapper
tt = make_wrapper (tree)
tt.add_friend (t2)
tt.add_list (Fourvec, 'el', 100)
tt.add_list (Pair, 'pair', 500)

# Make some plots.
from PyAnalysisUtils.pydraw import cmdhook, cmd
from PyAnalysisUtils.draw_obj import *
zone(1,2)
cmd ("d tt.pair$i.m")
cmd ("d tt.pair$i.m:el$1.E")
