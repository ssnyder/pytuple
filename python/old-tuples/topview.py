from pytuple.readtuple import make_wrapper
from pytuple.treeinfo import treeinfo
from pytuple import samplings
from pytuple.Fourvec import *
import string


class Particle (Fourvec_All):
    charge = treeinfo.float
    pdgId = treeinfo.float



class Matched (object):
    Matched = treeinfo.int
    DeltaR  = treeinfo.float


class MatchedParticle (Matched, Particle):
    pass



class egamma (Particle):
    emWeight = treeinfo.float
    pionWeight = treeinfo.float
    etaBE2 = treeinfo.float
    et37 = treeinfo.float
    etcone = treeinfo.float
    etcone30 = treeinfo.float
    author = treeinfo.int
    isEM = treeinfo.int
    
    # EMShower: Sampling 1 variables
    # E1 / E
    f1 = treeinfo.float
    # Energy of the cell at second energy maximum
    e2tsts1 = treeinfo.float
    # Energy in strip at minimum between first and second maxima
    emins1 = treeinfo.float
    # Shower width using three strips around maximal strip
    weta1 = treeinfo.float
    # Shower width in 0.0625X0.2 around maximal strip
    wtots1 = treeinfo.float
    # (E(+-3) - E(+-1)) / E(+-1), around maximal strip
    fracs1 = treeinfo.float

    # EMShower: Sampling 2 variables
    # Uncalibrated energy sum in 3X3 window.
    e233 = treeinfo.float
    # Uncalibrated energy sum in 3X7 window.
    e237 = treeinfo.float
    # Uncalibrated energy sum in 7X7 window.
    e277 = treeinfo.float
    # Lateral width in 3X5 window, with correction for particle impact point
    weta2 = treeinfo.float

    # EMShower: Hadronic variables
    # Transverse energy in the first had sampling
    ethad1 = treeinfo.float

    # EMShower: Combined information
    # Transverse isolation energy in cone with half-opening angle 0.2
    etcone20 = treeinfo.float
    # Transverse isolation energy in cone with half-opening angle 0.4
    etcone40 = treeinfo.float

    class L1cls (Matched, Fourvec_PtEtaPhiE):
        EmCore = treeinfo.float
        HdCore = treeinfo.float
        EmClus = treeinfo.float
        TauClus = treeinfo.float
        EmIsol = treeinfo.float
        HdIsol = treeinfo.float
    L1 = treeinfo.instance (L1cls)

    class L1_ROIcls (Matched):
        ThrPat  = treeinfo.int
        RoIWord = treeinfo.int
    L1_ROI = treeinfo.instance (L1_ROIcls)

    L2 = treeinfo.instance (Matched)
    EF = treeinfo.instance (Matched)

    class Trucls (MatchedParticle):
        etcone = treeinfo.float
        etcone10 = treeinfo.float
        etcone20 = treeinfo.float
        etcone30 = treeinfo.float
        etcone40 = treeinfo.float
        etcone50 = treeinfo.float
        etcone60 = treeinfo.float
        etcone70 = treeinfo.float
        status = treeinfo.float
        barcode = treeinfo.int
    Tru = treeinfo.instance (Trucls)



class Ele (egamma):
    elecNN = treeinfo.float
    softeElecNN = treeinfo.float
    softeEmWeight = treeinfo.float
    softePionWeight = treeinfo.float
    NBLayerHits = treeinfo.float
    NPixelHits = treeinfo.float
    NSCTHits = treeinfo.float
    NTRTHits = treeinfo.float
    NHighThresTRTHits = treeinfo.float
    softeIsEM = treeinfo.int
    hasTrack = treeinfo.int
    Tight = treeinfo.int
    SoftLepOverlap = treeinfo.int
    GoodLepton = treeinfo.int
    MuOverlap = treeinfo.int
    TransM = treeinfo.float

    # EMShower: Sampling 1 variables
    # E1(3X1) / E
    f1core = treeinfo.float

    # EMShower: Sampling 2 variables
    # Same as weta2, but without impact point correction
    widths2 = treeinfo.float

    # EMShower: Sampling 3 variables
    # E3(3X3) / E
    f3core = treeinfo.float

    # EMShower: Combined information
    # Difference between track and shower posn, in interstrip-distance units
    pos7 = treeinfo.float
    # E(3X3) / E(3X7)
    iso = treeinfo.float
    # zvertex reconstructed from pointing
    #zvertex
    # associated error
    #errz
    # eta reconstructed from pointing
    #etap
    # shower depth
    #depth

    # EMTrackMatch
    # Eta of track extrapolated to first sampling
    EtaCorrMag = treeinfo.float
    # Cluster energy / track momentum
    EoverP = treeinfo.float
    # Difference between cluster eta (sampl 1) & track extrapolated to sampl 1
    #deltaEta1
    # Difference between cluster eta (sampl 2) & track extrapolated to sampl 2
    #deltaEta2
    # Difference between cluster phi (sampl 2) & track extrapolated to sampl 2
    #deltaPhi2

    # EMConvert
    conversionTrackMatch = treeinfo.int
    conversionAngleMatch = treeinfo.int

    # EMBremFit
    # bremInvpT
    # bremRadius
    # bremX
    # bremClusterRadius
    # bremInvpTerr

    TruJet = treeinfo.instance (Matched)
    TruTau = treeinfo.instance (Matched)



class Pho (egamma):
    TrkIsolated = treeinfo.int


class Muon (Particle):
    etcone = treeinfo.float
    etcone10 = treeinfo.float
    etcone20 = treeinfo.float
    etcone30 = treeinfo.float
    etcone40 = treeinfo.float
    etcone50 = treeinfo.float
    etcone60 = treeinfo.float
    etcone70 = treeinfo.float
    nucone = treeinfo.float
    nucone10 = treeinfo.float
    nucone20 = treeinfo.float
    nucone30 = treeinfo.float
    nucone40 = treeinfo.float
    nucone50 = treeinfo.float
    nucone60 = treeinfo.float
    nucone70 = treeinfo.float
    matchChi2 = treeinfo.float
    matchChi2OverDoF = treeinfo.float
    fitChi2 = treeinfo.float
    fitChi2OverDoF = treeinfo.float
    TransM = treeinfo.float
    numberOfBLayerHits = treeinfo.int
    numberOfPixelHits = treeinfo.int
    numberOfSCTHits = treeinfo.int
    numberOfTRTHits = treeinfo.int
    numberOfTRTHighTresholdHits = treeinfo.int
    bestMatch = treeinfo.int
    isCombinedMuon = treeinfo.int
    hasInDetTrackParticle = treeinfo.int
    hasMuonSpectrometerTrackParticle = treeinfo.int
    hasMuonExtrapolatedTrackParticle = treeinfo.int
    hasCombinedMuonTrackParticle = treeinfo.int
    numberOfMDTHits = treeinfo.int
    numberOfCSCEtaHits = treeinfo.int
    numberOfCSCPhiHits = treeinfo.int
    numberOfRPCEtaHits = treeinfo.int
    numberOfRPCPhiHits = treeinfo.int
    numberOfTGCEtaHits = treeinfo.int
    numberOfTGCPhiHits = treeinfo.int
    author = treeinfo.int
    hasCluster = treeinfo.int
    hasCombinedMuon = treeinfo.int

    SoftLepOverlap = treeinfo.int
    GoodLepton = treeinfo.int
    MuonInJet = treeinfo.int

    # Should be moved to an instance...
    class L1cls (MatchedParticle):
        ThrNum = treeinfo.int
        RoIWord = treeinfo.int
    L1 = treeinfo.instance (L1cls)

    L2 = treeinfo.instance (Matched)
    EF = treeinfo.instance (Matched)

    class Trucls (Particle, Matched):
        etcone = treeinfo.float
        etcone10 = treeinfo.float
        etcone20 = treeinfo.float
        etcone30 = treeinfo.float
        etcone40 = treeinfo.float
        etcone50 = treeinfo.float
        etcone60 = treeinfo.float
        etcone70 = treeinfo.float
        status = treeinfo.float
        barcode = treeinfo.int
    Tru = treeinfo.instance (Trucls)


class Tau (Particle):
    logLikelihoodRatio = treeinfo.float
    lowPtTauJetDiscriminant = treeinfo.float
    lowPtTauEleDiscriminant = treeinfo.float
    tauJetNeuralnetwork = treeinfo.float
    tauENeuralNetwork = treeinfo.float ("%s_tauENeuralNetwork ")
    discriminant = treeinfo.float
    discNN = treeinfo.float
    discPDERS = treeinfo.float

    etHadCalib = treeinfo.float
    etEMCalib = treeinfo.float
    emRadius = treeinfo.float
    isolationFraction = treeinfo.float
    centralityFraction = treeinfo.float
    stripWidth2 = treeinfo.float
    nStripCells = treeinfo.float
    annularIsolationFraction = treeinfo.float
    etCaloAtEMScale = treeinfo.float
    etChargedHadCells = treeinfo.float
    etOtherEMCells = treeinfo.float
    etOtherHadCells = treeinfo.float
    totTrkCharge = treeinfo.float
    totTrkE = treeinfo.float
    sumPTtracks = treeinfo.float

    rWidth2Trk3P = treeinfo.float
    massTrk3P = treeinfo.float
    signDOTrk3P = treeinfo.float
    nAssocTracksCore = treeinfo.float
    nAssocTracksIsol = treeinfo.float
    numTrack = treeinfo.int
    author = treeinfo.int

    # Should be an instance...
    EMPx = treeinfo.float
    EMPy = treeinfo.float
    EMPz = treeinfo.float
    EME = treeinfo.float

    class L1cls (Matched, Fourvec_PtEtaPhiE):
        EmCore = treeinfo.float
        HdCore = treeinfo.float
        EmClus = treeinfo.float
        TauClus = treeinfo.float
        EmIsol = treeinfo.float
        HdIsol = treeinfo.float
    L1 = treeinfo.instance (L1cls)

    class L1_ROIcls (Matched):
        ThrPat  = treeinfo.int
        RoIWord = treeinfo.int
    L1_ROI = treeinfo.instance (L1_ROIcls)

    L2 = treeinfo.instance (Matched)
    EF = treeinfo.instance (Matched)

    class Trucls (MatchedParticle):
        tauEta = treeinfo.float
        tauPhi = treeinfo.float
        tauE = treeinfo.float
        tauET = treeinfo.float
        tauPT = treeinfo.float
        tauPx = treeinfo.float
        tauPy = treeinfo.float
        tauPz = treeinfo.float
        stablesEAll = treeinfo.float
        stablesEAllCharged = treeinfo.float
        stablesEAllPion = treeinfo.float
        stablesEAllKaon = treeinfo.float
        stablesEAllOtherCh = treeinfo.float
        stablesEAllNeut = treeinfo.float
        stablesEAllPiZ = treeinfo.float
        stablesEAllGamma = treeinfo.float
        stablesEAllOtherNeut = treeinfo.float
        stablesETAll = treeinfo.float
        stablesETAllCharged = treeinfo.float
        stablesETAllPion = treeinfo.float
        stablesETAllKaon = treeinfo.float
        stablesETAllOtherCh = treeinfo.float
        stablesETAllNeut = treeinfo.float
        stablesETAllPiZ = treeinfo.float
        stablesETAllGamma = treeinfo.float
        stablesETAllOtherNeut = treeinfo.float
        stablesPTAll = treeinfo.float
        stablesPTAllCharged = treeinfo.float
        stablesPTAllPion = treeinfo.float
        stablesPTAllKaon = treeinfo.float
        stablesPTAllOtherCh = treeinfo.float
        stablesPTAllNeut = treeinfo.float
        stablesPTAllPiZ = treeinfo.float
        stablesPTAllGamma = treeinfo.float
        stablesPTAllOtherNeut = treeinfo.float

        compPartIndex = treeinfo.int
        test_FailHasCharge = treeinfo.int
        stablesN = treeinfo.int
        nDaughters = treeinfo.int
        nStable = treeinfo.int
        nStableCharged = treeinfo.int
        nStablePion = treeinfo.int
        nStableKaon = treeinfo.int
        nStableOtherCh = treeinfo.int
        nStableNeut = treeinfo.int
        nStablePiZ = treeinfo.int
        nStableGamma = treeinfo.int
        nStableOtherNeut = treeinfo.int
        nStableCounted = treeinfo.int
        nStableNoneMissing = treeinfo.int
    Tru = treeinfo.instance (Trucls)


class PJet (Particle):
    EinPreSampB = treeinfo.float
    EinPreSampE = treeinfo.float
    EinEMB1 = treeinfo.float
    EinEMB2 = treeinfo.float
    EinEMB3 = treeinfo.float
    EinEME1 = treeinfo.float
    EinEME2 = treeinfo.float
    EinEME3 = treeinfo.float
    EinHEC0 = treeinfo.float
    EinHEC1 = treeinfo.float
    EinHEC2 = treeinfo.float
    EinHEC3 = treeinfo.float
    EinTileBar0 = treeinfo.float
    EinTileBar1 = treeinfo.float
    EinTileBar2 = treeinfo.float
    EinTileGap1 = treeinfo.float
    EinTileGap2 = treeinfo.float
    EinTileGap3 = treeinfo.float
    EinTileExt0 = treeinfo.float
    EinTileExt1 = treeinfo.float
    EinTileExt2 = treeinfo.float
    EinFCAL0 = treeinfo.float
    EinFCAL1 = treeinfo.float
    EinFCAL2 = treeinfo.float
    EinUnknown = treeinfo.float
    EinCryostat = treeinfo.float

    def esamp (self, samp):
        if samp == samplings.PreSamplerB:
            return self.EinPreSampB
        if samp == samplings.PreSamplerE:
            return self.EinPreSampE
        sname = samplings.samplings[samp]
        return getattr (self, "Ein" + sname)


    weight = treeinfo.float
    IP1D = treeinfo.float
    IP2D = treeinfo.float
    IP3D = treeinfo.float
    SV1 = treeinfo.float
    SecVtxTagBU = treeinfo.float
    SV2 = treeinfo.float
    DRTruB = treeinfo.float
    DRTruC = treeinfo.float
    DRTruT = treeinfo.float
    MCCalibFn = treeinfo.float
    MCCalib = treeinfo.float
    MCCalibError = treeinfo.float

    TRFIP2DWp3 = treeinfo.float
    TRFSV1Wp3 = treeinfo.float
    TRFIP2DWp3_Plus = treeinfo.float
    TRFSV1Wp3_Plus = treeinfo.float
    TRFIP2DWp3_Minus = treeinfo.float
    TRFSV1Wp3_Minus = treeinfo.float
    cosThetaStar = treeinfo.float

    nTracks = treeinfo.int
    TruthLabel = treeinfo.int
    TRFIP2DWp3_1incl = treeinfo.int
    TRFIP2DWp3_1excl = treeinfo.int
    TRFIP2DWp3_2incl = treeinfo.int
    TRFIP2DWp3_2excl = treeinfo.int
    TRFSV1Wp3_1incl = treeinfo.int
    TRFSV1Wp3_1excl = treeinfo.int
    TRFSV1Wp3_2incl = treeinfo.int
    TRFSV1Wp3_2excl = treeinfo.int
    TRFIP2DWp3_Plus_1incl = treeinfo.int
    TRFIP2DWp3_Plus_1excl = treeinfo.int
    TRFIP2DWp3_Plus_2incl = treeinfo.int
    TRFIP2DWp3_Plus_2excl = treeinfo.int
    TRFSV1Wp3_Plus_1incl = treeinfo.int
    TRFSV1Wp3_Plus_1excl = treeinfo.int
    TRFSV1Wp3_Plus_2incl = treeinfo.int
    TRFSV1Wp3_Plus_2excl = treeinfo.int
    TRFIP2DWp3_Minus_1incl = treeinfo.int
    TRFIP2DWp3_Minus_1excl = treeinfo.int
    TRFIP2DWp3_Minus_2incl = treeinfo.int
    TRFIP2DWp3_Minus_2excl = treeinfo.int
    TRFSV1Wp3_Minus_1incl = treeinfo.int
    TRFSV1Wp3_Minus_1excl = treeinfo.int
    TRFSV1Wp3_Minus_2incl = treeinfo.int
    TRFSV1Wp3_Minus_2excl = treeinfo.int

    FlatBTagged = treeinfo.int
    FlatBTaggedPlus = treeinfo.int
    FlatBTaggedMinus = treeinfo.int
    BTagged = treeinfo.int
    BTaggedPlus = treeinfo.int
    BTaggedMinus = treeinfo.int
    NonLooseJet = treeinfo.int
    GoodJet = treeinfo.int
    PhOverlap = treeinfo.int
    TauOverlap = treeinfo.int
    MuOverlap = treeinfo.int

    # Should be an instance?
    SecVtx_x = treeinfo.float
    SecVtx_y = treeinfo.float
    SecVtx_z = treeinfo.float
    SecVtx_x_SD = treeinfo.float
    SecVtx_y_SD = treeinfo.float
    SecVtx_z_SD = treeinfo.float
    SecVtx_Chi2 = treeinfo.float
    SecVtx_FitNDF = treeinfo.float

    class Softcls (Particle):
        Matched  = treeinfo.int
        ptRel    = treeinfo.float
        d0       = treeinfo.float
        lhSig    = treeinfo.float
        pairMass = treeinfo.float

        def _subname (b, l, k):
            if b[-1] == 'e':
                kk = 'Electron'
            else:
                kk = 'Muon'
            i = b.rindex ('_')
            b = b[:i]
            return "%s_Soft%s%s" % (b, kk, k)
        numTrack = treeinfo.int (_subname, 'NumTrack')
        weight = treeinfo.int (_subname, 'Weight')


    Softe = treeinfo.instance (Softcls)
    Softmu = treeinfo.instance (Softcls)

    class L1cls (Matched, Fourvec_PtEtaPhiE):
        Et4x4 = treeinfo.float
        Et6x6 = treeinfo.float
        Et8x8 = treeinfo.float
    L1 = treeinfo.instance (L1cls)

    class L1_ROIcls (Matched):
        ThrPat  = treeinfo.int
        RoIWord = treeinfo.int
    L1_ROI = treeinfo.instance (L1_ROIcls)

    L2 = treeinfo.instance (Matched)
    EF = treeinfo.instance (Matched)

    class Trucls (MatchedParticle):
        etcone = treeinfo.float
        etcone10 = treeinfo.float
        etcone20 = treeinfo.float
        etcone30 = treeinfo.float
        etcone40 = treeinfo.float
        etcone50 = treeinfo.float
        etcone60 = treeinfo.float
        etcone70 = treeinfo.float
        status = treeinfo.float
        barcode = treeinfo.int
    Tru = treeinfo.instance (Trucls)

    TruJet = treeinfo.instance (MatchedParticle)



# Should derive from a fourvec class?
class MET (object):
    ex = treeinfo.double
    ey = treeinfo.double
    et = treeinfo.double
    sumet = treeinfo.double


class PassedL1 (object):
    any = treeinfo.int ("%s")

    _2EM15 = treeinfo.int ('%s_2EM15')
    _2EM15I = treeinfo.int ('%s_2EM15I')
    _2J45 = treeinfo.int ('%s_2J45')
    _2MU06 = treeinfo.int ('%s_2MU06')
    _3J45 = treeinfo.int ('%s_3J45')
    _4J45 = treeinfo.int ('%s_4J45')
    BJT15 = treeinfo.int
    EM25 = treeinfo.int
    EM25I = treeinfo.int
    EM5 = treeinfo.int
    EM60 = treeinfo.int
    FJ30 = treeinfo.int
    J170 = treeinfo.int
    J300 = treeinfo.int
    J35 = treeinfo.int
    J45 = treeinfo.int
    J60 = treeinfo.int
    J80 = treeinfo.int
    MU06 = treeinfo.int
    MU08 = treeinfo.int
    MU10 = treeinfo.int
    MU11 = treeinfo.int
    MU20 = treeinfo.int
    MU40 = treeinfo.int
    TAU05 = treeinfo.int
    TAU10 = treeinfo.int
    TAU10I = treeinfo.int
    TAU15 = treeinfo.int
    TAU15I = treeinfo.int
    TAU20I = treeinfo.int
    TAU25I = treeinfo.int
    TAU35I = treeinfo.int
    XE100 = treeinfo.int
    XE20 = treeinfo.int
    XE200 = treeinfo.int
    XE30 = treeinfo.int
    XE40 = treeinfo.int
    XE50 = treeinfo.int



class HLTTriggers (object):
    any = treeinfo.int ("%s")

    b35 = treeinfo.int
    e10 = treeinfo.int
    e10TRTxK = treeinfo.int
    e25i = treeinfo.int
    e60 = treeinfo.int
    fljet10 = treeinfo.int
    frjet10 = treeinfo.int
    g10 = treeinfo.int
    g60 = treeinfo.int
    jet160 = treeinfo.int
    jet20kt = treeinfo.int
    mu20i = treeinfo.int
    mu6 = treeinfo.int
    mu6l = treeinfo.int
    tau10 = treeinfo.int
    tau10i = treeinfo.int
    tau15 = treeinfo.int
    tau15i = treeinfo.int
    tau20i = treeinfo.int
    tau25i = treeinfo.int
    tau35i = treeinfo.int
    tauNoCut = treeinfo.int


class PassedL2 (HLTTriggers):
    Ze10e10 = treeinfo.int
    e10L2_e10 = treeinfo.int
    e15iL2_e15i = treeinfo.int
    jet120L2_jet120 = treeinfo.int
    jet20a = treeinfo.int
    jet20bL2_jet20b = treeinfo.int
    jet20cL2_jet20cL2_jet20c = treeinfo.int
    jet20dL2_jet20dL2_jet20dL2_jet20d = treeinfo.int
    jet50L2_jet50L2_jet50L2_jet50 = treeinfo.int
    jet65L2_jet65L2_jet65 = treeinfo.int
    met10f = treeinfo.int
    g20iL2_g20i = treeinfo.int


class PassedEF (HLTTriggers):
    MuonTRTExt_mu6l = treeinfo.int
    e15iEF_e15i = treeinfo.int
    jet120EF_jet120 = treeinfo.int
    jet20aEt = treeinfo.int
    jet20bEtEF_jet20bEt = treeinfo.int
    jet20cEtEF_jet20cEtEF_jet20cEt = treeinfo.int
    jet20dEtEF_jet20dEtEF_jet20dEtEF_jet20dEt = treeinfo.int
    jet50EF_jet50EF_jet50EF_jet50 = treeinfo.int
    jet65EF_jet65EF_jet65 = treeinfo.int
    met10 = treeinfo.int
    g20iEF_g20i = treeinfo.int


class EventWeight (object):
    MCatNLO = treeinfo.double ("%sMCatNLO")
    IP2DWp3_1incl = treeinfo.double ("%sIP2DWp3_1incl")
    IP2DWp3_1excl = treeinfo.double ("%sIP2DWp3_1excl")
    IP2DWp3_2incl = treeinfo.double ("%sIP2DWp3_2incl")
    IP2DWp3_2excl = treeinfo.double ("%sIP2DWp3_2excl")
    SV1Wp3_1incl = treeinfo.double ("%sSV1Wp3_1incl")
    SV1Wp3_1excl = treeinfo.double ("%sSV1Wp3_1excl")
    SV1Wp3_2incl = treeinfo.double ("%sSV1Wp3_2incl")
    SV1Wp3_2excl = treeinfo.double ("%sSV1Wp3_2excl")
    IP2DWp3_1incl_Plus = treeinfo.double ("%sIP2DWp3_1incl_Plus")
    IP2DWp3_1excl_Plus = treeinfo.double ("%sIP2DWp3_1excl_Plus")
    IP2DWp3_2incl_Plus = treeinfo.double ("%sIP2DWp3_2incl_Plus")
    IP2DWp3_2excl_Plus = treeinfo.double ("%sIP2DWp3_2excl_Plus")
    SV1Wp3_1incl_Plus = treeinfo.double ("%sSV1Wp3_1incl_Plus")
    SV1Wp3_1excl_Plus = treeinfo.double ("%sSV1Wp3_1excl_Plus")
    SV1Wp3_2incl_Plus = treeinfo.double ("%sSV1Wp3_2incl_Plus")
    SV1Wp3_2excl_Plus = treeinfo.double ("%sSV1Wp3_2excl_Plus")
    IP2DWp3_1incl_Minus = treeinfo.double ("%sIP2DWp3_1incl_Minus")
    IP2DWp3_1excl_Minus = treeinfo.double ("%sIP2DWp3_1excl_Minus")
    IP2DWp3_2incl_Minus = treeinfo.double ("%sIP2DWp3_2incl_Minus")
    IP2DWp3_2excl_Minus = treeinfo.double ("%sIP2DWp3_2excl_Minus")
    SV1Wp3_1incl_Minus = treeinfo.double ("%sSV1Wp3_1incl_Minus")
    SV1Wp3_1excl_Minus = treeinfo.double ("%sSV1Wp3_1excl_Minus")
    SV1Wp3_2incl_Minus = treeinfo.double ("%sSV1Wp3_2incl_Minus")
    SV1Wp3_2excl_Minus = treeinfo.double ("%sSV1Wp3_2excl_Minus")


class Global (object):
    weight = treeinfo.double ('eventWeight')
    MTotal = treeinfo.double
    HT = treeinfo.double
    spher = treeinfo.double ('Event_Sphericity')
    aplan = treeinfo.double ('Event_Aplanarity')
    plan = treeinfo.double ('Event_Planarity')
    runNumber = treeinfo.int
    eventNumber = treeinfo.int
    N = treeinfo.int
    GoodLepton_N = treeinfo.int
    GoodJet_N = treeinfo.int
    NonLooseJet_N = treeinfo.int
    ForwardJet_N = treeinfo.int
    PassedTrigger = treeinfo.int


class FullReco (object):
    FullRecoInstance = treeinfo.int
    FullRecoCounter = treeinfo.int
    FullRecoEventAndInstance = treeinfo.int
    FullRecoRunNumber = treeinfo.int
    FullRecoNInstance = treeinfo.int


# Merge with met?
class L1ET (object):
    Ex = treeinfo.float
    Ey = treeinfo.float
    Etsum = treeinfo.float
    Etmiss = treeinfo.float


# Merge with MET somehow?
class METTop (object):
    MissingEx = treeinfo.double
    MissingEy = treeinfo.double
    MissingEt = treeinfo.double
    SumEt = treeinfo.double



def label_maker (prefix, label):
    ll = string.split (label, '.')
    if len (ll) == 1:
        def sel (tree, prefix=prefix, label=ll[0]):
            return [p for p in getattr(tree, prefix) if getattr(p, label)]
    elif len(ll) == 2:
        def sel (tree, prefix=prefix, label1=ll[0], label2=ll[1]):
            return [p for p in getattr(tree, prefix)
                    if getattr (getattr(p, label1), label2)]
    return sel


def wrap_FullReco0 (tree):
    tt = make_wrapper (tree)
    tt.add_list (Ele, 'El', 10, count = 'N')
    tt.add_transient ('ElTight', label_maker ('El', 'Tight'))
    tt.add_transient ('ElGood', label_maker ('El', 'GoodLepton'))
    tt.add_transient ('ElSoftOverlap', label_maker ('El', 'SoftLepOverlap'))
    tt.add_transient ('ElMuOverlap', label_maker ('El', 'MuOverlap'))
    tt.add_transient ('ElL1', label_maker ('El', 'L1.Matched'))
    tt.add_transient ('ElL1ROI', label_maker ('El', 'L1_ROI.Matched'))
    tt.add_transient ('ElL2', label_maker ('El', 'L2.Matched'))
    tt.add_transient ('ElEF', label_maker ('El', 'EF.Matched'))
    tt.add_transient ('ElTru', label_maker ('El', 'Tru.Matched'))
    tt.add_transient ('ElTruJet', label_maker ('El', 'TruJet.Matched'))
    tt.add_transient ('ElTruTau', label_maker ('El', 'TruTau.Matched'))

    tt.add_list (Pho, 'Ph', 10, count = 'N')
    tt.add_transient ('PhTrkIso', label_maker ('Ph', 'TrkIsolated'))
    tt.add_transient ('PhL1', label_maker ('Ph', 'L1.Matched'))
    tt.add_transient ('PhL1ROI', label_maker ('Ph', 'L1_ROI.Matched'))
    tt.add_transient ('PhL2', label_maker ('Ph', 'L2.Matched'))
    tt.add_transient ('PhEF', label_maker ('Ph', 'EF.Matched'))
    tt.add_transient ('PhTru', label_maker ('Ph', 'Tru.Matched'))

    tt.add_list (Muon, 'Mu', 10, count = 'N')
    tt.add_transient ('MuGood', label_maker ('Mu', 'GoodLepton'))
    tt.add_transient ('MuSoftOverlap', label_maker ('Mu', 'SoftLepOverlap'))
    tt.add_transient ('MuInJet', label_maker ('Mu', 'MuonInJet'))
    tt.add_transient ('MuL1', label_maker ('Mu', 'L1.Matched'))
    tt.add_transient ('MuL2', label_maker ('Mu', 'L2.Matched'))
    tt.add_transient ('MuEF', label_maker ('Mu', 'EF.Matched'))
    tt.add_transient ('MuTru', label_maker ('Mu', 'Tru.Matched'))

    tt.add_list (Tau, 'Tau', 10, count = 'N')
    tt.add_transient ('TauL1', label_maker ('Tau', 'L1.Matched'))
    tt.add_transient ('TauL1ROI', label_maker ('Tau', 'L1.ROI_Matched'))
    tt.add_transient ('TauL2', label_maker ('Tau', 'L2.Matched'))
    tt.add_transient ('TauEF', label_maker ('Tau', 'EF.Matched'))
    tt.add_transient ('TauTru', label_maker ('Tau', 'Tru.Matched'))

    tt.add_list (PJet, 'PJet', 20, count = 'N')
    for kern in ['IP2D', 'SV1']:
        for sgn in ['', '_Plus', '_Minus']:
            for n in ['1', '2']:
                for ex in ['incl', 'excl']:
                    word = "TRF%sWp3%s_%s%s" % (kern, sgn, n, ex)
                    tt.add_transient ("PJet" + word, label_maker ("PJet", word))
    tt.add_transient ('PJetFlatBTagged', label_maker ('PJet', 'FlatBTagged'))
    tt.add_transient ('PJetFlatBTaggedPlus', label_maker ('PJet', 'FlatBTaggedPlus'))
    tt.add_transient ('PJetFlatBTaggedMinus', label_maker ('PJet', 'FlatBTaggedMinus'))
    tt.add_transient ('PJetBTagged', label_maker ('PJet', 'BTagged'))
    tt.add_transient ('PJetBTaggedPlus', label_maker ('PJet', 'BTaggedPlus'))
    tt.add_transient ('PJetBTaggedMinus', label_maker ('PJet', 'BTaggedMinus'))
    tt.add_transient ('PJetNonLoose', label_maker ('PJet', 'NonLooseJet'))
    tt.add_transient ('PJetGood', label_maker ('PJet', 'GoodJet'))
    tt.add_transient ('PJetPhOverlap', label_maker ('PJet', 'PhOverlap'))
    tt.add_transient ('PJetTauOverlap', label_maker ('PJet', 'TauOverlap'))
    tt.add_transient ('PJetMuOverlap', label_maker ('PJet', 'MuOverlap'))
    tt.add_transient ('PJetSofte', label_maker ('PJet', 'Softe.Matched'))
    tt.add_transient ('PJetSoftmu', label_maker ('PJet', 'Softmu.Matched'))
    tt.add_transient ('PJetL1', label_maker ('PJet', 'L1.Matched'))
    tt.add_transient ('PJetL1ROI', label_maker ('PJet', 'L1_ROI.Matched'))
    tt.add_transient ('PJetL2', label_maker ('PJet', 'L2.Matched'))
    tt.add_transient ('PJetEF', label_maker ('PJet', 'EF.Matched'))
    tt.add_transient ('PJetTru', label_maker ('PJet', 'Tru.Matched'))
    tt.add_transient ('PJetTruJet', label_maker ('PJet', 'TruJet.Matched'))

    tt.add_list (Particle, 'VectSumAll', 10, count = 'N')

    tt.add (MET, 'MET_Final')
    tt.add (MET, 'MET_RefFinal')
    tt.add (MET, 'ObjMET_Final')
    tt.add (MET, 'MET_Calib')
    tt.add (MET, 'MET_Muon')
    tt.add (MET, 'MET_MuonBoy')
    tt.add (MET, 'MET_Cryo')
    tt.add (MET, 'MET_CryoCone')
    tt.add (MET, 'MET_Topo')
    tt.add (MET, 'MET_CellOut')
    tt.add (MET, 'MET_RefEle')
    tt.add (MET, 'MET_RefJet')
    tt.add (MET, 'TrigEFMissingET')

    tt.add (PassedL1, 'PassedL1')
    tt.add (PassedL2, 'PassedL2')
    tt.add (PassedEF, 'PassedEF')

    tt.add (EventWeight, 'eventWeight')
    tt.add (L1ET, 'L1ET')

    tt.add (Global)
    tt.add (FullReco)
    tt.add (METTop)

    return tt
