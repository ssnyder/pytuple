import ROOT
from pytuple.readtuple import make_wrapper
from pytuple.treeinfo import treeinfo
from pytuple import samplings
from pytuple.Fourvec import *
import string
import math


class Selarg:
    pass


def seltru (selarg):
    return selarg.have_truth


class Particle (Fourvec_All):
    charge = treeinfo.float
    pdgId = treeinfo.float



class Matched (object):
    Matched = treeinfo.int
    DeltaR  = treeinfo.float


class MatchedParticle (Matched, Particle):
    pass


class MatchedFourvec (Matched, Fourvec_PtEtaPhiE):
    pass



class egamma (Particle):
    author = treeinfo.int
    IsEM = treeinfo.int
    ElectronWeight = treeinfo.float
    BgWeight       = treeinfo.float
    NeuralNet      = treeinfo.float
    Hmatrix        = treeinfo.float
    Hmatrix5       = treeinfo.float
    SofteElectronWeight = treeinfo.float
    SofteBgWeight       = treeinfo.float
    SofteNeuralNet      = treeinfo.float
    IsolationLikelihood = treeinfo.float
    AdaBoost            = treeinfo.float
    LastEgammaPID       = treeinfo.float
    SofteIsEM = treeinfo.int
    
    # EMShower: Sampling 1 variables
    # E1 / E
    f1 = treeinfo.float
    # Energy of the cell at second energy maximum
    e2tsts1 = treeinfo.float
    # Energy in strip at minimum between first and second maxima
    emins1 = treeinfo.float
    # Shower width using three strips around maximal strip
    weta1 = treeinfo.float
    # Shower width in 0.0625X0.2 around maximal strip
    wtots1 = treeinfo.float
    # (E(+-3) - E(+-1)) / E(+-1), around maximal strip
    fracs1 = treeinfo.float
    # E1(3X1) / E
    f1core = treeinfo.float

    # EMShower: Sampling 2 variables
    # Uncalibrated energy sum in 3X3 window.
    e233 = treeinfo.float
    # Uncalibrated energy sum in 3X7 window.
    e237 = treeinfo.float
    # Uncalibrated energy sum in 7X7 window.
    e277 = treeinfo.float
    # Lateral width in 3X5 window, with correction for particle impact point
    weta2 = treeinfo.float
    # Same as weta2, but without impact point correction
    widths2 = treeinfo.float

    # EMShower: Sampling 3 variables
    # E3(3X3) / E
    f3core = treeinfo.float

    # EMShower: Hadronic variables
    # Transverse energy in the first had sampling
    ethad1 = treeinfo.float

    # EMShower: Combined information
    # Transverse isolation energy in cone with half-opening angle 0.2
    etcone20 = treeinfo.float
    # Difference between track and shower posn, in interstrip-distance units
    pos7 = treeinfo.float
    # E(3X3) / E(3X7)
    #iso = treeinfo.float
    # zvertex reconstructed from pointing
    zvertex = treeinfo.float
    # associated error
    errz = treeinfo.float
    # eta reconstructed from pointing
    etap = treeinfo.float
    # shower depth
    depth = treeinfo.float

    clusterTime = treeinfo.float
    ethad = treeinfo.float
    r33over37allcalo = treeinfo.float

    class Trucls (MatchedParticle):
        etcone = treeinfo.float
        etcone10 = treeinfo.float
        etcone20 = treeinfo.float
        etcone30 = treeinfo.float
        etcone40 = treeinfo.float
        etcone50 = treeinfo.float
        etcone60 = treeinfo.float
        etcone70 = treeinfo.float
        status =treeinfo.float
        barcode = treeinfo.int
    Tru = treeinfo.instance (Trucls, selfcn = seltru)




class Ele (egamma):
    # EMTrackMatch
    # Eta of track extrapolated to first sampling
    EtaCorrMag = treeinfo.float
    # Cluster energy / track momentum
    EoverP = treeinfo.float
    EoverP2 = treeinfo.float
    # Difference between cluster eta (sampl 1) & track extrapolated to sampl 1
    deltaEta1 = treeinfo.float
    # Difference between cluster eta (sampl 2) & track extrapolated to sampl 2
    deltaEta2 = treeinfo.float
    # Difference between cluster phi (sampl 2) & track extrapolated to sampl 2
    deltaPhi2 = treeinfo.float


    TruJet = treeinfo.instance (Matched, selfcn = seltru)
    TruTau = treeinfo.instance (Matched, selfcn = seltru)
    trkCharge = treeinfo.float
    trkPt = treeinfo.float
    trkIpt = treeinfo.float

    # EMBremFit
    bremInvpT     = treeinfo.float
    bremRadius     = treeinfo.float
    bremX     = treeinfo.float
    bremClusterRadius     = treeinfo.float

    # EMConvert
    convTrackMatch = treeinfo.int
    convAngleMatch = treeinfo.int

    PtCut = treeinfo.int
    PtCut_N = treeinfo.int
    Loose = treeinfo.int
    Loose_N = treeinfo.int
    Medium = treeinfo.int
    Medium_N = treeinfo.int
    Tight = treeinfo.int
    Tight_N = treeinfo.int


class EgCl (object):
    nsample  = treeinfo.int
    m_e_emb0  = treeinfo.float
    m_e_emb1  = treeinfo.float
    m_e_emb2  = treeinfo.float
    m_e_emb3  = treeinfo.float
    m_e_eme0  = treeinfo.float
    m_e_eme1  = treeinfo.float
    m_e_eme2  = treeinfo.float
    m_e_eme3  = treeinfo.float
    m_e_tileg1  = treeinfo.float
    m_e_tileg2  = treeinfo.float
    m_e_tileg3  = treeinfo.float
    etab0  = treeinfo.float
    etab1  = treeinfo.float
    etab2  = treeinfo.float
    etab3  = treeinfo.float
    etae0  = treeinfo.float
    etae1  = treeinfo.float
    etae2  = treeinfo.float
    etae3  = treeinfo.float
    phib0  = treeinfo.float
    phib1  = treeinfo.float
    phib2  = treeinfo.float
    phib3  = treeinfo.float
    phie0  = treeinfo.float
    phie1  = treeinfo.float
    phie2  = treeinfo.float
    phie3  = treeinfo.float
    m_eta0  = treeinfo.float
    m_eta1  = treeinfo.float
    m_eta2  = treeinfo.float
    m_eta3  = treeinfo.float
    m_e_hec0  = treeinfo.float
    m_e_hec1  = treeinfo.float
    m_e_hec2  = treeinfo.float
    m_e_hec3  = treeinfo.float
    m_e_tilee0  = treeinfo.float
    m_e_tilee1  = treeinfo.float
    m_e_tilee2  = treeinfo.float
    m_e_fcal0  = treeinfo.float
    m_e_fcal1  = treeinfo.float
    m_e_fcal2  = treeinfo.float
    m_e_tileb0  = treeinfo.float
    m_e_tileb1  = treeinfo.float
    m_e_tileb2  = treeinfo.float
    m1_eta  = treeinfo.float
    m1_phi  = treeinfo.float
    m2_r  = treeinfo.float
    m2_lambda  = treeinfo.float
    center_lambda  = treeinfo.float
    m1_dens  = treeinfo.float


class Pho (egamma):
    #Electron_Overlap  = treeinfo.int
    # EMBremFit
    #bremInvpT     = treeinfo.float
    #bremRadius     = treeinfo.float
    #bremX     = treeinfo.float
    #bremClusterRadius     = treeinfo.float
    #bremInvpTerr     = treeinfo.float

    # EMConvert
    convTrackMatch = treeinfo.int
    convAngleMatch = treeinfo.int

    Egamma = treeinfo.int
    Egamma_N = treeinfo.int
    Tight = treeinfo.int
    Tight_N = treeinfo.int


class MuonBase (Particle):
    matchChi2 = treeinfo.float
    matchChi2OverDoF = treeinfo.float
    fitChi2 = treeinfo.float
    fitChi2OverDoF = treeinfo.float
    bestMatch = treeinfo.int
    isCombinedMuon = treeinfo.int
    hasInDetTrackParticle = treeinfo.int
    hasMuonExtrapolatedTrackParticle = treeinfo.int
    hasCombinedMuonTrackParticle = treeinfo.int
    author = treeinfo.int
    hasCluster = treeinfo.int
    hasCombinedMuon = treeinfo.int
    #etcone = treeinfo.float
    #nucone = treeinfo.float
    SumTrkPtInCone = treeinfo.float
    nTrkInCone = treeinfo.int
    etcone20 = treeinfo.int
    etcone40 = treeinfo.int
    nucone40 = treeinfo.int



class MuonBase1 (MuonBase):
    #hasMuonSpectrometerTrackParticle = treeinfo.int
    pass



class Muon (Particle):
    #etcone = treeinfo.float
    etcone10 = treeinfo.float
    etcone20 = treeinfo.float
    etcone30 = treeinfo.float
    etcone40 = treeinfo.float
    #etcone50 = treeinfo.float
    #etcone60 = treeinfo.float
    #etcone70 = treeinfo.float
    #nucone = treeinfo.float
    nucone10 = treeinfo.float
    nucone20 = treeinfo.float
    nucone30 = treeinfo.float
    nucone40 = treeinfo.float
    #nucone50 = treeinfo.float
    #nucone60 = treeinfo.float
    #nucone70 = treeinfo.float

    matchChi2 = treeinfo.float
    matchChi2OverDoF = treeinfo.float
    fitChi2 = treeinfo.float
    fitChi2OverDoF = treeinfo.float
    author = treeinfo.int
    hasCluster = treeinfo.int
    hasCombinedMuon = treeinfo.int
    bestMatch = treeinfo.int
    isCombinedMuon = treeinfo.int
    hasInDetTrackParticle = treeinfo.int
    hasMuonExtrapolatedTrackParticle = treeinfo.int
    hasCombinedMuonTrackParticle = treeinfo.int
    SumTrkPtInCone = treeinfo.float
    nTrkInCone = treeinfo.int
    #hasMuonSpectrometerTrackParticle = treeinfo.int

    d0 = treeinfo.float
    z0 = treeinfo.float

    class Trucls (Particle, Matched):
        etcone = treeinfo.float
        etcone10 = treeinfo.float
        etcone20 = treeinfo.float
        etcone30 = treeinfo.float
        etcone40 = treeinfo.float
        etcone50 = treeinfo.float
        etcone60 = treeinfo.float
        etcone70 = treeinfo.float
        status = treeinfo.float
        barcode = treeinfo.int
    Tru = treeinfo.instance (Trucls, selfcn = seltru)

    Staco = treeinfo.int
    PtCut = treeinfo.int
    Isolated = treeinfo.int



class Tau (Particle):
    etHadCalib = treeinfo.float
    etEMCalib = treeinfo.float
    emRadius = treeinfo.float
    isolationFraction = treeinfo.float
    centralityFraction = treeinfo.float
    nAssocTracksCore = treeinfo.int
    nAssocTracksIsol = treeinfo.int
    numTrack = treeinfo.int
    #author = treeinfo.int
    numStripCells = treeinfo.int
    numPi0 = treeinfo.int

    sumEMpx = treeinfo.float
    sumEMpy = treeinfo.float
    sumEMpz = treeinfo.float
    sumEMe  = treeinfo.float

    trackCaloEta1 = treeinfo.float
    trackCaloEta2 = treeinfo.float
    trackCaloEta3 = treeinfo.float
    trackCaloPhi1 = treeinfo.float
    trackCaloPhi2 = treeinfo.float
    trackCaloPhi3 = treeinfo.float

    #leadingTrackPT = treeinfo.float
    etChrgHAD = treeinfo.float
    etIsolEM = treeinfo.float
    etIsolHAD = treeinfo.float
    massTrk3P = treeinfo.float
    rWidth2Trk3P = treeinfo.float
    signD0Trk3P = treeinfo.float
    etHadAtEMScale = treeinfo.float
    etEMAtEMScale = treeinfo.float
    etEMCL = treeinfo.float
    etChrgEM = treeinfo.float
    etNeuEM = treeinfo.float
    etResNeuEM = treeinfo.float
    etChrgEM01_1Trk = treeinfo.float
    etChrgEM01_2Trk = treeinfo.float
    etChrgEM01_3Trk = treeinfo.float
    etResChrgEM_1Trk = treeinfo.float
    etResChrgEM_2Trk = treeinfo.float
    etResChrgEM_3Trk = treeinfo.float

    stripWidth2 = treeinfo.float

    totTrkCharge = treeinfo.float
    totTrkE = treeinfo.float
    track1_pT = treeinfo.float
    track2_pT = treeinfo.float
    track3_pT = treeinfo.float
    track1_phi = treeinfo.float
    track2_phi = treeinfo.float
    track3_phi = treeinfo.float
    track1_eta = treeinfo.float
    track2_eta = treeinfo.float
    track3_eta = treeinfo.float
    secVtxX = treeinfo.float
    secVtxY = treeinfo.float
    secVtxZ = treeinfo.float
    secVtxCovXX = treeinfo.float
    secVtxCovYY = treeinfo.float
    secVtxCovZZ = treeinfo.float
    secVtxCovXY = treeinfo.float
    secVtxCovYZ = treeinfo.float
    secVtxCovXZ = treeinfo.float
    secVtxChi2 = treeinfo.float
    secVtxNDoF = treeinfo.int
    trFlightPathSig = treeinfo.float
    z0SinThetaSig = treeinfo.float
    etChrgHADoverPttot = treeinfo.float
    etIsolFrac = treeinfo.float
    etEflow = treeinfo.float
    hadronicLeak = treeinfo.float
    secondaryMax = treeinfo.float
    sumEtCellsLArOverLeadTrackPt = treeinfo.float
    leadingTrackPT = treeinfo.float
    ipSigLeadTrack = treeinfo.float
    etaCalo = treeinfo.float
    phiCalo = treeinfo.float
    hadRadius = treeinfo.float
    sumEmCellEt = treeinfo.float
    sumHadCellEt = treeinfo.float
    Likelihood = treeinfo.float
    DiscCut = treeinfo.float
    DiscNN = treeinfo.float
    EfficNN = treeinfo.float
    nTracksdrdR = treeinfo.int
    hasAuthor_TauRec = treeinfo.int
    hasAuthor_Tau1P3P = treeinfo.int
    ElectronFlag = treeinfo.int
    numTrackDetails = treeinfo.int
    Trk_pT = treeinfo.float
    Trk_phi = treeinfo.float
    Trk_eta = treeinfo.float
    TauElTauLikelihood = treeinfo.float

    TopDef = treeinfo.int
    Electron_Overlap = treeinfo.int
    Muon_Overlap = treeinfo.int
    Photon_Overlap = treeinfo.int
    MuonFlag = treeinfo.int

    class Trucls (MatchedParticle):
        tauEta = treeinfo.float
        tauPhi = treeinfo.float
        tauE = treeinfo.float
        tauET = treeinfo.float
        tauPT = treeinfo.float
        tauPx = treeinfo.float
        tauPy = treeinfo.float
        tauPz = treeinfo.float
        stablesEAll = treeinfo.float
        stablesEAllCharged = treeinfo.float
        stablesEAllPion = treeinfo.float
        stablesEAllKaon = treeinfo.float
        stablesEAllOtherCh = treeinfo.float
        stablesEAllNeut = treeinfo.float
        stablesEAllPiZ = treeinfo.float
        stablesEAllGamma = treeinfo.float
        stablesEAllOtherNeut = treeinfo.float
        stablesETAll = treeinfo.float
        stablesETAllCharged = treeinfo.float
        stablesETAllPion = treeinfo.float
        stablesETAllKaon = treeinfo.float
        stablesETAllOtherCh = treeinfo.float
        stablesETAllNeut = treeinfo.float
        stablesETAllPiZ = treeinfo.float
        stablesETAllGamma = treeinfo.float
        stablesETAllOtherNeut = treeinfo.float
        stablesPTAll = treeinfo.float
        stablesPTAllCharged = treeinfo.float
        stablesPTAllPion = treeinfo.float
        stablesPTAllKaon = treeinfo.float
        stablesPTAllOtherCh = treeinfo.float
        stablesPTAllNeut = treeinfo.float
        stablesPTAllPiZ = treeinfo.float
        stablesPTAllGamma = treeinfo.float
        stablesPTAllOtherNeut = treeinfo.float

        compPartIndex = treeinfo.int
        test_FailHasCharge = treeinfo.int
        stablesN = treeinfo.int
        nDaughters = treeinfo.int
        nStable = treeinfo.int
        nStableCharged = treeinfo.int
        nStablePion = treeinfo.int
        nStableKaon = treeinfo.int
        nStableOtherCh = treeinfo.int
        nStableNeut = treeinfo.int
        nStablePiZ = treeinfo.int
        nStableGamma = treeinfo.int
        nStableOtherNeut = treeinfo.int
        nStableCounted = treeinfo.int
        nStableNoneMissing = treeinfo.int
    Tru = treeinfo.instance (Trucls, selfcn = seltru)

    L1_ROI = treeinfo.instance (Matched)
    L2 = treeinfo.instance (MatchedParticle)
    EF = treeinfo.instance (MatchedParticle)

    Pi0_eta = treeinfo.float
    Pi0_phi = treeinfo.float
    Pi0_et = treeinfo.float
    Pi0_e = treeinfo.float


class Jet (Particle):
    weight = treeinfo.float
    IP1D = treeinfo.float
    IP2D = treeinfo.float
    IP3D = treeinfo.float
    #SV1 = treeinfo.float
    #SecVtxTagBU = treeinfo.float
    #SV2 = treeinfo.float

    #SoftMuonWeight = treeinfo.float
    #SoftElectronWeight = treeinfo.float
    #SoftMuonNumTrack = treeinfo.int
    #SoftElectronNumTrack = treeinfo.int


class PJet (Particle):
    EinPreSampB = treeinfo.float
    EinPreSampE = treeinfo.float
    EinEMB1 = treeinfo.float
    EinEMB2 = treeinfo.float
    EinEMB3 = treeinfo.float
    EinEME1 = treeinfo.float
    EinEME3 = treeinfo.float
    EinEME4 = treeinfo.float
    EinHEC0 = treeinfo.float
    EinHEC1 = treeinfo.float
    EinHEC2 = treeinfo.float
    EinHEC3 = treeinfo.float
    EinTileBar0 = treeinfo.float
    EinTileBar1 = treeinfo.float
    EinTileBar2 = treeinfo.float
    EinTileGap1 = treeinfo.float
    EinTileGap2 = treeinfo.float
    EinTileGap3 = treeinfo.float
    EinTileExt0 = treeinfo.float
    EinTileExt1 = treeinfo.float
    EinTileExt2 = treeinfo.float
    EinFCAL0 = treeinfo.float
    EinFCAL1 = treeinfo.float
    EinFCAL2 = treeinfo.float
    EinUnknown = treeinfo.float
    EinCryostat = treeinfo.float

    def esamp (self, samp):
        if samp == samplings.PreSamplerB:
            return self.EinPreSampB
        if samp == samplings.PreSamplerE:
            return self.EinPreSampE
        sname = samplings.samplings[samp]
        return getattr (self, "Ein" + sname)

    weight = treeinfo.float
    IP1D = treeinfo.float
    IP2D = treeinfo.float
    IP3D = treeinfo.float
    SV1 = treeinfo.float
    SecVtxTagBU = treeinfo.float
    SV2 = treeinfo.float

    SoftMuonWeight = treeinfo.float
    SoftMuonNumTrack = treeinfo.float
    SoftElectronWeight = treeinfo.float
    SoftElectronNumTrack = treeinfo.float

    DRTruB = treeinfo.float
    DRTruC = treeinfo.float
    DRTruT = treeinfo.float

    nTracks = treeinfo.int
    TruthLabel = treeinfo.int

    TopDef  = treeinfo.int
    BTagged = treeinfo.int
    BTaggedPlus = treeinfo.int
    BTaggedMinus = treeinfo.int
    Electron_Overlap = treeinfo.int
    Photon_Overlap = treeinfo.int
    TauJet_Overlap = treeinfo.int
    Muon_Overlap = treeinfo.int

    # Should be an instance?
    SecVtx_x = treeinfo.float
    SecVtx_y = treeinfo.float
    SecVtx_z = treeinfo.float
    SecVtx_x_SD = treeinfo.float
    SecVtx_y_SD = treeinfo.float
    SecVtx_z_SD = treeinfo.float
    SecVtx_Chi2 = treeinfo.float
    SecVtx_FitNDF = treeinfo.float

    class Trucls (MatchedParticle):
        etcone = treeinfo.float
        etcone10 = treeinfo.float
        etcone20 = treeinfo.float
        etcone30 = treeinfo.float
        etcone40 = treeinfo.float
        etcone50 = treeinfo.float
        etcone60 = treeinfo.float
        etcone70 = treeinfo.float
        status = treeinfo.float
        barcode = treeinfo.int
    Tru = treeinfo.instance (Trucls, selfcn = seltru)

    TruJet = treeinfo.instance (MatchedParticle, selfcn = seltru)

    L1_ROI = treeinfo.instance (MatchedFourvec)
    L2 = treeinfo.instance (MatchedParticle)
    EF = treeinfo.instance (MatchedParticle)


# Should derive from a fourvec class?
class MET (object):
    ex = treeinfo.double
    ey = treeinfo.double
    pt = treeinfo.double ('%s_et')
    sumet = treeinfo.double
    def get_phi (self):
        return math.atan2 (self.ey, self.ex)
    def set_phi (self, phi):
        self.ex = self.et * math.cos (phi)
        self.ey = self.et * math.sin (phi)
        return
    phi = property (get_phi, set_phi)
    def get_et (self):
        return self.pt
    def set_et (self, x):
        self.pt = x
    et = property (get_et, set_et)


class Global (object):
    evweight = treeinfo.double ('eventWeight')
    eventWeightMCatNLO = treeinfo.double
    runNumber = treeinfo.int
    eventNumber = treeinfo.int

class FullGlobal (Global):
    N = treeinfo.int
    GoodLepton_N = treeinfo.int
    GoodJet_N = treeinfo.int
    NonLooseJet_N = treeinfo.int
    IsSimulation = treeinfo.int
    IsCalibration = treeinfo.int
    IsTestBeam = treeinfo.int
    LumiBlock = treeinfo.int
    TimeStampOffset = treeinfo.int
    TimeStamp = treeinfo.int
    BunchCrossingID = treeinfo.int
    DetectorMask0 = treeinfo.int
    DetectorMask1 = treeinfo.int


class Counters (object):
    Instance = treeinfo.int ("%sInstance")
    Counter = treeinfo.int ("%sCounter")
    EventAndInstance = treeinfo.int ("%sEventAndInstance")
    RunNumber = treeinfo.int ("%sRunNumber")
    NInstance = treeinfo.int ("%sNInstance")


def label_maker (prefix, label):
    ll = string.split (label, '.')
    if len (ll) == 1:
        def sel (tree, prefix=prefix, label=ll[0]):
            return [p for p in getattr(tree, prefix) if getattr(p, label)]
    elif len(ll) == 2:
        def sel (tree, prefix=prefix, label1=ll[0], label2=ll[1]):
            return [p for p in getattr(tree, prefix)
                    if getattr (getattr(p, label1), label2)]
    return sel


def wrap_FullReco0 (tt):
    tt.add_list (EgCl, 'EgCl', 100, count = 'N', warnmissing = True)
    tt.add_list (Ele, 'El', 100, count = 'N', warnmissing = True)
    tt.add_transient ('ElTop', label_maker ('El', 'TopDef'))
    tt.add_transient ('ElMuOverlap', label_maker ('El', 'Muon_Overlap'))
    #tt.add_transient ('ElTru', label_maker ('El', 'Tru.Matched'))
    tt.add_transient ('ElTruJet', label_maker ('El', 'TruJet.Matched'))
    tt.add_transient ('ElTruTau', label_maker ('El', 'TruTau.Matched'))
    tt.add_transient ('ElL1_ROI', label_maker ('El', 'L1_ROI.Matched'))
    tt.add_transient ('ElL2', label_maker ('El', 'L2.Matched'))
    tt.add_transient ('ElEF', label_maker ('El', 'EF.Matched'))

    tt.add_list (Pho, 'Ph', 100, count = 'N', warnmissing = True)
    tt.add_transient ('PhTop', label_maker ('Ph', 'TopDef'))
    tt.add_transient ('PhMuOverlap', label_maker ('Ph', 'Muon_Overlap'))
    tt.add_transient ('PhElOverlap', label_maker ('Ph', 'Electron_Overlap'))
    #tt.add_transient ('PhTru', label_maker ('Ph', 'Tru.Matched'))
    tt.add_transient ('PhL1_ROI', label_maker ('Ph', 'L1_ROI.Matched'))
    tt.add_transient ('PhL2', label_maker ('Ph', 'L2.Matched'))
    tt.add_transient ('PhEF', label_maker ('Ph', 'EF.Matched'))

    tt.add_list (Muon, 'Mu', 100, count = 'N', warnmissing = True)
    tt.add_transient ('MuTop', label_maker ('Mu', 'TopDef'))
    #tt.add_transient ('MuTru', label_maker ('Mu', 'Tru.Matched'))
    #tt.add_list (MuonBase1, 'MuidMuon', 10, count = 'N', warnmissing = True)
    #tt.add_list (MuonBase, 'CaloMuon', 10, count = 'N', warnmissing = True)
    tt.add_transient ('MuL1_ROI', label_maker ('Mu', 'L1_ROI.Matched'))
    tt.add_transient ('MuL2', label_maker ('Mu', 'L2.Matched'))
    tt.add_transient ('MuEF', label_maker ('Mu', 'EF.Matched'))

    #tt.add_list (Tau, 'Tau', 100, count = 'N', warnmissing = True)
    #tt.add_transient ('TauTru', label_maker ('Tau', 'Tru.Matched'))
    tt.add_transient ('TauTop', label_maker ('Tau', 'TopDef'))
    tt.add_transient ('TauElOverlap', label_maker ('Tau', 'Electron_Overlap'))
    tt.add_transient ('TauMuOverlap', label_maker ('Tau', 'Muon_Overlap'))
    tt.add_transient ('TauPhOverlap', label_maker ('Tau', 'Photon_Overlap'))
    tt.add_transient ('TauL1_ROI', label_maker ('Tau', 'L1_ROI.Matched'))
    tt.add_transient ('TauL2', label_maker ('Tau', 'L2.Matched'))
    tt.add_transient ('TauEF', label_maker ('Tau', 'EF.Matched'))

    #tt.add_list (Particle, 'Tau1P3P', 100, count = 'N', warnmissing = True)

    #tt.add_list (PJet, 'PJet', 100, count = 'N', warnmissing = True)
    tt.add_transient ('PJetTop', label_maker ('PJet', 'TopDef'))
    tt.add_transient ('PJetBTagged', label_maker ('PJet', 'BTagged'))
    tt.add_transient ('PJetBTaggedPlus', label_maker ('PJet', 'BTaggedPlus'))
    tt.add_transient ('PJetBTaggedMinus', label_maker ('PJet', 'BTaggedMinus'))
    tt.add_transient ('PJetPhOverlap', label_maker ('PJet', 'Photon_Overlap'))
    tt.add_transient ('PJetTauOverlap', label_maker ('PJet', 'TauJet_Overlap'))
    tt.add_transient ('PJetMuOverlap', label_maker ('PJet', 'Muon_Overlap'))
    tt.add_transient ('PJetElOverlap', label_maker ('PJet', 'Electron_Overlap'))
    tt.add_transient ('PJetTru', label_maker ('PJet', 'Tru.Matched'))
    tt.add_transient ('PJetTruJet', label_maker ('PJet', 'TruJet.Matched'))
    tt.add_transient ('PJetL1_ROI', label_maker ('PJet', 'L1_ROI.Matched'))
    tt.add_transient ('PJetL2', label_maker ('PJet', 'L2.Matched'))
    tt.add_transient ('PJetEF', label_maker ('PJet', 'EF.Matched'))

    #tt.add_list (Jet, 'Cone4H1TopoJets', 100, count = 'N', warnmissing = True)
    #tt.add_list (Jet, 'Cone7H1TowerJets', 100, count = 'N', warnmissing = True)
    #tt.add_list (Jet, 'Kt6LCTopoJets', 100, count = 'N', warnmissing = True)
    tt.add_list (Jet, 'Jet', 100, count = 'N', warnmissing = True)

    tt.add (MET, 'MET_Final')
    tt.add (MET, 'MET_RefFinal')
    tt.add (MET, 'ObjMET_Final')
    tt.add (MET, 'MET_Base')
    tt.add (MET, 'MET_Calib')
    tt.add (MET, 'MET_Muon')
    tt.add (MET, 'MET_MuonBoy')
    tt.add (MET, 'MET_Cryo')
    tt.add (MET, 'MET_CryoCone')
    tt.add (MET, 'MET_Topo')

    tt.add_list (L1EM, 'L1EM', 50, count = 'N', warnmissing = True)
    tt.add_list (L2El, 'L2El', 50, count = 'N', warnmissing = True)
    tt.add_list (L2Pho, 'L2Pho', 50, count = 'N', warnmissing = True)
    tt.add_list (EFEl, 'EFEl', 50, count = 'N', warnmissing = True)
    tt.add_list (EFPh, 'EFPh', 50, count = 'N', warnmissing = True)

    tt.add (PassedL1, 'PassedL1', warnmissing = True)
    tt.add (PassedL2, 'PassedL2', warnmissing = True)
    tt.add (PassedEF, 'PassedEF', warnmissing = True)
    tt.add (Passed, 'Passed', warnmissing = True)

    tt.add_list (Tru, 'Truth', 1000, count = 'N', warnmissing = True)

    tt.add_list (Particle, 'Zee', 100, count = 'N', warnmissing = True)

    tt.add (FullGlobal)
    tt.add (Counters, 'FullReco')

    return tt


class Tru (Particle):
    etcone = treeinfo.float
    etcone10 = treeinfo.float
    etcone20 = treeinfo.float
    etcone30 = treeinfo.float
    etcone40 = treeinfo.float
    etcone50 = treeinfo.float
    etcone60 = treeinfo.float
    etcone70 = treeinfo.float
    status = treeinfo.float

    #TopWithHadronicW = treeinfo.int
    #TopWithLeptonicW = treeinfo.int
    #HadronicW = treeinfo.int
    #LeptonicW = treeinfo.int
    #LightQuark = treeinfo.int
    #Bottom = treeinfo.int
    #Electron = treeinfo.int
    #Muon = treeinfo.int
    #Tau = treeinfo.int
    #Neutrino = treeinfo.int
    #motherTop_pdgId = treeinfo.int
    #motherTop_barcode = treeinfo.int
    nParents = treeinfo.int
    nDecay = treeinfo.int
    barcode = treeinfo.int

    # These are of type vector<vector<double> >
    # So these declarations aren't right.
    # But it works temporarily.
    ParentIDs = treeinfo.double
    ParentBarcodes = treeinfo.double
    ParentStatus = treeinfo.double
    #ParentNumber = treeinfo.double
    DecayIDs = treeinfo.double
    DecayBarcodes = treeinfo.double
    DecayStatus = treeinfo.double
    #DecayNumber = treeinfo.double

    ParentPt = treeinfo.double
    DecayPt = treeinfo.double
    ParentEta = treeinfo.double
    DecayEta = treeinfo.double



class TruGlobal (Global):
    FilteredN = treeinfo.int
    StableN = treeinfo.int



def wrap_Truth0 (tt):
    tt.add_list (Tru, 'Tru', 1000, count = 'N', warnmissing = True)
    tt.add_list (Particle, 'TruJet', 1000, count = 'N', warnmissing = True)
    tt.add (MET, 'MET_Truth_Int', warnmissing = True)
    tt.add (MET, 'MET_Truth_NonInt', warnmissing = True)
    tt.add (MET, 'MET_Truth_IntCentral', warnmissing = True)
    tt.add (MET, 'MET_Truth_IntFwd', warnmissing = True)
    tt.add (MET, 'MET_Truth_IntOutCover', warnmissing = True)
    tt.add (MET, 'MET_Truth_Muons', warnmissing = True)

    tt.add (TruGlobal, warnmissing = True)
    tt.add (Counters, "Truth", warnmissing = True)
    return tt



class PassedL1 (object):
    EM100 = treeinfo.int
    EM13 = treeinfo.int
    EM13I = treeinfo.int
    EM13I_MU11 = treeinfo.int
    EM13_MU11 = treeinfo.int
    EM13_XE20 = treeinfo.int
    EM18 = treeinfo.int
    EM18I = treeinfo.int
    EM18_XE15 = treeinfo.int
    EM18_XE30 = treeinfo.int
    EM23I = treeinfo.int
    EM3 = treeinfo.int
    EM7 = treeinfo.int
    EM7_MU6 = treeinfo.int
    EM7_XE30 = treeinfo.int
    FJ120 = treeinfo.int
    FJ18 = treeinfo.int
    FJ35 = treeinfo.int
    FJ70 = treeinfo.int
    J10 = treeinfo.int
    J120 = treeinfo.int
    J18 = treeinfo.int
    J23 = treeinfo.int
    J35 = treeinfo.int
    J42 = treeinfo.int
    J42_XE30_EM13I = treeinfo.int
    J42_XE30_MU11 = treeinfo.int
    J5 = treeinfo.int
    J70 = treeinfo.int
    J70_XE30 = treeinfo.int
    JE120 = treeinfo.int
    JE220 = treeinfo.int
    JE280 = treeinfo.int
    JE340 = treeinfo.int
    MBTS_1 = treeinfo.int
    MBTS_1_1 = treeinfo.int
    MBTS_2 = treeinfo.int
    MU10 = treeinfo.int
    MU10_J18 = treeinfo.int
    MU11 = treeinfo.int
    MU11_XE15 = treeinfo.int
    MU20 = treeinfo.int
    MU20_XE30 = treeinfo.int
    MU4 = treeinfo.int
    MU40 = treeinfo.int
    MU4_EM3 = treeinfo.int
    MU4_J10 = treeinfo.int
    MU4_J18 = treeinfo.int
    MU4_J23 = treeinfo.int
    MU4_J35 = treeinfo.int
    MU4_J42 = treeinfo.int
    MU4_J5 = treeinfo.int
    MU6 = treeinfo.int
    MU6_EM3 = treeinfo.int
    MU6_J5 = treeinfo.int
    RD0 = treeinfo.int
    TAU11I = treeinfo.int
    TAU11I_2J5_J18 = treeinfo.int
    TAU11I_2J5_J35 = treeinfo.int
    TAU11I_3J5_2J23 = treeinfo.int
    TAU11I_MU10 = treeinfo.int
    TAU11I_MU20 = treeinfo.int
    TAU11I_MU6 = treeinfo.int
    TAU11I_XE30 = treeinfo.int
    TAU16I = treeinfo.int
    TAU16I_XE20 = treeinfo.int
    TAU16I_XE30 = treeinfo.int
    TAU16I_XE40 = treeinfo.int
    TAU25 = treeinfo.int
    TAU25I = treeinfo.int
    TAU25I_XE20 = treeinfo.int
    TAU25_XE40 = treeinfo.int
    TAU40 = treeinfo.int
    TAU5 = treeinfo.int
    TAU6 = treeinfo.int
    TAU6_2J5_J35 = treeinfo.int
    TAU6_3J5_2J42_XE30 = treeinfo.int
    TAU6_3J5_J42 = treeinfo.int
    TAU6_XE20 = treeinfo.int
    TAU9I = treeinfo.int
    TAU9I_2J5_J35 = treeinfo.int
    TAU9I_3J5_2J23 = treeinfo.int
    TAU9I_5J5_4J23 = treeinfo.int
    TAU9I_MU10 = treeinfo.int
    TAU9I_MU6 = treeinfo.int
    TAU9I_XE20 = treeinfo.int
    TAU9I_XE30 = treeinfo.int
    TAU9I_XE40 = treeinfo.int
    TE150 = treeinfo.int
    TE250 = treeinfo.int
    TE360 = treeinfo.int
    TE650 = treeinfo.int
    XE15 = treeinfo.int
    XE20 = treeinfo.int
    XE25 = treeinfo.int
    XE30 = treeinfo.int
    XE40 = treeinfo.int
    XE50 = treeinfo.int
    XE70 = treeinfo.int
    XE80 = treeinfo.int
    _2EM13 = treeinfo.int ('%s_2EM13')
    _2EM13I = treeinfo.int ('%s_2EM13I')
    _2EM13_MU6 = treeinfo.int ('%s_2EM13_MU6')
    _2EM18 = treeinfo.int ('%s_2EM18')
    _2EM18I = treeinfo.int ('%s_2EM18I')
    _2EM18_MU6 = treeinfo.int ('%s_2EM18_MU6')
    _2EM23I = treeinfo.int ('%s_2EM23I')
    _2EM3 = treeinfo.int ('%s_2EM3')
    _2EM3_EM7 = treeinfo.int ('%s_2EM3_EM7')
    _2EM3_TAU6 = treeinfo.int ('%s_2EM3_TAU6')
    _2EM7 = treeinfo.int ('%s_2EM7')
    _2EM7_MU6 = treeinfo.int ('%s_2EM7_MU6')
    _2FJ18 = treeinfo.int ('%s_2FJ18')
    _2FJ35 = treeinfo.int ('%s_2FJ35')
    _2FJ70 = treeinfo.int ('%s_2FJ70')
    _2J42_XE30 = treeinfo.int ('%s_2J42_XE30')
    _2MU10 = treeinfo.int ('%s_2MU10')
    _2MU11 = treeinfo.int ('%s_2MU11')
    _2MU20 = treeinfo.int ('%s_2MU20')
    _2MU4 = treeinfo.int ('%s_2MU4')
    _2MU4_MU6 = treeinfo.int ('%s_2MU4_MU6')
    _2MU6 = treeinfo.int ('%s_2MU6')
    _2MU6_EM7 = treeinfo.int ('%s_2MU6_EM7')
    _2TAU11I_EM23I = treeinfo.int ('%s_2TAU11I_EM23I')
    _2TAU16I = treeinfo.int ('%s_2TAU16I')
    _2TAU6 = treeinfo.int ('%s_2TAU6')
    _2TAU6_EM7 = treeinfo.int ('%s_2TAU6_EM7')
    _2TAU6_TAU11I_EM7 = treeinfo.int ('%s_2TAU6_TAU11I_EM7')
    _2TAU6_TAU16I = treeinfo.int ('%s_2TAU6_TAU16I')
    _2TAU6_TAU25 = treeinfo.int ('%s_2TAU6_TAU25')
    _2TAU6_TAU9I_EM7 = treeinfo.int ('%s_2TAU6_TAU9I_EM7')
    _2TAU9I = treeinfo.int ('%s_2TAU9I')
    _2TAU9I_EM13I = treeinfo.int ('%s_2TAU9I_EM13I')
    _3EM7 = treeinfo.int ('%s_3EM7')
    _3J10 = treeinfo.int ('%s_3J10')
    _3J18 = treeinfo.int ('%s_3J18')
    _3J23 = treeinfo.int ('%s_3J23')
    _3J70 = treeinfo.int ('%s_3J70')
    _3MU6 = treeinfo.int ('%s_3MU6')
    _4J10 = treeinfo.int ('%s_4J10')
    _4J18 = treeinfo.int ('%s_4J18')
    _4J23 = treeinfo.int ('%s_4J23')
    _4J23_EM13I = treeinfo.int ('%s_4J23_EM13I')
    _4J23_MU11 = treeinfo.int ('%s_4J23_MU11')
    _4J35 = treeinfo.int ('%s_4J35')


class PassedHLT (object):
    FJ120 = treeinfo.int
    FJ18 = treeinfo.int
    FJ35 = treeinfo.int
    FJ70 = treeinfo.int
    J10 = treeinfo.int
    J50 = treeinfo.int
    J80 = treeinfo.int
    JE120 = treeinfo.int
    JE220 = treeinfo.int
    JE280 = treeinfo.int
    JE340 = treeinfo.int
    Jpsiee = treeinfo.int
    MU4_Bmumu_FS = treeinfo.int
    MU4_Jpsie5e3_FS = treeinfo.int
    MU4_Jpsimumu_FS = treeinfo.int
    MU4_Upsimumu_FS = treeinfo.int
    MbRndm = treeinfo.int
    MbSp = treeinfo.int
    MbTrk = treeinfo.int
    Zee = treeinfo.int
    _2FJ18 = treeinfo.int ('%s_2FJ18')
    _2FJ35 = treeinfo.int ('%s_2FJ35')
    _2FJ70 = treeinfo.int ('%s_2FJ70')
    _2MU4_DiMu = treeinfo.int ('%s_2MU4_DiMu')
    _2MU4_Jpsimumu     = treeinfo.int  ('%s_2MU4_Jpsimumu')
    _2MU4_Upsimumu     = treeinfo.int  ('%s_2MU4_Upsimumu')
    _2b23_3L1J23_passHLT = treeinfo.int ('%s_2b23_3L1J23_passHLT')
    _2e10_loose = treeinfo.int ('%s_2e10_loose')
    _2e10_mu6 = treeinfo.int ('%s_2e10_mu6')
    _2e12_tight = treeinfo.int ('%s_2e12_tight')
    _2e15_medium = treeinfo.int ('%s_2e15_medium')
    _2e20_loose = treeinfo.int ('%s_2e20_loose')
    _2e5_medium = treeinfo.int ('%s_2e5_medium')
    _2e5_medium1 = treeinfo.int ('%s_2e5_medium1')
    _2e6_medium = treeinfo.int ('%s_2e6_medium')
    _2e6_medium1 = treeinfo.int ('%s_2e6_medium1')
    _2g10 = treeinfo.int ('%s_2g10')
    _2g10_mu6 = treeinfo.int ('%s_2g10_mu6')
    _2g15 = treeinfo.int ('%s_2g15')
    _2g17i_L33 = treeinfo.int ('%s_2g17i_L33')
    _2g20 = treeinfo.int ('%s_2g20')
    _2j42_xe30 = treeinfo.int ('%s_2j42_xe30')
    _2mu10 = treeinfo.int ('%s_2mu10')
    _2mu20 = treeinfo.int ('%s_2mu20')
    _2mu4 = treeinfo.int ('%s_2mu4')
    _2mu6 = treeinfo.int ('%s_2mu6')
    _2mu6_e10 = treeinfo.int ('%s_2mu6_e10')
    _2mu6_g10 = treeinfo.int ('%s_2mu6_g10')
    _2tau12 = treeinfo.int ('%s_2tau12')
    _2tau20i = treeinfo.int ('%s_2tau20i')
    _2tau20i_PT = treeinfo.int ('%s_2tau20i_PT')
    _2tau29i = treeinfo.int ('%s_2tau29i')
    _3b18_4L1J18 = treeinfo.int ('%s_3b18_4L1J18')
    _3b18_4L1J18_passHLT = treeinfo.int ('%s_3b18_4L1J18_passHLT')
    _3b23_3L1J23 = treeinfo.int ('%s_3b23_3L1J23')
    _3e15_medium = treeinfo.int ('%s_3e15_medium')
    _3g10 = treeinfo.int ('%s_3g10')
    _3mu6 = treeinfo.int ('%s_3mu6')
    _4J80 = treeinfo.int ('%s_4J80')
    _4j23_e15i = treeinfo.int ('%s_4j23_e15i')
    _4j23_mu15 = treeinfo.int ('%s_4j23_mu15')
    b18 = treeinfo.int
    b23 = treeinfo.int
    b35 = treeinfo.int
    b42 = treeinfo.int
    b70 = treeinfo.int
    e105_loose1 = treeinfo.int
    e10_medium = treeinfo.int
    e10_mu6 = treeinfo.int
    e10_xe30 = treeinfo.int
    e12_medium = treeinfo.int
    e15_medium = treeinfo.int
    e15_xe20 = treeinfo.int
    e15i_medium = treeinfo.int
    e20_g20 = treeinfo.int
    e20_loose = treeinfo.int
    e20_loose_passEF = treeinfo.int
    e20_loose_passL2 = treeinfo.int
    e20_xe15 = treeinfo.int
    e20_xe30 = treeinfo.int
    e20i_loose = treeinfo.int
    e22i_tight = treeinfo.int
    e25_loose = treeinfo.int
    e25i_loose = treeinfo.int
    e25i_medium1 = treeinfo.int
    e55_loose1 = treeinfo.int
    e5_e10_medium = treeinfo.int
    e5_e7_medium = treeinfo.int
    e5_medium = treeinfo.int
    e5_medium1 = treeinfo.int
    e6_medium = treeinfo.int
    e6_medium1 = treeinfo.int
    e7_medium = treeinfo.int
    em105_passHLT = treeinfo.int
    em20_passHLT = treeinfo.int
    em20i_passHLT = treeinfo.int
    g10 = treeinfo.int
    g105 = treeinfo.int
    g10_calib = treeinfo.int
    g15 = treeinfo.int
    g150 = treeinfo.int
    g15i = treeinfo.int
    g20 = treeinfo.int
    g20_xe15 = treeinfo.int
    g20i = treeinfo.int
    g20i_calib = treeinfo.int
    g25 = treeinfo.int
    g25_xe30 = treeinfo.int
    g25i = treeinfo.int
    g3_L33 = treeinfo.int
    g55_L33 = treeinfo.int
    j42_xe30_e15i = treeinfo.int
    j42_xe30_mu15 = treeinfo.int
    j70_xe30 = treeinfo.int
    mu10 = treeinfo.int
    mu10_j18 = treeinfo.int
    mu15 = treeinfo.int
    mu15_xe15 = treeinfo.int
    mu15i_loose = treeinfo.int
    mu20 = treeinfo.int
    mu20_passHLT = treeinfo.int
    mu20_xe30 = treeinfo.int
    mu20i_loose = treeinfo.int
    mu4 = treeinfo.int
    mu40 = treeinfo.int
    mu4_DsPhiPi_FS = treeinfo.int
    mu4_j10 = treeinfo.int
    mu4_j10_matched = treeinfo.int
    mu4_j18_matched = treeinfo.int
    mu4_j23_matched = treeinfo.int
    mu4_j35_matched = treeinfo.int
    mu4_j42_matched = treeinfo.int
    mu4_mu6 = treeinfo.int
    mu6 = treeinfo.int
    stau = treeinfo.int
    tau125_loose = treeinfo.int
    tau12_PT = treeinfo.int
    tau12_b23_j42 = treeinfo.int
    tau12_b35 = treeinfo.int
    tau12_e10 = treeinfo.int
    tau12_loose = treeinfo.int
    tau12_tau29i = treeinfo.int
    tau12_tau29i_PT = treeinfo.int
    tau12_tau38 = treeinfo.int
    tau12_xe20 = treeinfo.int
    tau16i_2j23 = treeinfo.int
    tau16i_2j70 = treeinfo.int
    tau16i_3j23 = treeinfo.int
    tau16i_4j23 = treeinfo.int
    tau16i_4j23_WO = treeinfo.int
    tau16i_e10 = treeinfo.int
    tau16i_e15i = treeinfo.int
    tau16i_j120 = treeinfo.int
    tau16i_j70 = treeinfo.int
    tau16i_j70_WO = treeinfo.int
    tau16i_loose = treeinfo.int
    tau16i_mu10 = treeinfo.int
    tau16i_mu6 = treeinfo.int
    tau16i_xe30 = treeinfo.int
    tau20i_2b23 = treeinfo.int
    tau20i_b18 = treeinfo.int
    tau20i_b35 = treeinfo.int
    tau20i_e10 = treeinfo.int
    tau20i_e15i = treeinfo.int
    tau20i_e25i = treeinfo.int
    tau20i_j120 = treeinfo.int
    tau20i_j70 = treeinfo.int
    tau20i_loose = treeinfo.int
    tau20i_mu10 = treeinfo.int
    tau20i_mu20 = treeinfo.int
    tau20i_mu6 = treeinfo.int
    tau20i_xe30 = treeinfo.int
    tau29i_loose = treeinfo.int
    tau29i_xe20 = treeinfo.int
    tau29i_xe30 = treeinfo.int
    tau29i_xe40 = treeinfo.int
    tau38_loose = treeinfo.int
    tau38_xe40 = treeinfo.int
    tau38i_loose = treeinfo.int
    tau38i_xe20 = treeinfo.int
    tau50_loose = treeinfo.int
    tau84_loose = treeinfo.int
    tauNoCut = treeinfo.int
    te150 = treeinfo.int
    te250 = treeinfo.int
    te360 = treeinfo.int
    te650 = treeinfo.int
    xe15 = treeinfo.int
    xe20 = treeinfo.int
    xe25 = treeinfo.int
    xe30 = treeinfo.int
    xe40 = treeinfo.int
    xe50 = treeinfo.int
    xe70 = treeinfo.int
    xe80 = treeinfo.int



class PassedL2 (PassedHLT):
    J110 = treeinfo.int
    J150               = treeinfo.int
    J205 = treeinfo.int
    J23 = treeinfo.int
    J60 = treeinfo.int
    _3J120 = treeinfo.int ('%s_3J120')
    _3J15 = treeinfo.int ('%s_3J15')
    _3J35 = treeinfo.int ('%s_3J35')
    _4J15 = treeinfo.int ('%s_4J15')
    _4J35 = treeinfo.int ('%s_4J35')
    _4J50 = treeinfo.int ('%s_4J50')
    trk16i_calib = treeinfo.int
    trk9i_calib = treeinfo.int
class PassedEF (PassedHLT):
    J115 = treeinfo.int
    J140 = treeinfo.int
    J180 = treeinfo.int
    J265 = treeinfo.int
    J350 = treeinfo.int
    _3J180 = treeinfo.int ('%s_3J180')
    _3J25 = treeinfo.int ('%s_3J25')
    _3J60 = treeinfo.int ('%s_3J60')
    _4J125 = treeinfo.int ('%s_4J125')
    _4J45 = treeinfo.int ('%s_4J45')
    _4J95 = treeinfo.int ('%s_4J95')


class Trigobj (Fourvec_NoM):
    charge = treeinfo.float


class L1Trigobj (Trigobj):
    RoIWord = treeinfo.int


class L1EM (L1Trigobj):
    Core = treeinfo.float
    EMClus = treeinfo.float
    TauClus = treeinfo.float
    EMIsol = treeinfo.float
    HadIsol = treeinfo.float
    HadCore = treeinfo.float
    ThrPat = treeinfo.int


class L1Mu (L1Trigobj):
    ThrNum = treeinfo.int
    RoINum = treeinfo.int
    SecAddr = treeinfo.int
    FirstCand = treeinfo.int
    RoIOvfl = treeinfo.int
    SecOvfl = treeinfo.int
    Source = treeinfo.int
    Hemisphere = treeinfo.int


class L1Jet (L1Trigobj):
    ET4x4 = treeinfo.float
    ET6x6 = treeinfo.float
    ET8x8 = treeinfo.float
    ThrPat = treeinfo.int


class L2egamma (Trigobj):
    class CLcls (object):
        e237 = treeinfo.float
        e277 = treeinfo.float
        fracs1 = treeinfo.float
        weta2 = treeinfo.float
        ehad1 = treeinfo.float
        Eta1 = treeinfo.float
        emaxs1 = treeinfo.float
        e2tsts1 = treeinfo.float


class L2IDcls (object):
    chi2 = treeinfo.float
    param_a0 = treeinfo.float
    param_z0 = treeinfo.float
    param_phi0 = treeinfo.float
    param_eta = treeinfo.float
    param_p_T = treeinfo.float
    param_ea0 = treeinfo.float
    param_ez0 = treeinfo.float
    param_ephi0 = treeinfo.float
    param_eeta = treeinfo.float
    param_ep_T = treeinfo.float
    algoId = treeinfo.float
    NStrawHits = treeinfo.int
    NStraw = treeinfo.int
    NStrawTime = treeinfo.int
    NTRHits = treeinfo.int


class L2El (L2egamma):
    Zvtx = treeinfo.float
    err_eta = treeinfo.float
    err_phi = treeinfo.float
    err_Zvtx = treeinfo.float
    err_p_T = treeinfo.float
    trkClusDeta = treeinfo.float
    trkClusDphi = treeinfo.float
    RoIId = treeinfo.int
    trkAlgoId = treeinfo.int

    ID = treeinfo.instance (L2IDcls)

    class L2ElCLcls (L2egamma.CLcls):
        eta = treeinfo.float
        phi = treeinfo.float
        E = treeinfo.float
        Et = treeinfo.float
    CL = treeinfo.instance (L2ElCLcls)


class L2Pho (L2egamma):
    class L2PhCLcls (L2egamma.CLcls):
        e = treeinfo.float
        et = treeinfo.float
    CL = treeinfo.instance (L2PhCLcls)

    Et = treeinfo.float

    HadEt = treeinfo.float
    energyRatio = treeinfo.float
    rCore = treeinfo.float
    dPhi = treeinfo.float
    dEta = treeinfo.float
    isValid = treeinfo.int
    roiId = treeinfo.int



class L2Mu (Trigobj):
    Sigma_p_T = treeinfo.float

    class MFcls (object):
        p_T = treeinfo.float
        radius = treeinfo.float
        eta = treeinfo.float
        phi = treeinfo.float
        dir_phi = treeinfo.float
        zeta = treeinfo.float
        dir_zeta = treeinfo.float
        beta = treeinfo.float
        saddress = treeinfo.int
    MF = treeinfo.instance (MFcls)

    ID = treeinfo.instance (L2IDcls)


class L2Jet (Trigobj):
    #coneRadius = treeinfo.float
    ehad0 = treeinfo.float
    eem0 = treeinfo.float
    pass


class L2Tau (Trigobj):
    # xxx use a 4-vector base casee
    class CLcls (object):
        energy = treeinfo.float
        et = treeinfo.float
        EMCalibEnergy = treeinfo.float
        numStripCells = treeinfo.float
        EMenergy = treeinfo.float
        HADenergy = treeinfo.float
        eta = treeinfo.float
        phi = treeinfo.float
        IsoFrac = treeinfo.float
        stripWidth = treeinfo.float
        eCalib = treeinfo.float
        eEMCalib = treeinfo.float
    #CL = treeinfo.instance (CLcls)
    
    roiId = treeinfo.int
    nMatchedTracks = treeinfo.int
    Zvtx = treeinfo.float
    #err_eta = treeinfo.float
    #err_phi = treeinfo.float
    err_Zvtx = treeinfo.float
    #err_Pt = treeinfo.float
    etCalibCluster = treeinfo.float
    simpleEtFlow = treeinfo.float


class TrigBJet (Trigobj):
    prmVtx = treeinfo.float
    xComb = treeinfo.float
    xIP1D = treeinfo.float
    xIP2D = treeinfo.float
    xIP3D = treeinfo.float
    xCHI2 = treeinfo.float
    xSV = treeinfo.float
    xMVtx = treeinfo.float
    xEVtx = treeinfo.float
    xNVtx = treeinfo.float
    roiId = treeinfo.int
    isValid = treeinfo.int


class EFEgamma (Trigobj):
    class ShowerCls (object):
        e011 = treeinfo.float
        e033 = treeinfo.float
        e131 = treeinfo.float
        e1152 = treeinfo.float
        ethad1 = treeinfo.float
        ethad = treeinfo.float
        ehad1 = treeinfo.float
        f1 = treeinfo.float
        f3 = treeinfo.float
        f1core = treeinfo.float
        f3core = treeinfo.float
        e233 = treeinfo.float
        e235 = treeinfo.float
        e355 = treeinfo.float
        e237 = treeinfo.float
        e277 = treeinfo.float
        e333 = treeinfo.float
        e335 = treeinfo.float
        e337 = treeinfo.float
        e377 = treeinfo.float
        weta1 = treeinfo.float
        weta2 = treeinfo.float
        e2ts1 = treeinfo.float
        e2tsts1 = treeinfo.float
        fracs1 = treeinfo.float
        widths1 = treeinfo.float
        widths2 = treeinfo.float
        poscs1 = treeinfo.float
        poscs2 = treeinfo.float
        asy1 = treeinfo.float
        pos = treeinfo.float
        pos7 = treeinfo.float
        barys1 = treeinfo.float
        wtots1 = treeinfo.float
        emins1 = treeinfo.float
        emaxs1 = treeinfo.float
        etcone = treeinfo.float
        etcone20 = treeinfo.float
        etcone30 = treeinfo.float
        etcone40 = treeinfo.float
        iso = treeinfo.float
        ecore = treeinfo.float
    #Shower = treeinfo.instance (ShowerCls)

    class TrackMatchCls (object):
        EtaCorrMag = treeinfo.float
        EoverP = treeinfo.float
        TracksInBroadWindow = treeinfo.int
        isPrimary = treeinfo.int
        trackRefit = treeinfo.int
    #TrackMatch = treeinfo.instance (TrackMatchCls)

    class bremCls (object):
        D0 = treeinfo.float
        Phi0 = treeinfo.float
        InvpT = treeinfo.float
        Z0 = treeinfo.float
        DzDr = treeinfo.float
        Radius = treeinfo.float
        X = treeinfo.float
        ClusterRadius = treeinfo.float
        FitChi2 = treeinfo.float
        NTRT = treeinfo.int
        NR = treeinfo.int
        NZ = treeinfo.int
        FitStatus = treeinfo.int
        ErrorFlag = treeinfo.int
    #brem = treeinfo.instance (bremCls)

    class convertCls (object):
        TrackMatch = treeinfo.int
        AngleMatch = treeinfo.int
    #convert = treeinfo.instance (convertCls)
    
    #isem = treeinfo.int
    #isemse = treeinfo.int

    etcone20 = treeinfo.float
    ethad1 = treeinfo.float
    e233 = treeinfo.float
    e237 = treeinfo.float
    e277 = treeinfo.float
    weta1 = treeinfo.float
    weta2 = treeinfo.float
    f1 = treeinfo.float
    e2tsts1 = treeinfo.float
    emins1 = treeinfo.float
    wtots1 = treeinfo.float
    fracs1 = treeinfo.float
    f1core = treeinfo.float
    f3core = treeinfo.float
    pos7 = treeinfo.float
    #iso = treeinfo.float
    widths2 = treeinfo.float
    zvertex = treeinfo.float
    errz = treeinfo.float
    etap = treeinfo.float
    depth = treeinfo.float
    IsEM = treeinfo.int
    author = treeinfo.int
    clusterTime = treeinfo.float
    ethad = treeinfo.float
    r33over37allcalo = treeinfo.float


class EFEl (EFEgamma):
    EtaCorrMag = treeinfo.float
    EoverP = treeinfo.float
    deltaEta1 = treeinfo.float
    deltaEta2 = treeinfo.float
    deltaPhi2 = treeinfo.float
    bremInvpT = treeinfo.float
    bremRadius = treeinfo.float
    bremX = treeinfo.float
    bremClusterRadius = treeinfo.float
    trkCharge = treeinfo.float
    trkPt     = treeinfo.float
    trkIpt    = treeinfo.float


class EFPh (EFEgamma):
    pass



class EFMu (Trigobj):
    Charge = treeinfo.float
    MuonCode = treeinfo.int


class EFTau (Trigobj):
    numTrack = treeinfo.float
    emRadius = treeinfo.float
    isolationFrac = treeinfo.float
    centralityFrac = treeinfo.float
    stripWidth2 = treeinfo.float = treeinfo.float
    numStipCells = treeinfo.float
    etEMCalib = treeinfo.float
    etHadCalib = treeinfo.float
    leadingTrackPT = treeinfo.float
    author = treeinfo.int


# Merge with met?
class L1ET (object):
    Ex = treeinfo.float
    Ey = treeinfo.float
    Etsum = treeinfo.float
    ExMiss = treeinfo.float
    EyMiss = treeinfo.float
    def get_pt (self):
        return math.hypot (self.ExMiss, self.EyMiss)
    pt = property (get_pt)
    def get_phi (self):
        return math.atan2 (self.EyMiss, self.ExMiss)
    phi = property (get_phi)


class Passed (object):
    Trigger = treeinfo.int ('%sTrigger')
    L1 = treeinfo.int ('%sL1')
    L2 = treeinfo.int ('%sL2')
    EF = treeinfo.int ('%sEF')
    Passed = treeinfo.int ('Passed')


def wrap_Trigger0 (tt):
    tt.add_list (L1EM, 'L1EM', 50, count = 'N', warnmissing = True)
    #tt.add_list (L1Mu, 'L1Mu', 50, count = 'N', warnmissing = True)
    #tt.add_list (L1Jet, 'L1Jet', 50, count = 'N', warnmissing = True)
    tt.add_list (L2El, 'L2El', 50, count = 'N', warnmissing = True)
    tt.add_list (L2Pho, 'L2Pho', 50, count = 'N', warnmissing = True)
    #tt.add_list (L2Mu, 'L2Mu', 50, count = 'N', warnmissing = True)
    #tt.add_list (L2Jet, 'L2Jet', 50, count = 'N', warnmissing = True)
    #tt.add_list (L2Tau, 'L2Tau', 50, count = 'N', warnmissing = True)
    #tt.add_list (TrigBJet, 'L2BJet', 50, count = 'N', warnmissing = True)
    #tt.add_list (TrigBJet, 'EFBJet', 50, count = 'N', warnmissing = True)
    tt.add_list (EFEl, 'EFEl', 50, count = 'N', warnmissing = True)
    tt.add_list (EFPh, 'EFPh', 50, count = 'N', warnmissing = True)
    #tt.add_list (EFMu, 'EFMu', 50, count = 'N', warnmissing = True)
    #tt.add_list (Trigobj, 'EFJet', 50, count = 'N', warnmissing = True)
    #tt.add_list (EFTau, 'EFTau', 50, count = 'N', warnmissing = True)

    tt.add (PassedL1, 'PassedL1', warnmissing = True)
    tt.add (PassedL2, 'PassedL2', warnmissing = True)
    tt.add (PassedEF, 'PassedEF', warnmissing = True)
    #tt.add (L1ET, 'L1ET', warnmissing = True)
    #tt.add (MET, 'TrigEFMissingET', warnmissing = True)
    tt.add (Counters, "Trigger", warnmissing = True)
    tt.add (Passed, 'Passed', warnmissing = True)
    return


class TruPart (Particle):
    etcone = treeinfo.float
    etcone10 = treeinfo.float
    etcone20 = treeinfo.float
    etcone30 = treeinfo.float
    etcone40 = treeinfo.float
    etcone50 = treeinfo.float
    etcone60 = treeinfo.float
    etcone70 = treeinfo.float
    status = treeinfo.int
    barcode = treeinfo.int


class Mother (object):
    pdgId = treeinfo.int
    barcode = treeinfo.int
    

class ElTru (TruPart):
    class Reco (MatchedParticle):
        etcone20 = treeinfo.float
        IsEM = treeinfo.int
        author = treeinfo.int
    Reco = treeinfo.instance (Reco)
    motherW = treeinfo.instance (Mother)


class MuTru (TruPart):
    class Reco (MatchedParticle):
        etcone10 = treeinfo.float
        etcone20 = treeinfo.float
        etcone30 = treeinfo.float
        etcone40 = treeinfo.float
        nucone10 = treeinfo.float
        nucone20 = treeinfo.float
        nucone30 = treeinfo.float
        nucone40 = treeinfo.float
        author = treeinfo.int
        matchChi2OverDoF = treeinfo.float
        SumTrkPtInCone = treeinfo.float
        hasCluster = treeinfo.int
        hasCombinedMuon = treeinfo.int
        nTrkInCone = treeinfo.int
        matchChi2 = treeinfo.float
    Reco = treeinfo.instance (Reco)
    motherW = treeinfo.instance (Mother)


def stable_name (rootname, vname):
    xx = rootname.split('_')
    thisname = xx[-1]
    rootname = '_'.join(xx[:-1])

    return "%s_stables%s%s" % (rootname, vname, thisname)
def nstable_name (rootname, vname):
    xx = rootname.split('_')
    thisname = xx[-1]
    rootname = '_'.join(xx[:-1])

    return "%s_nStable%s" % (rootname, thisname)
class TauTru (TruPart):
    Reco_VHad = treeinfo.instance (MatchedParticle)

    class VHad (Particle):
        Matched = treeinfo.int
        
        class Stable (object):
            n = treeinfo.float (nstable_name)
            EAll = treeinfo.float (stable_name)
            ETAll = treeinfo.float (stable_name)
            PTAll = treeinfo.float (stable_name)
        compPartIndex = treeinfo.int
        test_FailHasCharge = treeinfo.int
        stablesN = treeinfo.int
        nDaughters = treeinfo.int
        nStableCounted = treeinfo.int
        nStableNoneMissing = treeinfo.int
        tauEta = treeinfo.float
        tauPhi = treeinfo.float
        tauE = treeinfo.float
        tauET = treeinfo.float
        tauPT = treeinfo.float
        tauPx = treeinfo.float
        tauPy = treeinfo.float
        tauPz = treeinfo.float

        Charged   = treeinfo.instance (Stable)
        Pion      = treeinfo.instance (Stable)
        Kaon      = treeinfo.instance (Stable)
        OtherCh   = treeinfo.instance (Stable)
        Neut      = treeinfo.instance (Stable)
        PiZ       = treeinfo.instance (Stable)
        Gamma     = treeinfo.instance (Stable)
        OtherNeut = treeinfo.instance (Stable)
        Stables   = treeinfo.instance (Stable, '%s_')
    VHad = treeinfo.instance (VHad)
    motherW = treeinfo.instance (Mother)


class NuTru (TruPart):
    Reco = treeinfo.instance (MatchedParticle)
    motherW = treeinfo.instance (Mother)


class MatchedJet (Jet, Matched):
    pass


class LQTru (TruPart):
    Reco = treeinfo.instance (MatchedParticle)
    TruJet = treeinfo.instance (MatchedJet)
    motherW = treeinfo.instance (Mother)


class BotTru (TruPart):
    Reco = treeinfo.instance (MatchedParticle)
    TruJet = treeinfo.instance (MatchedJet)
    motherTop = treeinfo.instance (Mother)
    

class WTru (TruPart):
    Reco = treeinfo.instance (MatchedParticle)
    motherTop = treeinfo.instance (Mother)


class TopTru (TruPart):
    Reco = treeinfo.instance (MatchedParticle)



class TruAnaGlobal (object):
    EvtTypeTT = treeinfo.int
    EvtTypeST = treeinfo.int


def wrap_TruthAna0 (tt):
    tt.add_list (ElTru, 'ElTru', 100, count = 'N', warnmissing = True,
                 rootname = 'TruthAna0.El')
    tt.add_list (MuTru, 'MuTru', 100, count = 'N', warnmissing = True,
                 rootname = 'TruthAna0.Mu')
    tt.add_list (TauTru, 'TauTru', 100, count = 'N', warnmissing = True,
                 rootname = 'TruthAna0.Tau')
    tt.add_list (NuTru, 'NuTru', 100, count = 'N', warnmissing = True,
                 rootname = 'TruthAna0.Nu')
    tt.add_list (LQTru, 'LQTru', 100, count = 'N', warnmissing = True,
                 rootname = 'TruthAna0.LQ')
    tt.add_list (BotTru, 'BotTru', 100, count = 'N', warnmissing = True,
                 rootname = 'TruthAna0.Bot')
    tt.add_list (WTru, 'HadW', 100, count = 'N', warnmissing = True,
                 rootname = 'TruthAna0.HadW')
    tt.add_list (WTru, 'LepW', 100, count = 'N', warnmissing = True,
                 rootname = 'TruthAna0.LepW')
    tt.add_list (TopTru, 'LepTop', 100, count = 'N', warnmissing = True,
                 rootname = 'TruthAna0.LepTop')
    tt.add_list (TopTru, 'HadTop', 100, count = 'N', warnmissing = True,
                 rootname = 'TruthAna0.HadTop')
    tt.add (TruAnaGlobal, warnmissing = True)
    tt.add (Counters, "TruthAna", warnmissing = True)
    return


# file can be either a string or a list of file names.
# if the latter, we make chains.
def open_d3pd (file):
    if type(file) == type(""):
        file = ROOT.TFile.Open (file)
        tree_FullReco0 = file.Get ("FullReco0")
        tree_Truth0    = file.Get ("Truth0")
        tree_FullReco0.AddFriend (tree_Truth0)
        tree_TruthAna0    = file.Get ("TruthAna0")
        if tree_TruthAna0:
            tree_FullReco0.AddFriend (tree_TruthAna0)
    else:
        tree_FullReco0 = ROOT.TChain ("FullReco0")
        tree_Truth0    = ROOT.TChain ("Truth0")
        tree_TruthAna0 = ROOT.TChain ("TruthAna0")
        for f in file:
            tree_FullReco0.Add (f + "/FullReco0")
            tree_Truth0.Add (f + "/Truth0")
            tree_TruthAna0.Add (f + "/TruthAna0")
        tree_FullReco0.AddFriend ("Truth0")
        tree_FullReco0.AddFriend ("TruthAna0")
        tree_FullReco0.GetEntry(0)

    selarg = Selarg()
    selarg.have_truth = (tree_Truth0 != None)
    
    tt = make_wrapper (tree_FullReco0, selarg = selarg)
    tt.tree_FullReco0 = tree_FullReco0
    wrap_FullReco0 (tt)
    if tree_Truth0:
        tt.add_friend (tree_Truth0)
        tt.tree_Truth0    = tree_Truth0
        wrap_Truth0 (tt)
    if tree_TruthAna0:
        tt.add_friend (tree_TruthAna0)
        tt.tree_TruthAna0    = tree_TruthAna0
        wrap_TruthAna0 (tt)
    return tt
            

