samplings = [
    "PreSamplerB", "EMB1", "EMB2", "EMB3",       # LAr barrel
    "PreSamplerE", "EME1", "EME2", "EME3",       # LAr EM endcap
    "HEC0", "HEC1", "HEC2", "HEC3",              # Hadronic end cap cal.
    "TileBar0", "TileBar1", "TileBar2",          # Tile barrel
    "TileGap1", "TileGap2", "TileGap3",          # Tile gap (ITC & scint)
    "TileExt0", "TileExt1", "TileExt2",          # Tile extended barrel
    "FCAL0", "FCAL1", "FCAL2",                   # Forward EM endcap
    "Unknown",
    "Cryostat",
    ]


for (i,s) in enumerate (samplings):
    globals()[s] = i

