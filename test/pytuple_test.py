import ROOT
import PyCintex
from pytuple.readtuple import make_wrapper
from pytuple.treeinfo import treeinfo
from array import array


#############################################################################


def make_tuple1():
    f = ROOT.TFile ('tuple1.root', 'recreate')
    t = ROOT.TTree ('tuple12', 'tuple12')

    
    a1 = array ('i', [0])
    t.Branch ('a1', a1, 'a1/I')
    o1_n = array ('i', [0])
    t.Branch ('o1_n', o1_n, 'o1_n/I')
    o1_b1 = getattr (ROOT, 'vector<int>')()
    t.Branch ('o1_b1', o1_b1)

    for i in range(10):
        a1[0] = i
        o1_n[0] = 10-i
        o1_b1.clear()
        for j in range(10-i):
            o1_b1.push_back (j + i*100)
        t.Fill()
        
    t.Write()
    f.Close()
    return


#############################################################################

def make_tuple2():
    f = ROOT.TFile ('tuple2.root', 'recreate')
    t = ROOT.TTree ('tuple12', 'tuple12')

    
    a2 = array ('i', [0])
    t.Branch ('a2', a2, 'a2/I')
    o1_n = array ('i', [0])
    t.Branch ('o1_n', o1_n, 'o1_n/I')
    o1_b2 = getattr (ROOT, 'vector<int>')()
    t.Branch ('o1_b2', o1_b2)

    for i in range(10):
        a2[0] = i
        o1_n[0] = 10-i
        o1_b2.clear()
        for j in range(10-i):
            o1_b2.push_back (j + i*100)
        t.Fill()
        
    t.Write()
    f.Close()
    return


#############################################################################

class Tuple12Global (object):
    a1 = treeinfo.int
    a2 = treeinfo.int (allow_missing = True)


class Tuple12Obj1 (object):
    b1 = treeinfo.int
    b2 = treeinfo.int (allow_missing = True, use_vector = True)


def open_tuple12():
    ch = ROOT.TChain('tuple12')
    ch.Add ('tuple1.root')
    ch.Add ('tuple2.root')

    tt = make_wrapper (ch)
    tt.add (Tuple12Global, warnmissing = True, create = False)
    tt.add_list (Tuple12Obj1, 'o1', 10, warnmissing = True, create = False)

    for i in range (ch.GetEntries()):
        tt.GetEntry(i)
        print i, tt.a1, tt.a2, [(x.b1, x.b2) for x in tt.o1]
    return tt



def loop_tuple1():
    f = ROOT.TFile ('tuple1.root')
    t = f.tuple12
    tt = make_wrapper (t)
    tt.add_list (Tuple12Obj1, 'o1', 10)

    tt.GetEntry(5)
    print [o.b1 for o in tt.o1]
    tt.disable()
    tt.GetEntry(0)
    print [o.b1 for o in tt.o1]

    def loopf (i, tr):
        print i, [o.b1 for o in tr.o1]
        return

    tt.loop (loopf)
    tt.loop (loopf)
    return tt, f

    
#make_tuple1()
#make_tuple2()
tt=open_tuple12()
tt, t=loop_tuple1()


