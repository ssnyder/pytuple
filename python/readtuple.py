# TODO: arrays, instances, references
#     get manip for calling make for t/l obj doesn't work with psyco?
# ??? reset func can be called multiple times for a non-list obj
#     automatically make reset/copy functions?
# ??? be able to (persistently) ref objects instead of copying them?
# ??? Merge with branch used in cluster calibration analysis
# ??? Need better fallback to raw vbl
# ??? If we read a tuple with a vector with a size larger than that
#     provided to add_list, then we get an obscure error.
#     Would be nice to have lists expand automatically,
#     or at least provide a better error.

from __future__ import print_function

import sys
import operator
import ROOT
import cppyy
import atexit
import weakref
#import new
from array import array
from functools import reduce

# Try to import vecbuf from pytuple.
# If that fails, look for libvecbuf in LD_LIBRARY_PATH.
def load_vecbuf():
    return None  # FIXME --- vecbuf not working currently.
    try:
        from pytuple import vecbuf
        return vecbuf
    except ImportError:
        pass

    import imp
    import os
    dirs = os.environ['LD_LIBRARY_PATH'].split(':')
    try:
        d = imp.find_module ('libvecbuf', dirs)
    except ImportError:
        d = None
    if d == None:
        try:
            d = imp.find_module ('libvecbufmodule', dirs)
        except ImportError:
            return None
    return imp.load_module ('vecbuf', d[0], d[1], d[2])
                         
    

vecbuf = load_vecbuf()
if not vecbuf:
    class vector_char:
        def __init__ (self):
            self._v = getattr (ROOT, 'vector<char>')()
            return
        def __getitem__ (self, i):
            x = ord (self._v[i])
            if x >= 128: x -= 128
            return x
        def __setitem__ (self, i, v):
            if v < 0: v += 128
            self._v[i] = chr(v)
            return
        def resize (self, n):
            self._v.resize (n)
            return
        def __len__ (self):
            return self._v.size()
    class vecbuf:
        @staticmethod
        def vecbuf (typ, maxn):
            if typ == 'i':
                buf = getattr (ROOT, 'vector<int>')()
            elif typ == 'u' or typ == 'I':
                buf = getattr (ROOT, 'vector<unsigned int>')()
            elif typ == 'h':
                buf = getattr (ROOT, 'vector<short>')()
            elif typ == 'H':
                buf = getattr (ROOT, 'vector<unsigned short>')()
            elif typ == 'b':
                buf = vector_char()
            elif typ == 'B':
                buf = getattr (ROOT, 'vector<unsigned char>')()
            elif typ == 'd':
                buf = getattr (ROOT, 'vector<double>')()
            elif typ == 'f':
                buf = getattr (ROOT, 'vector<float>')()
            elif typ == 'l':
                buf = getattr (ROOT, 'vector<long>')()
            elif typ == 'L':
                buf = getattr (ROOT, 'vector<unsigned long>')()
            elif typ == '@':
                buf = getattr (ROOT, 'vector<bool>')()
            else:
                print ('Error: unknown type code', buf)
                assert 0
            buf.resize (maxn)
            return buf

from pytuple.treeinfo import treeinfo

kDoNotProcess = 1024


# PyROOT objects need special handling for SetBranchAddress.
# For TTree::SetBranchAddress, the pythonization process adds
# a wrapper around this method that handles this properly.
# However, TChain has its own TChain::SetBranchAddress, and this
# does not get pythonized.  Thus, if you call SetBranchAddress(name, obj)
# on a chain, if obj is a pyroot object, then what gets passed is the
# address of the object directly, not, as is actually required, the
# address of a pointer to the object.  We work around this here.
def _set_branch_address (tree, branchname, buf):
    if isinstance (tree, ROOT.TChain):
        # See if buf is a pyroot object; i.e., does it have
        # PyRootType as a metaclass.  Note, though, that in 5.18,
        # PyRootType actually appears as a meta-meta-class.
        if (type(type(buf)) == ROOT.PyRootType or
            type(type(type(buf))) == ROOT.PyRootType):
            buf = ROOT.AddressOf (buf)
        elif type(type(cppyy.gbl))==type(type(type(buf))):
            buf = ROOT.AddressOf (buf)
    tree.SetBranchAddress (branchname, buf)
    return


def _set_address (branch, buf):
    if 'Aux.' in branch.GetName():
        branch.SetMakeClass(True)
    else:
        # Handle vector_char, etc.
        if hasattr(buf, '_v'): buf = buf._v

        if (hasattr(ROOT,'PyRootType') and
            (type(type(buf)) == ROOT.PyRootType or
             type(type(type(buf))) == ROOT.PyRootType)):
            buf = ROOT.AddressOf (buf)
        elif type(type(cppyy.gbl))==type(type(type(buf))):
            buf = ROOT.AddressOf (buf)
    if isinstance (buf, ULongbuf):
        buf = buf.a
    branch.SetAddress (buf)
    return


class ULongbuf (object):
    def __init__ (self):
        self.a = array ('L', [0,0])
        return

    def __getitem__ (self, i):
        return self.a[0] + (self.a[1]<<32)

    def __setitem__ (self, i, v):
        self.a[0] = v&((1<<32)-1)
        self.a[1] = v>>32
        return

    def __getattr__ (self, a):
        return getattr (self.a, a)

    def __len__ (self):
        return len (self.a)


_typecodes = {'Int_t' : 'i',
              'UInt_t' : 'I',
              'Char_t' : 'b',
              'Float_t' : 'f',
              'Bool_t'  : 'i',
              'Double_t' : 'd',
              'Long64_t' : 'l',
              'ULong64_t' : 'L',
              }
def _buf_from_typename (type, maxn):
    buf = None
    if type == "vector<double>":
        #buf = getattr(ROOT, type)()
        #buf.resize (maxn)
        buf = vecbuf.vecbuf ('d', maxn)
    elif type == "vector<float>":
        buf = vecbuf.vecbuf ('f', maxn)
    elif type == "vector<int>":
        buf = vecbuf.vecbuf ('i', maxn)
    elif type == "vector<unsigned int>":
        buf = vecbuf.vecbuf ('I', maxn)
    elif type == "vector<short>":
        buf = vecbuf.vecbuf ('h', maxn)
    elif type == "vector<unsigned short>":
        buf = vecbuf.vecbuf ('H', maxn)
    elif type == "vector<long>":
        buf = vecbuf.vecbuf ('l', maxn)
    elif type == "vector<unsigned long>":
        buf = vecbuf.vecbuf ('L', maxn)
    elif type == "vector<ULong64_t>":
        # FIXME: this uses `unsigned long', which is not guaranteed to be 64-bit
        buf = vecbuf.vecbuf ('L', maxn)
    elif type == "vector<char>":
        buf = vecbuf.vecbuf ('b', maxn)
    elif type == "vector<unsigned char>":
        buf = vecbuf.vecbuf ('B', maxn)
    elif type == "vector<bool>":
        buf = vecbuf.vecbuf ('@', maxn)
    elif type == "vector<vector<double> >":
        # XXX temporary?
        buf = getattr(ROOT, type)()
        buf.resize (maxn)
    elif type == "vector<vector<float> >":
        # XXX temporary?
        buf = getattr(ROOT, type)()
        buf.resize (maxn)
    elif type == "vector<vector<int> >":
        # XXX temporary?
        buf = getattr(ROOT, type)()
        buf.resize (maxn)
    elif type == "vector<vector<unsigned int> >":
        # XXX temporary?
        buf = getattr(ROOT, type)()
        buf.resize (maxn)
    elif type == "vector<string>":
        # XXX temporary?
        buf = getattr(ROOT, type)()
        buf.resize (maxn)
    elif type.startswith ('vector<ElementLink'):
        # XXX temporary?
        buf = getattr(ROOT, type)()
        buf.resize (maxn)
    elif type.startswith ('vector<vector<ElementLink'):
        # XXX temporary?
        buf = getattr(ROOT, type)()
        buf.resize (maxn)
    elif type == 'ULong64_t':
        buf = ULongbuf()
    return buf
def make_buffer_from_branch (branch, tree, maxn):
    #pypy compat
    #leaves = list (branch.GetListOfLeaves())
    leaves = list(branch.GetListOfLeaves())
    if len (leaves) != 1:
        print ('Branch', branch.GetName(), 'has multiple leaves; not handled')
        return None
    leaf = leaves[0]
    type = leaf.GetTypeName()
    buf = _buf_from_typename (type, maxn)
    if not buf and hasattr(branch, 'GetInfo'):
        buf = _buf_from_typename (branch.GetInfo().GetName(), maxn)
    if not buf:
        typecode = _typecodes.get (leaf.GetTypeName())
        if typecode == None:
            print ('Branch', branch.GetName(), 'has unknown type',leaf.GetTypeName())
            return None
        n = maxn
        count = leaf.GetLeafCount()
        if count:
            n = int (max (maxn, count.GetMaximum()))
        else:
            n = int (max (maxn, leaf.GetLen()))
        buf = array (typecode, n*[0])
    oldstat = branch.TestBit (kDoNotProcess)
    branch.ResetBit (kDoNotProcess)
    _set_address (branch, buf)
    if oldstat:
        branch.SetBit (kDoNotProcess)
    return buf


def make_buffer_from_ti (ti, maxn):
    if ti.use_vector():
        return vecbuf.vecbuf (ti.typecode(), maxn)
    return array (ti.typecode(), ti.zero (maxn))


def make_branch (tree, ti, branchname, count = None):
    desc = branchname
    if count:
        desc = desc + '[' + count + ']'
    desc = desc + '/' + ti.rootcode()
    return tree.Branch (branchname, 0, desc)


# Generic version.
#  Functions defined in derived class:
#    enable_branch
#    post_reset
class Descrip_Base (object):
    def __init__ (self, buf, leafname):
        self.buf = buf
        self.ptr = [self]
        self.makefunc = None
        self.resetfunc = None
        self.enabled = False
        self.leafname= leafname
        self.is_vecbuf = hasattr (self.buf, 'set_enable_cb')
        if self.is_vecbuf:
            self.buf.set_enable_cb (self.enable_branch)
        return

    def _getitem__enable (self, i):
        self.ptr[0] = self.buf
        self.enable_branch ()
        return self.buf[i]

    def _setitem__enable (self, i, val):
        self.ptr[0] = self.buf
        self.enable_branch ()
        self.buf[i] = val
        return

    def _len__enable (self):
        self.ptr[0] = self.buf
        self.enable_branch ()
        return len(self.buf)


    def _getitem__reset (self, i):
        self.ptr[0] = self.buf
        if self.makefunc:
            self.makefunc ()
        self.post_reset ()
        return self.buf[i]

    def _setitem__reset (self, i, val):
        self.ptr[0] = self.buf
        self.post_reset ()
        self.buf[i] = val
        return

    def _len__reset (self):
        self.ptr[0] = self.buf
        if self.makefunc:
            self.makefunc ()
        self.post_reset ()
        return len(self.buf)


    def disable (self):
        if self.is_vecbuf:
            self.buf.reset()
        self.ptr[0] = self
        self.enabled = False
        return

    def reset (self):
        self.resetfunc()
        self.ptr[0] = self
        return


    def makeprop (self, host, i, flat=False):
        if flat and 'cppyy.gbl.std.vector' in repr(type(self.buf)):
            def get (dum, arr=self.ptr):
                return list(arr[0])
            get.__name__ = 'get_' + self.leafname
            setattr (host, get.__name__, get)

            set = None
            
        elif self.is_vecbuf:
            get = vecbuf.vecbufGetter (self.buf, i)
            setattr (host, 'get_' + self.leafname, get)

            set = vecbuf.vecbufSetter (self.buf, i)
            setattr (host, 'set_' + self.leafname, set)
        else:
            def get (dum, arr=self.ptr, i=i):
                return arr[0][i]
            get.__name__ = 'get_' + self.leafname
            setattr (host, get.__name__, get)

            def set (dum, val, arr=self.ptr, i=i):
                arr[0][i] = val
                return
            set.__name__ = 'set_' + self.leafname
            setattr (host, set.__name__, set)

        setattr (host, self.leafname, property (get, set))
        return


# Version relying on hacking function internals to speed things up.
# Idea is to try to get rid of the outer subscript reference in get().
# get_X: bound method
#  im_func points to the function.
#   func_code points to the code object.
#   This code object is shared by all get functions for this Descrip.
#     co_consts is the constants.  This contains the array
#     object we dereference, either self of buf.  When we want
#     to change, we replace the consts object.
# In the end, doesn't give much of a speed improvement.
# Don't bother with this for now...
# class Descrip_Base_Funchack (object):
#     def __init__ (self, buf, leafname):
#         self.buf = buf
#         self.ptr = [self]
#         self.makefunc = None
#         self.resetfunc = None
#         self.enabled = False
#         self.leafname= leafname

#         def get_shared (dum, i=0):
#             return 'dummy'[i]
#         code = get_shared.func_code
#         self._get_shared_code = new.code (code.co_argcount,
#                                           code.co_nlocals,
#                                           code.co_stacksize,
#                                           code.co_flags,
#                                           code.co_code,
#                                           code.co_consts,
#                                           code.co_names,
#                                           code.co_varnames,
#                                           'dummy-get-filename',
#                                           'dummy-get-name',
#                                           code.co_firstlineno,
#                                           code.co_lnotab)
#         self._enabled_consts = (None, buf)
#         self._disabled_consts = (None, self)
#         vecbuf.munge_consts (self._get_shared_code, self._disabled_consts)
#         return

#     def _getitem__enable (self, i):
#         self.ptr[0] = self.buf
#         vecbuf.munge_consts (self._get_shared_code, self._enabled_consts)
#         self.enable_branch ()
#         return self.buf[i]

#     def _setitem__enable (self, i, val):
#         self.ptr[0] = self.buf
#         vecbuf.munge_consts (self._get_shared_code, self._enabled_consts)
#         self.enable_branch ()
#         self.buf[i] = val
#         return

#     def _len__enable (self):
#         self.ptr[0] = self.buf
#         vecbuf.munge_consts (self._get_shared_code, self._enabled_consts)
#         self.enable_branch ()
#         return len(self.buf)



#     def _getitem__reset (self, i):
#         self.ptr[0] = self.buf
#         vecbuf.munge_consts (self._get_shared_code, self._enabled_consts)
#         if self.makefunc:
#             self.makefunc ()
#         self.post_reset ()
#         return self.buf[i]

#     def _setitem__reset (self, i, val):
#         self.ptr[0] = self.buf
#         vecbuf.munge_consts (self._get_shared_code, self._enabled_consts)
#         self.post_reset ()
#         self.buf[i] = val
#         return

#     def _len__reset (self):
#         self.ptr[0] = self.buf
#         vecbuf.munge_consts (self._get_shared_code, self._enabled_consts)
#         self.post_reset ()
#         return len(buf[i])

#     def disable (self):
#         if self.is_vecbuf:
#             self.buf.reset()
#         self.ptr[0] = self
#         vecbuf.munge_consts (self._get_shared_code, self._disabled_consts)
#         self.enabled = False
#         return

#     def reset (self):
#         if self.is_vecbuf:
#             self.buf.reset()
#         self.resetfunc()
#         self.ptr[0] = self
#         vecbuf.munge_consts (self._get_shared_code, self._disabled_consts)
#         return


#     def makeprop (self, host, i):
#         def get (dum, i=i):
#             return 'dummy'[i]
#         get.__name__ = 'get_' + self.leafname
#         get.func_code = self._get_shared_code
#         self._get_func = get
#         setattr (host, get.__name__, get)

#         def set (dum, val, arr=self.ptr, i=i):
#             arr[0][i] = val
#             return
#         set.__name__ = 'set_' + self.leafname
#         setattr (host, set.__name__, set)

#         setattr (host, self.leafname, property (get, set))
#         return


class Descrip (Descrip_Base):
    def __init__ (self, buf, treewrapper, branchname, leafname, found,
                  treename, maxn):
        super(Descrip, self).__init__ (buf, leafname)
        self.branchname = branchname
        self.treewrapper = treewrapper
        self.found = found
        self.treename = treename
        self.maxn = maxn
        treewrapper._allbranches[branchname] = self
        return


    def post_reset (self):
        if self.resetfunc:
            self.treewrapper.add_reset (self.reset)
        return
    
    def enable_branch (self):
        if not self.branchname: return

        if self.enabled: return
        self.enabled = True

        branchname = self.branchname
        print ('enable', branchname)

        branch = None
        if not self.treename:
            for tree in self.treewrapper._realtrees:
                branch = tree.GetBranch (branchname)
                if branch != None: break
            if branch:
                self.treename = branch.GetTree().GetName()

        treename = self.treename
        if not treename:
            self.treewrapper._missing_branches.append ((branchname, self))
            self.clearbuf()
            return

        self.treewrapper._enabled_branches.setdefault (treename, []) . \
                                                      append ((self.branchname,
                                                               self))

        if not branch:
            tree = self.treewrapper._realmap[treename]
            branch = tree.GetBranch (branchname)

        if not branch:
            self.clearbuf()
            return

        ##branch.GetTree().AddBranchToCache(branch)

        dumtree = self.treewrapper._dummap[treename]
        dumtree.GetListOfBranches().Add (branch)
        #_set_branch_address (tree, branchname, self.buf)
        _set_address (branch, self.buf)
        branch.ResetReadEntry()

        #branch.GetTree().AddBranchToCache (branch)

        # We're likely not at the correct entry.
        # Move to the proper place.
        entry = dumtree.GetReadEntry()
        branch.GetEntry (entry)

        return


    def set_resetfunc (self, resetfunc):
        if not self.found and resetfunc:
            self.resetfunc = resetfunc
        return self.found

    def maxbuflen (self):
        return len (self.buf)


    def clearbuf (self):
        buf = self.buf
        if hasattr (buf, 'resize'):
            buf.resize (0)
            buf.resize (self.maxn)
        else:
            for i in range(len(buf)):
                buf[i] = 0
        return


class Descrip_Instance (object):
    missing = False
    def __init__ (self, cls, descs, leafname, prefix):
        self.cls = cls
        self.descs = descs
        self.leafname = leafname
        self.prefix = prefix
        return
    
    def set_resetfunc (self, resetfunc):
        anyfound = False
        for d in self.descs:
            anyfound = d.set_resetfunc (resetfunc) or anyfound
        return anyfound

    def disable (self):
        for d in self.descs:
            d.disable()
        return

    def maxbuflen (self):
        return max ([d.maxbuflen() for d in self.descs])


    def makeprop (self, host, i, flat=False):
        inst = make_instance (self.cls,
                              self.prefix,
                              i,
                              self.descs)
        setattr (host, self.leafname, inst)
        return


def find_base_descr (leafname, branchname, base_descrs):
    for d in base_descrs:
        if d.leafname == leafname:
            return d
    return None

def _find_branch (treewrapper, ti, rootname, leafname, is_DxAOD, remap):
    if leafname in remap:
        leafname = remap[leafname]
    branchname = ti.branchname (rootname, leafname, is_DxAOD)
    branch = None
    for tree in treewrapper._realtrees:
        branch = tree.GetBranch (branchname)
        if not not branch: break
    return (branchname, branch, tree)

def make_descr (treewrapper, ti, rootname, leafname, maxn, hostobj,
                count = None, create=1, warnmissing = False,
                is_DxAOD = False,
                base_descrs = [],
                remap = {}):
    if not is_DxAOD:
        (branchname, branch, tree) = _find_branch (treewrapper, ti, rootname, leafname, None, remap)
    else:
        for ix in is_DxAOD.split(','):
            (branchname, branch, tree) = _find_branch (treewrapper, ti, rootname, leafname, ix, remap)
            if not not branch: break
    found = False
    d = None
    treename = None
    if branch:
        found = True
    if not branch:
        d = find_base_descr (leafname, branchname, base_descrs)
    if d == None:
        branch_missing = False
        if not branch and warnmissing and not ti.allow_missing():
            print ("WARNING: missing branch", branchname)
            branch_missing = True
        if not branch and create:
            branch = make_branch (tree, ti, branchname, count)
        if branch:
            buf = make_buffer_from_branch (branch, tree, maxn)
            treename = branch.GetTree().GetName()
        else:
            buf = make_buffer_from_ti (ti, maxn)
            if not ti.allow_missing():
                branchname = None
        class Descrip_Specific (Descrip):
            missing = branch_missing
        if found or ti.allow_missing():
            Descrip_Specific.__getitem__ = Descrip._getitem__enable
            Descrip_Specific.__setitem__ = Descrip._setitem__enable
            Descrip_Specific.__len__     = Descrip._len__enable
        else:
            Descrip_Specific.__getitem__ = Descrip._getitem__reset
            Descrip_Specific.__setitem__ = Descrip._setitem__reset
            Descrip_Specific.__len__     = Descrip._len__reset
        d = Descrip_Specific (buf, treewrapper, branchname, leafname, found,
                              treename, maxn)

    setattr (hostobj, leafname, d.buf)
    setattr (hostobj, '_desc_' + leafname, d)
    return d


class Instance_Host:
    pass
def make_descr_instance (treewrapper,
                         ti,
                         rootname,
                         prefname,
                         maxn,
                         hostobj,
                         countdesc_name,
                         create,
                         warnmissing = False,
                         is_DxAOD = False,
                         base_descrs = []):
    name = ti.prefix (rootname, prefname)
    newhost = Instance_Host()
    setattr (hostobj, prefname, newhost)
    d = Descrip_Instance (ti.cls(),
                          make_descrs (ti.cls(),
                                       treewrapper,
                                       name,
                                       maxn,
                                       newhost,
                                       countdesc_name,
                                       create,
                                       warnmissing = warnmissing,
                                       is_DxAOD = is_DxAOD,
                                       base_descrs = base_descrs,
                                       remap = ti.remap()),
                          prefname,
                          name)
    setattr (hostobj, '_desc_' + prefname, d)
    return d


def make_descrs (cls, treewrapper, name, maxn, hostobj,
                 countdesc_name, create,
                 warnmissing = False,
                 is_DxAOD = False,
                 base_descrs = [],
                 remap = {}):
    descs = reduce (operator.add, [make_descrs (b,
                                                treewrapper,
                                                name,
                                                maxn,
                                                hostobj,
                                                countdesc_name,
                                                create=create,
                                                warnmissing = warnmissing,
                                                is_DxAOD = is_DxAOD,
                                                base_descrs = base_descrs,
                                                remap = remap)
                                   for b in cls.__bases__], [])

    for (vname, val) in cls.__dict__.items():
        
        if isinstance (val, treeinfo._ti_type):
            if not val.select (treewrapper.selarg):
                continue
            desc = make_descr (treewrapper,
                               val,
                               name,
                               vname,
                               maxn,
                               hostobj,
                               countdesc_name,
                               create = create,
                               warnmissing = warnmissing,
                               is_DxAOD = is_DxAOD,
                               base_descrs = base_descrs,
                               remap = remap)
            descs.append (desc)

        elif isinstance (val, treeinfo.instance):
            if not val.select (treewrapper.selarg):
                continue
            desc = make_descr_instance (treewrapper,
                                        val,
                                        name,
                                        vname,
                                        maxn,
                                        hostobj,
                                        countdesc_name,
                                        create,
                                        warnmissing = warnmissing,
                                        is_DxAOD = is_DxAOD,
                                        base_descrs = base_descrs)
            descs.append (desc)
    return descs



def make_instance (cls, name, i, descrs, flat=False):
    pname = name
    if not pname: pname = cls.__name__
    class Specific (cls):
        def __repr__ (self):
            return '<%s(%d)>' % (pname, i)
    Specific.__name__ = cls.__name__ + '_Specific'
    for d in descrs:
        d.makeprop (Specific, i, flat=flat)
    # Note: don't run __init__.
    #  If this assigns to instance variables, this can trigger
    #  enabling the branch, which can have side effects such as changing
    #  the size of our underlying buffers (if vecbuf is being used).
    return Specific.__new__(Specific)


class Objbase (object):
    def disable (self):
        for d in self._descs:
            d.disable()
        return


_no_reset = "_no_reset"
class Obj (Objbase):
    def __init__ (self, cls, name, treewrapper, create = True,
                  resetfunc = _no_reset,
                  warnmissing = False,
                  is_DxAOD = False,
                  base_descrs = []):
        self._cls = cls
        self._name = name

        self._descs = make_descrs (cls, treewrapper, name, 1, self,
                                   None, create,
                                   warnmissing = warnmissing,
                                   is_DxAOD = is_DxAOD,
                                   base_descrs = base_descrs)
        self._instance = make_instance (cls, name, 0, self._descs, flat=True)

        if resetfunc == _no_reset:
            resetfunc = None
        elif resetfunc == None:
            if hasattr (self._instance, 'reset'):
                resetfunc = getattr (self._instance, 'reset')
        else:
            resetfunc = lambda obj=self._instance, f=resetfunc: f(obj)

        self.anyfound = False
        for d in self._descs:
            self.anyfound = d.set_resetfunc (resetfunc) or self.anyfound
        
        return
    
    def __repr__ (self):
        return "<%s(%s)>" % (self._cls.__name__, self._name)


class Objtuple (tuple):
    def new (self, *args, **kw):
        list = self._objlist
        i = list._countdesc.buf[0]
        if i >= len (list._instances):
            raise IndexError
        i = i + 1
        list._countdesc.buf[0] = i
        list._countdesc.post_reset()
        obj = list._instances[i-1]
        if self._resetfunc:
            self._resetfunc (obj)
        if hasattr (obj, '__init__'):
            # Cache this test?
            obj.__init__ (*args, **kw)
        return obj


class Objlist (Objbase):
    def __init__ (self, cls, name, treewrapper, maxn, count, create = True,
                  resetlist = True,
                  resetfunc = _no_reset,
                  makefunc = None,
                  warnmissing = False,
                  count_ti = treeinfo.int,
                  is_DxAOD = False,
                  base_descrs = []):
        self._cls = cls
        self._name = name

        if is_DxAOD or count == None:
            countdesc = None
            countdesc_branch = None
        else:
            countdesc = make_descr (treewrapper, count_ti, name, count, 1,
                                    self,
                                    create = create,
                                    warnmissing = warnmissing,
                                    base_descrs = base_descrs)
            countdesc_branch = countdesc.branchname

            if not countdesc.found:
                if resetlist:
                    countdesc.resetfunc = self.reset

                if makefunc:
                    countdesc.makefunc = \
                         lambda list=self: makefunc (list._linstances[0])

        self._descs = make_descrs (cls, treewrapper, name, maxn, self,
                                   countdesc_branch,
                                   create,
                                   warnmissing = warnmissing,
                                   is_DxAOD = is_DxAOD,
                                   base_descrs = base_descrs)

        if countdesc:
            self._countdesc = countdesc
            self._descs.append (countdesc)
        else:
            idesc = 1
            while self._descs[-idesc].missing:
                idesc += 1
                if idesc > len(self._descs):
                    print ('problem with count for', name)
            self._countdesc = self._descs[-idesc]

        ninst = max ([d.maxbuflen() for d in self._descs])
        ninst = max (ninst, maxn)
        # Sanity check
        if ninst > 100000:
            print ('ninst too large???')
            ninst = 100000

        self._instances = [make_instance (cls, name, i, self._descs)
                           for i in range(ninst)]

        if resetfunc == _no_reset:
            resetfunc = None
        elif resetfunc == None:
            if hasattr (cls, 'reset'):
                resetfunc = getattr (cls, 'reset')

        self._linstances = [Objtuple(self._instances[:i])
                            for i in range(ninst+1)]
        for ot in self._linstances:
            ot._objlist = self
            ot._resetfunc = resetfunc
            ot.tree = treewrapper

        return



    def __repr__ (self):
        return "<%s-list(%s)>" % (self._cls.__name__, self._name)


    def reset (self):
        self._countdesc.buf[0] = 0
        return


    
class Treewrapper (object):
    trees = None

    @staticmethod
    def disable_trees ():
        for t in Treewrapper.trees:
            tt = t()
            if tt:
                tt.cleanup()
        return

    
    def __init__ (self, tree, selarg = None):
        tree.LoadTree(0)
        self.tree = tree
        self._cleaned = False
        self._notifyfuncs = []
        self._resetfuncs = []
        self._enabled_branches = {}
        self._missing_branches = []
        self._transientobjs = {}
        self._dependson = {}
        self._realtrees = [ tree ]
        self._realmap = { tree.GetTree().GetName() :  self._realtrees[0] }
        self._dumtrees = [ self.make_dumtree (tree) ]
        self._dummap = { tree.GetTree().GetName() :  self._dumtrees[0] }
        self._allbranches = {}
        self._children = []
        self.selarg = selarg

        # The dummy trees don't actually own the branches.
        # But if we just let their dtors run, they'll try to delete them.
        # So try to make sure that they get cleared out before
        # we exit.  (We can't rely on __del__ for this; due to cycles,
        # it may not be run.)
        if Treewrapper.trees == None:
            atexit.register (Treewrapper.disable_trees)
            Treewrapper.trees = []

        Treewrapper.trees.append (weakref.ref (self))
        return


    def __del__ (self):
        if not self._cleaned:
            self.disable()
        return


    def make_dumtree (self, tree):
        dumname = 'dum_' + tree.GetName()
        d = ROOT.TTree (dumname, dumname)
        d.GetListOfBranches().SetOwner(False)
        # xxx set this properly!
        d.SetEntries (999999999)
        return d


    def add_friend (self, ftree):
        self._realtrees.append (ftree)
        self._realmap[ftree.GetName()] = ftree
        ftree.GetTree().SetTreeIndex (None)
        dumtree = self.make_dumtree (ftree)
        self._dumtrees.append (dumtree)
        self._dummap[ftree.GetTree().GetName()] = dumtree
        return


    def GetEntry (self, i):
        self.reset()
        treenums = [t.GetTreeNumber() for t in self._realtrees]
        for (j, tnum) in enumerate (treenums):
            dumt = self._dumtrees[j]
            realt = self._realtrees[j]
            realt.LoadTree(i)
            if realt.GetTreeNumber() != tnum:
                bl = dumt.GetListOfBranches()
                bl.Clear()
                tnew = realt.GetTree()
                tname = tnew.GetName()
                tnamed = tname + '.'
                mb = self._missing_branches
                self._missing_branches = []
                for (bb, dsc) in mb:
                    if bb.startswith (tnamed):
                        bb = bb[len(tnamed):]
                    if tnew.GetBranch (bb):
                        self._enabled_branches.setdefault (tname, []). \
                                                          append ((bb, dsc))
                    else:
                        self._missing_branches.append ((bb, dsc))

                for (bb, dsc) in self._enabled_branches.get(tnew.GetName(),[]):
                    if bb.startswith (tnamed):
                        bb = bb[len(tnamed):]
                    br = tnew.GetBranch (bb)
                    if br:
                        bl.Add (br)
                        _set_address (br, dsc.buf)
                    else:
                        dsc.clearbuf()
                self.notify()

            dumt.GetEntry (i - realt.GetChainEntryNumber (0))
        return


    def Fill (self):
        self.tree.Fill ()
        return self.reset()


    def __getitem__ (self, i):
        self.GetEntry (i)
        return self


    # xxx doesn't work!
    def loop_unopt (self, f, lo = 0, hi = 999999999):
        self.disable()
        self.tree.GetListOfBranches().Clear()
        if hi > self.tree.GetEntries():
            hi = self.tree.GetEntries()
        reset = self.reset
        getentry = self.tree.GetEntry
        for i in xrange(lo, hi):
            reset()
            getentry (i)
            f (i, self)
        return


    def loop_opt1 (self, f, lo, hi):
        reset = self.reset
        gelist = [lambda e,t=t,rt=rt,offs=rt.GetChainEntryNumber(0): \
                      (t.GetEntry (e-offs), rt.LoadTree(e)) # rt call needed for TTreeCache.
                  for (t, rt) in zip (self._dumtrees, self._realtrees)]
        for i in range(lo, hi):
            # xxx can optimize this more!
            reset()
            for ge in gelist: ge(i)
            f (i, self)
        return


    def loop_opt (self, f, lo = 0, hi = 999999999, nlearn = 10):
        self.disable()
        if self.tree.GetEntries() == 0: return
        status = None
        if isinstance (self.tree, ROOT.TChain):
            status = self.tree.GetStatus()
            status_save = list (status)
        try:
            #if hi > self.tree.GetEntries():
            #    hi = self.tree.GetEntries()
            if status: status.Clear()
            while lo < hi:
                self.GetEntry (lo)
                this_hi = hi
                for t in self._realtrees:
                    ent = t.LoadTree (lo)
                    if ent < 0: break
                    nent = t.GetTree().GetEntries()
                    this_hi = min (this_hi, lo-ent+nent)
                if ent < 0: break
                if nlearn > 0:
                    this_hi = min (this_hi, lo + nlearn)
                self.loop_opt1 (f, lo, this_hi)
                # if nlearn > 0:
                #     nlearn -= (this_hi - lo)
                #     if nlearn <= 0:
                #         for t in self._realtrees:
                #             t.StopCacheLearningPhase()
                lo = this_hi
        finally:
            if status:
                status.Clear()
                for s in status_save: status.Add (s)
        return
    

    loop = loop_opt


    def add_list (self, cls, name, maxn, count = 'n', create = True,
                  resetlist = True, resetfunc = None,
                  makefunc = None,
                  warnmissing = False,
                  rootname = None,
                  count_ti = treeinfo.int):
        if rootname == None:
            rootname = name
        list = Objlist (cls, rootname, self, maxn, count,
                        create = create,
                        resetlist = resetlist,
                        resetfunc = resetfunc,
                        makefunc = makefunc,
                        warnmissing = warnmissing,
                        count_ti = count_ti)
        if count == None:
            idesc = 1
            while list._descs[-idesc].missing:
                idesc += 1
            def get (dum, ll=list._linstances, nn=list._descs[-idesc].ptr):
                return ll[len(nn[0])]
        else:
            def get (dum, ll=list._linstances, nn=list._descs[-1].ptr):
                return ll[nn[0][0]]
        get.__name__ = 'get_' + name

        setattr (self.__class__, name, property (get))
        setattr (self, name + '_list', list)
        return


    def add_xaod_list (self, cls, name, maxn, count = 'n', create = False,
                       resetlist = True, resetfunc = None,
                       makefunc = None,
                       warnmissing = True,
                       rootname = None,
                       count_ti = None,
                       base_name = None,
                       xaodpref = 'AuxDyn,Aux'):
        if rootname == None:
            rootname = name

        base_descrs = []
        if base_name:
            baselist = getattr (self, base_name + '_list', None)
            if baselist:
                base_descrs = baselist._descs
            else:
                print ("WARNING: Can't find base object', base_name, 'for', name")
                
        list = Objlist (cls, rootname, self, maxn, count,
                        create = create,
                        resetlist = resetlist,
                        resetfunc = resetfunc,
                        makefunc = makefunc,
                        warnmissing = warnmissing,
                        count_ti = count_ti,
                        is_DxAOD = xaodpref,
                        base_descrs = base_descrs)
        def get (dum, ll=list._linstances, nn=list._descs[-1].ptr):
            return ll[len(nn[0])]
        get.__name__ = 'get_' + name

        setattr (self.__class__, name, property (get))
        setattr (self, name + '_list', list)
        return


    def add (self, cls, name = '', create = True, resetfunc = None,
             rootname = None,
             makefunc = None,
             warnmissing = False,
             is_DxAOD = False):
        if rootname == None:
            rootname = name

        obj = Obj (cls, rootname, self, create = create, resetfunc = resetfunc,
                   warnmissing = warnmissing,
                   is_DxAOD = is_DxAOD)
        pname = name
        if not pname: pname = cls.__name__
        setattr (self, pname + '_obj', obj)

        if makefunc and not obj.anyfound:
            def get (tree,
                     obj=obj,
                     makefunc=makefunc,
                     treecls=self.__class__,
                     pname=pname):
                setattr (treecls, pname, obj)
                makefunc (obj)
                tree.add_reset (obj._reset)
                return obj
            prop = property (get)
            setattr (self, pname + '_prop', prop)
            setattr (self.__class__, pname, prop)
            def _reset (obj=obj, pname=pname, treecls=self.__class__,
                       prop=prop):
                setattr (treecls, pname, prop)
                return
            obj._reset = _reset
        else:
            setattr (self, pname, obj._instance)
            
        if not name:
            for d in obj._descs:
                d.makeprop (self.__class__, 0, flat=True)
        return


    def add_xaod (self, cls, name = '', warnmissing = False,
                  rootname = None,
                  xaodpref = 'AuxDyn,Aux'):
        return self.add (cls, name=name, warnmissing=warnmissing,
                         rootname = rootname,
                         create = False,
                         is_DxAOD = xaodpref)

    #def add_raw (self, name, ti, maxn = 1, warnmissing = False):
    #    class cls (object):
    #        pass
    #    cls.__name__ = name
    #    setattr (cls, name, ti (name))
    #    o = Obj (cls, name, self, warnmissng = warnmissing, create = False)
    #    setattr (self, pname + '_obj', obj)
    #    setattr (self, name, getattr (obj, '_desc_' + name)
    #    return


    def add_transient (self, name, makefunc, *nameargs, **kw):
        obj = [None, makefunc, nameargs, kw]
        def get (tree, obj=obj, makefunc=makefunc):
            x = obj[0]
            if x == None:
                in_objs = [getattr(tree, name) for name in nameargs]
                x = makefunc (tree, *in_objs, **kw)
                obj[0] = x
            return x
        setattr (self.__class__, name, property (get))
        self._transientobjs[name] = obj
        for a in nameargs:
            self._dependson.setdefault (a, set()).add (name)

        for c in self._children:
            c.add_transient_if_depends (name, makefunc, *nameargs, **kw)
        return


    def get_transient (self, name):
        obj = self._transientobjs.get (name)
        if obj:
            return (obj[1], obj[2], obj[3])
        return None


    def dependson (self, name):
        return self._dependson.get (name, set())


    def disable (self):
        for v in self.__dict__.values():
            if isinstance (v, Objbase) or isinstance (v, Descrip_Base):
                v.disable()
        for t in self._dumtrees:
            if t and t.GetListOfBranches():
                t.GetListOfBranches().Clear()
        self._enabled_branches = {}
        self._missing_branches = []
        return


    def reset (self):
        for t in self._transientobjs.values():
            t[0] = None
        rr = self._resetfuncs
        if not rr: return
        self._resetfuncs = []
        for r in rr:
            r()
        return


    def add_reset (self, func):
        self._resetfuncs.append (func)
        return


    def add_child (self, child):
        self._children.append (child)
        self.add_reset (child.reset)
        return


    # Return a function that when called will evaluate to attribute N.
    @classmethod
    def getfunc (cl, n):
        return getattr (cl, n).__get__


    def test_branches (self, tree):
        for b in tree.GetListOfBranches():
            name = b.GetName()
            if (not self._allbranches.has_key (name) and
                not self._allbranches.has_key (tree.GetName() + '.' + name)):
                print (name)
            elif (self.tree.GetBranch(name).GetTree().GetName() !=
                  tree.GetName()):
                if not self._allbranches.has_key (tree.GetName() + '.' + name):
                    print (name)
        return


    # Remove cycles; allows the tree to be freed.
    def cleanup (self, noclose = False):
        self.disable()
        if not self.tree: return
        dir = self.tree.GetDirectory()
        if not noclose and dir and hasattr (dir, 'Close'):
            #cache=dir.GetCacheRead()
            #dir.SetCacheRead(None)
            #if cache:
            #    cache.Delete()
            
            dir.Close()
        Treewrapper.trees = [t for t in Treewrapper.trees
                             if t() != self]
        vars = list(self.__class__.__dict__.keys())
        for k in vars:
            if not k.startswith ('_'):
                delattr (self.__class__, k)

        vars = list(self.__dict__.keys())
        for k in vars:
            if not k.startswith ('_'):
                delattr (self, k)

        self._cleaned = True
        return


    def notify (self):
        for f in self._notifyfuncs: f(self)
        return

    def add_notify (self, f):
        self._notifyfuncs.append (f)
        return

    # Given a branch name BNAME, return the corresponding buffer,
    # enabling the branch in the process.
    def get_buffer (self, bname):
        desc = self._allbranches[bname]
        desc.enable_branch()
        return desc.buf


def make_wrapper (tree, selarg = None, cachesize=0, nlearn=0):
    tree.SetCacheSize (cachesize)
    #ROOT.zzllqq_ana12.setTreeCacheSize (tree, cachesize)
    #tree.SetCacheLearnEntries (nlearn)

    def delete_cache():
        if not tree:
            return
        dir = tree.GetDirectory()
        if dir:
            cache=dir.GetCacheRead()
            dir.SetCacheRead(None)
            if cache:
                cache.Delete()
        return
    #atexit.register (delete_cache)

    class Treewrapper_Specific (Treewrapper):
        def __init__ (self, tree):
            Treewrapper.__init__ (self, tree, selarg)
            return
    return Treewrapper_Specific (tree)


#def pfilt (code):
#    if 'add_list' in code.co_names:
#        return False
#    return True
#psyco.setfilter (pfilt)
