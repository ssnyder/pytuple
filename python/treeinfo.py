class treeinfo:
    class _ti_type:
        def __init__ (self, rootcode, typecode, zero, eltname,
                      allow_missing = False,
                      use_vector = False,
                      selfcn = None,
                      namepat = None,
                      nameargs = ()):
            self._rootcode = rootcode
            self._typecode = typecode
            self._zero = zero
            self._eltname = eltname
            self._namepat = namepat
            self._nameargs = nameargs
            self._selfcn = selfcn
            self._allow_missing = allow_missing
            self._use_vector = use_vector
            return
        def rootcode (self):
            return self._rootcode
        def typecode (self):
            return self._typecode
        def allow_missing (self):
            return self._allow_missing
        def use_vector (self):
            return self._use_vector
        def zero (self, n):
            return n * [self._zero]

        def vecname (self):
            s = 'std::vector<' + self._eltname
            if s[-1] == '>':
                s += ' '
            s += '>'
            return s

        def __call__ (self,
                      namepat = None,
                      selfcn = None,
                      allow_missing = False,
                      use_vector = False,
                      *nameargs):
            return treeinfo._ti_type (self._rootcode,
                                      self._typecode,
                                      self._zero,
                                      self._eltname,
                                      allow_missing,
                                      use_vector,
                                      selfcn,
                                      namepat,
                                      nameargs)

        def branchname (self, rootname, leafname, is_DxAOD = False):
            if self._namepat:
                if callable (self._namepat):
                    return self._namepat (rootname, leafname, *self._nameargs)
                if self._namepat.find ('%') >= 0:
                    return self._namepat % rootname
                return self._namepat
            if len(rootname) > 0:
                if len(leafname) > 0:
                    if is_DxAOD:
                        return '%s%s.%s' % (rootname, is_DxAOD, leafname)
                    return '%s_%s' % (rootname, leafname)
                return rootname
            return leafname

        def select (self, selargs):
            if self._selfcn:
                return self._selfcn (selargs)
            return True
        
    int = _ti_type ('I', 'i', 0, 'int')
    short = _ti_type ('S', 'h', 0, 'short')
    unsigned_short = _ti_type ('s', 'H', 0, 'unsigned short')
    unsigned_int   = _ti_type ('i', 'I', 0, 'unsigned int')
    float = _ti_type ('F', 'f', 0.0, 'float')
    double = _ti_type ('D', 'd', 0.0, 'double')
    char = _ti_type ('B', 'b', 0, 'char')
    long = _ti_type ('L', 'l', 0, 'long')
    unsigned_long = _ti_type ('l', 'L', 0, 'unsigned long')


    class instance:
        def __init__ (self, cls, namepat = None, selfcn = None,
                      remap = {}, *nameargs):
            self._cls = cls
            self._namepat = namepat
            self._selfcn = selfcn
            self._nameargs = nameargs
            self._remap = remap
            return

        def prefix (self, rootname, prefname):
            if self._namepat:
                if callable (self._namepat):
                    return self._namepat (rootname, prefname, *self._nameargs)
                if self._namepat.find ('%') >= 0:
                    return self._namepat % rootname
                return self._namepat
            if len(rootname) > 0:
                if len(prefname) > 0:
                    return '%s_%s' % (rootname, prefname)
                return rootname
            return prefname
        
        def cls (self):
            return self._cls
        
        def select (self, selargs):
            if self._selfcn:
                return self._selfcn (selargs)
            return True

        def remap (self):
            return self._remap
