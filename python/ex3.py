# Example of reading ntuples using pytuple.

import ROOT
import PyCintex
import sys
sys.path.append ('.')

from pytuple.d3pd import open_d3pd
from PyAnalysisUtils.normphi import normphi
from math import *

# Open the DPD file.  Fill in the proper file name here.
tt = open_d3pd ('d3pd.root', treename = 'egamma')

# Helper to sort a list in descending pt order.
def ptsort(l):
    l.sort (key=lambda e:e.pt, reverse=1)
    return l

# Define `goode' to be a list of electrons that have isEM==0.
def goode (tree):
    return [o for o in tree.el if o.isEM==0]
tt.add_transient ('goode', lambda l: ptsort(goode(l)))

# Define `isojet' to be a list of jets that are separated from electrons.
def deltar (a, b):
    return sqrt((a.eta-b.eta)**2 + normphi(a.phi-b.phi)**2)
def mindr (a, l):
    mindr = 999
    for p in l:
        dr = deltar (a, p)
        if dr < mindr: mindr = dr
    return mindr
def goodj (tree):
    return [o for o in tree.jet if mindr (o, tree.goode) > 0.2]
tt.add_transient ('isojet', lambda l: ptsort(goodj(l)))

# Define a function to calculate transverse mass.
from math import *
def mt(a,b): return sqrt(2*a.pt*b.pt*(1-cos(a.phi-b.phi)))

# Set up to make interactive plots.
from PyAnalysisUtils.pydraw import cmdhook, cmd
cmdhook()

# Plot the invariant mass of the leading electron and jet.
# Normalize the plot to an area of 1.
cmd ("d tt.mt(goode$1,isojet$1); 50 0 400000 NORM")

# Plot the invariant mass of all jet pairs.
# Normalize the plot to an area of 1 and put on the same axis.
cmd ("d tt.(goode$i+goode$j).m if $i>$j; 50 0 400000 NORM SAME")




