#
# Define sub- (`view') events, with alternate definitions for some objects.
# Objects that depend on them are recomputed in the subevent.
# See example.
#

class Subevent (object):

    """Define sub- (`view') events, with alternate definitions for some objects.
    Objects that depend on them are recomputed in the subevent.

    Examples:
    >>> import ROOT
    >>> from pytuple.readtuple import make_wrapper
    >>> tre = ROOT.TTree ('tre', 'tre')
    >>> t = make_wrapper (tre)
    >>> def make_foo (tree, fxx):
    ...     print 'make_foo', fxx
    ...     return fxx+1
    >>> def make_bar (tree, foo):
    ...     print 'make_bar', foo
    ...     return foo+1
    >>> t.add_transient ('foo', make_foo, 'fxx')
    >>> t.add_transient ('foy', make_foo, 'fyy')
    >>> t.add_transient ('bar', make_bar, 'foo')
    >>> t.fxx = 1
    >>> t.fyy = 100
    >>> t.bar
    make_foo 1
    make_bar 2
    3
    >>> se = make_subevent (t, 'prime')
    >>> def make_fooprime (tree, fxx):
    ...     print 'make_fooprime', fxx
    ...     return fxx*10
    >>> se.add_transient ('foo', make_fooprime, 'fxx')
    >>> t.prime.bar
    make_fooprime 1
    make_bar 10
    11
    >>> t.prime.bar
    11
    >>> t.reset()
    >>> t.prime.bar
    make_fooprime 1
    make_bar 10
    11
    >>> (f, n, k) = se.get_transient ('foo')
    >>> print f.__name__, n, k
    make_fooprime ('fxx',) {}
    >>> (f, n, k) = se.get_transient ('foy')
    >>> print f.__name__, n, k
    make_foo ('fyy',) {}
    >>> t.prime.fxx
    1
    >>> t.prime.foy
    make_foo 100
    101
    >>> try:
    ...   t.prime.xxx
    ... except AttributeError:
    ...   print 'AttributeError'
    AttributeError
    >>> try:
    ...   t.prime._xxx
    ... except AttributeError:
    ...   print 'AttributeError'
    AttributeError
    >>> se.dependson ('foo')
    set(['bar'])
    >>> 
    >>> def make_bax (tree, foo):
    ...     print 'make_bax', foo
    ...     return foo+1
    >>> t.add_transient ('bax', make_bax, 'bar')
    >>> t.bax
    make_foo 1
    make_bar 2
    make_bax 3
    4
    >>> t.prime.bax
    make_bax 11
    12
    >>> t.add_transient ('boy', make_bax, 'foy')
    >>> t.boy
    make_bax 101
    102
    >>> t.prime.boy
    102
    >>>
    >>> se2 = make_subevent (t, 'prime2')
    >>> se2.add_transient ('foo', make_fooprime, 'fxx')
    >>> t.prime2.bax
    make_fooprime 1
    make_bar 10
    make_bax 11
    12
    >>>
    >>> se3 = make_subevent (se, 'pp')
    >>> t.add_transient ('boz', make_bax, 'boy')
    >>> t.add_transient ('bay', make_bax, 'bax')
    >>> t.reset()
"""    

    def __init__ (self, parent):
        self._parent = parent
        self._transientobjs = {}
        self._dependson = {}
        self._children = []
        parent.add_child (self)
        return

    def add_child (self, child):
        self._children.append (child)
        return

    def add_reset (self, x):
        return

    def __getattr__ (self, a):
        if a[0] != '_':
            return getattr (self._parent, a)
        raise AttributeError
    
    def add_transient (self, name, makefunc, *nameargs, **kw):

        for c in self._children:
            c.add_transient_if_depends (name, makefunc, *nameargs, **kw)

        no_propagate = False
        if 'no_propagate' in kw:
            no_propagate = kw['no_propagate']
            del kw['no_propagate']

        from_parent = False
        if 'from_parent' in kw:
            from_base = kw['from_parent']
            del kw['from_parent']
            
        if name in self._transientobjs and from_parent: return

        obj = [None, makefunc, nameargs, kw, from_parent]
        def get (tree, obj=obj, makefunc=makefunc):
            x = obj[0]
            if x == None:
                in_objs = [getattr(tree, name) for name in nameargs]
                x = makefunc (tree, *in_objs, **kw)
                obj[0] = x
            return x
        setattr (self.__class__, name, property (get))
        self._transientobjs[name] = obj
        for a in nameargs:
            self._dependson.setdefault (a, set()).add (name)

        if not no_propagate:
            for n in self.dependson (name):
                self._maybe_propagate (n)

        return


    def _maybe_propagate (self, name):
        if hasattr (self.__class__, name): return
        (makefunc, nameargs, kw) = self._parent.get_transient (name)
        self.add_transient (name, makefunc, no_propagate = False,
                            from_parent = True,
                            *nameargs, **kw)
        return


    def dependson (self, name):
        return self._dependson.get (name, set()) .\
               union (self._parent.dependson (name))


    def get_transient (self, name):
        obj = self._transientobjs.get (name)
        if obj:
            return (obj[1], obj[2], obj[3])
        return self._parent.get_transient (name)
    
    def reset (self):
        for t in self._transientobjs.values():
            t[0] = None
        for c in self._children:
            c.reset()
        self._parent.add_reset (self.reset)
        return


    # NAME was added as a transient in the parent.
    # We need to add it here if depends on anything that we've changed
    # in this subevent.
    def add_transient_if_depends (self, name, makefunc, *nameargs, **kw):
        for c in self._children:
            c.add_transient_if_depends (name, makefunc, *nameargs, **kw)
        if name in self._transientobjs: return

        for n in nameargs:
            if n in self._transientobjs:
                self.add_transient (name, makefunc, from_parent = True, *nameargs, **kw)
                return
        return


    # Return a function that when called will evaluate to attribute N.
    def getfunc (self, n):
        g = getattr (self.__class__, n, None)
        if g: return g.__get__
        return self._parent.getfunc (n)
        
        
def make_subevent (tree, name):
    cls = type ('Subevent_' + name, (Subevent,), {})
    se = cls (tree)
    setattr (tree, name, se)
    return se

