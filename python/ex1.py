# Example of reading ntuples using PyROOT.

import ROOT
import PyCintex

# Calling this speeds up PyROOT's accesses to TTree branches,
# and also sets up to automatically enable branches when they're referenced.
from RootUtils.PyROOTFixes import enable_tree_speedups
enable_tree_speedups()

# Read the tuple from the file.
# Fill in your actual file name here.
file = ROOT.TFile.Open ('d3pd.root')
tree = file.Get ('physics')

h1 = ROOT.TH1F ('h1', 'h1', 50, 0, 200000)

# Simple loop over the tuple, no performance tweaks.
def ex1a():
    h1.Reset()
    for i in range (tree.GetEntries()):
        tree.GetEntry(i)
        for e in tree.el_E[:tree.el_n]: h1.Fill (e)
    return



# Loop over the tuple, enabling only the branches being read.
def ex1b():
    h1.Reset()
    tree.SetBranchStatus('*', 0)
    # If you don't enable the speedup patches above, you need to explicitly
    # enable branches as shown below.  If they are enabled, then this
    # is done automatically when the branch is first referenced.
    #tree.SetBranchStatus('el_E', 1)
    #tree.SetBranchStatus('el_n', 1)
    for i in range (tree.GetEntries()):
        tree.GetEntry(i)
        for e in tree.el_E[:tree.el_n]: h1.Fill (e)
    return


# Loop over the tuple, reading branches explicitly.
def ex1c():
    h1.Reset()
    br = (tree.GetBranch ('el_E'), tree.GetBranch ('el_n'))
    for i in range (tree.GetEntries()):
        for b in br: b.GetEntry(i)
        for e in tree.el_E[:tree.el_n]: h1.Fill (e)
    return
