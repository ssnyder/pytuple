import ROOT
from pytuple.readtuple import make_wrapper
from pytuple.treeinfo import treeinfo
from pytuple import samplings
from pytuple.Fourvec import *
import string
import math



class Selarg:
    pass


def seltru (selarg):
    return selarg.have_truth


def seldec01 (selarg):
    return selarg.date >= 20091201


class Global (object):
    RunNumber = treeinfo.int
    EventNumber = treeinfo.int
    timestamp = treeinfo.int
    timestamp_ns = treeinfo.int
    lbn = treeinfo.int
    bcid = treeinfo.int
    detmask0 = treeinfo.int
    detmask1 = treeinfo.int


class egamma (Fourvec_NoM):
    author           = treeinfo.int
    isEM             = treeinfo.int
    convFlag         = treeinfo.int
    isConv           = treeinfo.int
    nConv            = treeinfo.int
    nSingleTrackConv = treeinfo.int
    nDoubleTrackConv = treeinfo.int
    loose            = treeinfo.int
    tight            = treeinfo.int
    tightIso         = treeinfo.int
    convanglematch   = treeinfo.int
    convtrackmatch   = treeinfo.int
    hasconv          = treeinfo.int

    # type           = treeinfo.int
    # origin         = treeinfo.int

    # class Trucls (Fourvec_PtEtaPhiE):
    #     matched     = treeinfo.int
    #     type        = treeinfo.int
    #     mothertype  = treeinfo.int
    #     barcode     = treeinfo.int


    Ethad          = treeinfo.float
    # EMShower: Hadronic variables
    # Transverse energy in the first had sampling
    Ethad1         = treeinfo.float
    # EMShower: Sampling 2 variables
    # Uncalibrated energy sum in 3X3 window.
    E233           = treeinfo.float
    # Uncalibrated energy sum in 3X7 window.
    E237           = treeinfo.float
    # Uncalibrated energy sum in 7X7 window.
    E277           = treeinfo.float
    reta           = treeinfo.float (selfcn = seldec01)
    rphi           = treeinfo.float (selfcn = seldec01)
    rphiallcalo           = treeinfo.float

    # E1 / E
    f1 = treeinfo.float
    # E1(3X1) / E
    f1core = treeinfo.float
    f3 = treeinfo.float
    # E3(3X3) / E
    f3core = treeinfo.float
    # Energy in strip at minimum between first and second maxima
    Emins1 = treeinfo.float
    fside = treeinfo.float
    Emax2 = treeinfo.float
    ws3 = treeinfo.float
    wstot = treeinfo.float
    # Lateral width in 3X5 window, with correction for particle impact point
    weta2 = treeinfo.float
    deltaEs = treeinfo.float
    deltaEmax2 = treeinfo.float
    emaxs1 = treeinfo.float

    # # Transverse isolation energy in cone with half-opening angle 0.2
    Etcone20 = treeinfo.float
    Etcone30 = treeinfo.float
    Etcone40 = treeinfo.float
    Etcone45 = treeinfo.float
    ptcone30 = treeinfo.float

    convvtxx              = treeinfo.float
    convvtxy              = treeinfo.float
    convvtxz              = treeinfo.float
    Rconv                 = treeinfo.float
    zconv                 = treeinfo.float
    convvtxchi2           = treeinfo.float
    pt1conv               = treeinfo.float
    pt2conv               = treeinfo.float
    ptconv                = treeinfo.float
    pzconv                = treeinfo.float
    convtrk1nBLHits       = treeinfo.int
    convtrk1nPixHits      = treeinfo.int
    convtrk1nSCTHits      = treeinfo.int
    convtrk1nTRTHits      = treeinfo.int
    convtrk2nBLHits       = treeinfo.int
    convtrk2nPixHits      = treeinfo.int
    convtrk2nSCTHits      = treeinfo.int
    convtrk2nTRTHits      = treeinfo.int

    L1_matched = treeinfo.int
    L1_dr = treeinfo.float
    L1_index = treeinfo.int
    L1_pt = treeinfo.float
    L1_Et = treeinfo.float
    L1_eta = treeinfo.float
    L1_phi = treeinfo.float
    L1_EMisol = treeinfo.float
    L1_hadisol = treeinfo.float
    L1_hadcore = treeinfo.float

    L2_matched = treeinfo.int
    L2_dr = treeinfo.float
    L2_index = treeinfo.int
    L2_E = treeinfo.float
    L2_Et = treeinfo.float
    L2_eta = treeinfo.float
    L2_phi = treeinfo.float
    L2_pt = treeinfo.float
    L2_Ethad1 = treeinfo.float
    L2_reta = treeinfo.float
    L2_Eratio = treeinfo.float

    EF_matched = treeinfo.int
    EF_dr = treeinfo.float
    EF_index = treeinfo.int
    EF_E = treeinfo.float
    EF_Et = treeinfo.float
    EF_eta = treeinfo.float
    EF_phi = treeinfo.float
    EF_pt = treeinfo.float

    EF_Ethad1 = treeinfo.float
    EF_Ethad = treeinfo.float
    EF_rphiallcalo = treeinfo.float
    EF_E233 = treeinfo.float
    EF_E237 = treeinfo.float
    EF_E277 = treeinfo.float
    EF_f1 = treeinfo.float
    EF_Emins1 = treeinfo.float
    EF_fside = treeinfo.float
    EF_Emax2 = treeinfo.float
    EF_ws3 = treeinfo.float
    EF_wstot = treeinfo.float
    EF_weta2 = treeinfo.float

    cl_E  = treeinfo.float
    cl_pt = treeinfo.float
    cl_eta = treeinfo.float
    cl_phi = treeinfo.float

    Es0 = treeinfo.float
    Es1 = treeinfo.float
    Es2 = treeinfo.float
    Es3 = treeinfo.float
    etas0 = treeinfo.float
    etas1 = treeinfo.float
    etas2 = treeinfo.float
    etas3 = treeinfo.float
    phis0 = treeinfo.float
    phis1 = treeinfo.float
    phis2 = treeinfo.float
    phis3 = treeinfo.float

    EtringnoisedR03sig2 = treeinfo.float
    EtringnoisedR03sig3 = treeinfo.float
    EtringnoisedR03sig4 = treeinfo.float
    isolationlikelihoodjets = treeinfo.float
    isolationlikelihoodhqelectrons = treeinfo.float

    neuralnet = treeinfo.float
    Hmatrix = treeinfo.float
    Hmatrix5 = treeinfo.float
    adaboost = treeinfo.float

    zvertex = treeinfo.float
    errz = treeinfo.float
    etap = treeinfo.float
    depth = treeinfo.float

    jet_matched = treeinfo.int
    jet_dr = treeinfo.float
    jet_eta = treeinfo.float
    jet_phi = treeinfo.float
    jet_pt = treeinfo.float
    jet_E = treeinfo.float
    jet_m = treeinfo.float
    # jet_imatchtruejet = treeinfo.int (selfcn = seldec01)
    # jet_truth_dr = treeinfo.float
    # jet_truth_eta = treeinfo.float
    # jet_truth_phi = treeinfo.float
    # jet_truth_pt = treeinfo.float
    # jet_truth_E = treeinfo.float
    # jet_truth_m = treeinfo.float
    # jet_truth_parton = treeinfo.int (selfcn = seldec01)

    L2_E237               = treeinfo.float
    L2_E277               = treeinfo.float
    L2_fside              = treeinfo.float
    L2_weta2              = treeinfo.float
    L2_Emaxs1             = treeinfo.float
    L2_Emax2              = treeinfo.float

    EF_zvertex        = treeinfo.float (selfcn = seldec01)
    EF_errz           = treeinfo.float (selfcn = seldec01)
    EF_Etcone20           = treeinfo.float
    EF_Etcone30           = treeinfo.float
    EF_Etcone40           = treeinfo.float
    EF_Etcone45           = treeinfo.float

    rawcl_Es0 = treeinfo.float
    rawcl_etas0 = treeinfo.float
    rawcl_phis0 = treeinfo.float
    rawcl_Es1 = treeinfo.float
    rawcl_etas1 = treeinfo.float
    rawcl_phis1 = treeinfo.float
    rawcl_Es2 = treeinfo.float
    rawcl_etas2 = treeinfo.float
    rawcl_phis2 = treeinfo.float
    rawcl_Es3 = treeinfo.float
    rawcl_etas3 = treeinfo.float
    rawcl_phis3 = treeinfo.float
    rawcl_E = treeinfo.float
    rawcl_pt = treeinfo.float
    rawcl_eta = treeinfo.float
    rawcl_phi = treeinfo.float

    isIso            = treeinfo.int

    Etcone40_pt_corrected = treeinfo.float
    Etcone40_ED_corrected = treeinfo.float
    Etcone40_corrected = treeinfo.float


class Ele (egamma):
    charge         = treeinfo.float
    medium         = treeinfo.int
    mediumIso      = treeinfo.int

    # class Trucls (egamma.Trucls):
    #     hasHardBrem = treeinfo.int
    # truth = treeinfo.instance (Trucls, selfcn = seltru)

    # Difference between track and shower posn, in interstrip-distance units
    pos7 = treeinfo.float
    firstEdens = treeinfo.float
    cellmaxfrac = treeinfo.float
    longitudinal = treeinfo.float
    secondlambda = treeinfo.float
    lateral = treeinfo.float
    secondR = treeinfo.float
    centerlambda = treeinfo.float
    etacorrmag = treeinfo.float
    deltaeta1 = treeinfo.float
    deltaeta2 = treeinfo.float
    deltaphi2 = treeinfo.float
    hastrack = treeinfo.int
    deltaphiRescaled = treeinfo.float
    expectHitInBLayer = treeinfo.int

    trackqoverp = treeinfo.float
    trackpt = treeinfo.float
    tracketa = treeinfo.float
    trackd0 = treeinfo.float
    trackz0 = treeinfo.float
    tracktheta = treeinfo.float
    trackphi = treeinfo.float
    nBLHits = treeinfo.int
    nPixHits = treeinfo.int
    nSCTHits = treeinfo.int
    nTRTHits = treeinfo.int
    nPixHoles = treeinfo.int
    nSCTHoles = treeinfo.int
    nTRTHighTHits = treeinfo.int
    nTRTOutliers = treeinfo.int
    nTRTHighTOutliers = treeinfo.int
    nBLSharedHits = treeinfo.int
    nPixSharedHits = treeinfo.int
    nSCTSharedHits = treeinfo.int
    nSiHits = treeinfo.int
    TRTHighTHitsRatio = treeinfo.float

    trackd0beam = treeinfo.float
    tracksigd0beam = treeinfo.float
    trackd0pv = treeinfo.float
    tracksigd0pv = treeinfo.float
    trackz0pv = treeinfo.float
    tracksigz0pv = treeinfo.float
    trackd0pvunbiased = treeinfo.float
    tracksigd0pvunbiased = treeinfo.float
    trackz0pvunbiased = treeinfo.float
    tracksigz0pvunbiased = treeinfo.float

    L2_charge = treeinfo.float
    L2_trackclusterdeta = treeinfo.float
    L2_trackclusterdphi = treeinfo.float
    L2_Etoverpt = treeinfo.float
    L2_trackpt = treeinfo.float
    L2_trackalgo = treeinfo.int

    EF_charge = treeinfo.float
    EF_nBLHits = treeinfo.int
    EF_nPixHits = treeinfo.int
    EF_nSCTHits = treeinfo.int
    EF_nSiHits = treeinfo.int
    EF_nTRTHits = treeinfo.int
    EF_nTRTHighTHits = treeinfo.int
    EF_TRTHighTHitsRatio = treeinfo.float
    EF_hastrack = treeinfo.int
    EF_tracketa = treeinfo.float
    EF_trackpt = treeinfo.float
    EF_trackd0 = treeinfo.float
    EF_trackz0 = treeinfo.float
    EF_tracktheta = treeinfo.float
    EF_trackphi = treeinfo.float
    EF_deltaeta1 = treeinfo.float
    EF_deltaphi2 = treeinfo.float
    EF_deltaeta2 = treeinfo.float
    EF_etacorrmag         = treeinfo.float
    EF_expectHitInBLayer  = treeinfo.int
    EF_trackqoverp  = treeinfo.float
    EF_pixeldEdx  = treeinfo.float
    

    electronweight = treeinfo.float
    electronbgweight = treeinfo.float
    softeweight = treeinfo.float
    softebgweight = treeinfo.float
    softeneuralnet = treeinfo.float
    trackfitchi2 = treeinfo.float
    trackfitndof = treeinfo.int
    vertx = treeinfo.float
    verty = treeinfo.float
    vertz = treeinfo.float
    breminvpt = treeinfo.float
    bremradius = treeinfo.float
    bremx = treeinfo.float
    bremclusterradius = treeinfo.float
    breminvpterr = treeinfo.float
    bremtrackauthor = treeinfo.int
    hasbrem = treeinfo.int
    bremdeltaqoverp = treeinfo.float
    bremmaterialtraversed = treeinfo.float
    refittedtrackqoverp = treeinfo.float
    refittedtrackd0 = treeinfo.float
    refittedtrackz0 = treeinfo.float
    refittedtracktheta = treeinfo.float
    refittedtrackphi = treeinfo.float

    L2_trackpt            = treeinfo.float
    L2_tracketa           = treeinfo.float
    L2_trackd0            = treeinfo.float
    L2_trackz0            = treeinfo.float
    L2_tracktheta         = treeinfo.float
    L2_trackphi           = treeinfo.float
    L2_tracketaatcalo     = treeinfo.float
    L2_trackphiatcalo     = treeinfo.float
    L2_errpt              = treeinfo.float
    L2_erreta             = treeinfo.float
    L2_errphi             = treeinfo.float


    trackcov_d0            = treeinfo.float
    trackcov_z0            = treeinfo.float
    trackcov_phi           = treeinfo.float
    trackcov_theta         = treeinfo.float
    trackcov_qoverp        = treeinfo.float
    trackcov_d0_z0          = treeinfo.float
    trackcov_z0_phi         = treeinfo.float
    trackcov_z0_theta       = treeinfo.float
    trackcov_z0_qoverp      = treeinfo.float
    trackcov_d0_phi         = treeinfo.float
    trackcov_d0_theta       = treeinfo.float
    trackcov_d0_qoverp      = treeinfo.float
    trackcov_phi_theta      = treeinfo.float
    trackcov_phi_qoverp     = treeinfo.float
    trackcov_theta_qoverp   = treeinfo.float
    refittedtrackcovd0    = treeinfo.float
    refittedtrackcovz0    = treeinfo.float
    refittedtrackcovphi   = treeinfo.float
    refittedtrackcovtheta = treeinfo.float
    refittedtrackcovqoverp = treeinfo.float
    refittedtrackcovd0z0  = treeinfo.float
    refittedtrackcovz0phi = treeinfo.float
    refittedtrackcovz0theta = treeinfo.float
    refittedtrackcovz0qoverp = treeinfo.float
    refittedtrackcovd0phi = treeinfo.float
    refittedtrackcovd0theta = treeinfo.float
    refittedtrackcovd0qoverp = treeinfo.float
    refittedtrackcovphitheta = treeinfo.float
    refittedtrackcovphiqoverp = treeinfo.float
    refittedtrackcovthetaqoverp = treeinfo.float

    #jetcone_n
    #jetcone_dr
    #jetcone_signedIP
    #jetcone_ptrel
    #jetcone_index
    #jetcone_E
    #jetcone_pt
    #jetcone_m
    #jetcone_eta
    #jetcone_phi

    pixeldEdx             = treeinfo.float
    eProbabilityComb      = treeinfo.float
    eProbabilityHT        = treeinfo.float
    eProbabilityToT       = treeinfo.float
    eProbabilityBrem      = treeinfo.float


    E_PreSamplerB = treeinfo.float
    E_EMB1 = treeinfo.float
    E_EMB2 = treeinfo.float
    E_EMB3 = treeinfo.float
    E_PreSamplerE = treeinfo.float
    E_EME1 = treeinfo.float
    E_EME2 = treeinfo.float
    E_EME3 = treeinfo.float
    E_HEC0 = treeinfo.float
    E_HEC1 = treeinfo.float
    E_HEC2 = treeinfo.float
    E_HEC3 = treeinfo.float
    E_TileBar0 = treeinfo.float
    E_TileBar1 = treeinfo.float
    E_TileBar2 = treeinfo.float
    E_TileGap1 = treeinfo.float
    E_TileGap2 = treeinfo.float
    E_TileGap3 = treeinfo.float
    E_TileExt0 = treeinfo.float
    E_TileExt1 = treeinfo.float
    E_TileExt2 = treeinfo.float
    E_FCAL0 = treeinfo.float
    E_FCAL1 = treeinfo.float
    E_FCAL2 = treeinfo.float




class Gam (egamma):
    # class Trucls (egamma.Trucls):
    #     isConv = treeinfo.int
    #     isBrem = treeinfo.int
    #     isFromHardProc = treeinfo.int
    #     isPhotonFromHardProc = treeinfo.int
    #     deltaRRefPhoton = treeinfo.float
    #     Rconv = treeinfo.float
    #     zconv = treeinfo.float
    # truth = treeinfo.instance (Trucls, selfcn = seltru)

    isRecovered = treeinfo.int

    E033 = treeinfo.float
    E132 = treeinfo.float
    E1152 = treeinfo.float


    loglikelihood = treeinfo.float
    photonweight = treeinfo.float
    photonbgweight = treeinfo.float

    topomatched = treeinfo.int
    topodr = treeinfo.float
    topoeta = treeinfo.float
    topopt = treeinfo.float
    topophi = treeinfo.float
    topoEMmatched = treeinfo.int
    topoEMdr = treeinfo.float
    topoEMeta = treeinfo.float
    topoEMphi = treeinfo.float
    topoEMpt = treeinfo.float
    topoEtcone20 = treeinfo.float
    topoEtcone40 = treeinfo.float
    topoEtcone60 = treeinfo.float

    trackIsol = treeinfo.float

    convIP = treeinfo.float
    convIPRev = treeinfo.float
    ptIsolationCone = treeinfo.float
    ptIsolationConePhAngle = treeinfo.float


class Vertex (object):
    px = treeinfo.float
    py = treeinfo.float
    pz = treeinfo.float
    E = treeinfo.float
    m = treeinfo.float

    x = treeinfo.float
    y = treeinfo.float
    z = treeinfo.float
    err_x = treeinfo.float
    err_y = treeinfo.float
    err_z = treeinfo.float

    chi2 = treeinfo.float
    sumPt = treeinfo.float
    ndof = treeinfo.int
    nTracks = treeinfo.int
    type = treeinfo.int


    # trk_n = treeinfo.int
    # trk_chi2 = treeinfo.float
    # trk_d0 = treeinfo.float
    # trk_z0 = treeinfo.float
    # trk_unbiased_d0 = treeinfo.float
    # trk_unbiased_z0 = treeinfo.float
    # trk_err_unbiased_d0 = treeinfo.float
    # trk_err_unbiased_z0 = treeinfo.float
    # trk_phi = treeinfo.float
    # trk_theta = treeinfo.float
    # trk_weight = treeinfo.float


class PassedL1 (object):
    MBTS_1 = treeinfo.int
    EM2 = treeinfo.int


def wrap_tree (tt):
    tt.add (Global)
    tt.add_list (Ele, 'el', 100, warnmissing = True)
    tt.add_list (Gam, 'ph', 100, warnmissing = True)
    tt.add_list (Vertex, 'vxp', 100, warnmissing = True)
    tt.add_list (Fourvec, 'jet', 100, warnmissing = True)
    tt.add (PassedL1, 'L1', warnmissing = True)
    return



# file can be either a string or a list of file names.
# if the latter, we make chains.
def open_d3pd (file, treename = 'physics',
               have_truth = False,
               date = 20091218):
    if type(file) == type(""):
        file = ROOT.TFile.Open (file)
        tree = file.Get (treename)
    else:
        tree = ROOT.TChain (treename)
        for f in file:
            tree.Add (f + "/" + treename)
        tree.GetEntry(0)

    selarg = Selarg()
    selarg.have_truth = have_truth
    selarg.date = date
    
    tt = make_wrapper (tree, selarg = selarg)
    tt.tree = tree
    wrap_tree (tt)
    return tt
            

