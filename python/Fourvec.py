from PyAnalysisUtils.normphi import normphi
from pytuple.treeinfo import treeinfo
from math import sqrt, cosh, hypot, atan2, cos, sin, atan, tan, exp, log, sinh

def asinh (x):
    return log (sqrt (x*x+1) + x)


class Fourvec_Base (object):
    PT_ETA_PHI_E=0
    X_Y_Z_E = 1
    PT_ETA_PHI_M = 2
    #PARTICLE = 3
    PT_PZ_PHI_E = 4

    ## for massless fourvecs
    #PT_ETA_PHI = 11
    #X_Y_Z = 12
    #PT_ETA_PHI = 13

    def get_p_T (self):
        return self.pt
    def set_p_T (self, px):
        self.pt = pt
    p_T = property (get_p_T, set_p_T)

    def get_e (self):
        return self.E
    def set_e (self, e):
        self.E = e
    e = property (get_e, set_e)


    def get_px (self):
        return self.pt*cos(self.phi)
    def set_px (self, px):
        py = self.py
        self.pt = hypot (px, py)
        if px == 0 and py == 0:
            self.phi = 0
        else:
            self.phi = atan2 (py, px)
        return

    def get_py (self):
        return self.pt*sin(self.phi)
    def set_py (self, py):
        px = self.px
        self.pt = hypot (px, py)
        if px == 0 and py == 0:
            self.phi = 0
        else:
            self.phi = atan2 (py, px)
        return

    def get_pz (self):
        return self.pt*sinh (self.eta)
    def set_pz (self, pz):
        pt = self.pt
        if pt == 0:
            if pz > 0: self.eta = 100
            else: self.eta = -100
        else:
            self.eta = asinh (pz / pt)
        return

    px = property (get_px, set_px)
    py = property (get_py, set_py)
    pz = property (get_pz, set_pz)


    @property
    def eta (self):
        pz = self.pz
        p = sqrt(self.px**2 + self.py**2 + pz**2)
        if p == 0: return 0
        if p == pz: return 999
        if p == -pz: return -999
        eta = 0.5 * log((p+pz) / (p-pz))
        if self.e < 0: eta = -eta
        return eta
    @property
    def phi (self):
        return atan2 (self.py, self.px)


    # rapidity
    @property
    def y (self):
        E = self.E
        pz = self.pz
        try:
            return 0.5 * log ((E+pz) / (E-pz))
        except:
            print ('In Fourvec.y: bad values; E, pz = ', E, pz)
            raise


    def mag3 (self):
        return self.pt * cosh(self.eta)

    def _set_PtEtaPhi_from_XYZ (self, x, y, z):
        pt  = hypot(x,y)
        self.pt = pt
        if x == 0 and y == 0:
            self.phi = 0
        else:
            self.phi = atan2 (y,x)

        if pt == 0 or abs(z/pt)>700:
            if z > 0: self.eta = 100
            else: self.eta = -100
        else:
            self.eta = asinh (z / pt)
        return


    def __add__ (self, other):
        return Fourvec (self.px + other.px,
                        self.py + other.py,
                        self.pz + other.pz,
                        self.E  + other.E,
                        Fourvec.X_Y_Z_E)

    def __sub__ (self, other):
        return Fourvec (self.px - other.px,
                        self.py - other.py,
                        self.pz - other.pz,
                        self.E  - other.E,
                        Fourvec.X_Y_Z_E)


class Fourvec_All (Fourvec_Base):
    E     = treeinfo.float
    pt    = treeinfo.float
    phi   = treeinfo.float
    eta   = treeinfo.float
    px    = treeinfo.float
    py    = treeinfo.float
    pz    = treeinfo.float
    m     = treeinfo.float


    def set_xyz (self, x, y, z):
        self.px = x
        self.py = y
        self.pz = z
        self._set_PtEtaPhi_from_XYZ (x, y, z)
        
        return


    def __imul__ (self, c):
        self.pt *= c
        self.E *= c
        self.px *= c
        self.py *= c
        self.pz *= c
        self.m *= c
        return self


class Fourvec_NoM_Mixin (object):
    E     = treeinfo.float
    pt    = treeinfo.float
    phi   = treeinfo.float
    eta   = treeinfo.float
    px    = treeinfo.float
    py    = treeinfo.float
    pz    = treeinfo.float


    def get_m (self):
        mm = self.E**2 - self.px**2 - self.py**2 - self.pz**2
        if mm < 0: return -sqrt(-mm)
        return sqrt(mm)
    m = property (get_m)

    def set_xyz (self, x, y, z):
        self.px = x
        self.py = y
        self.pz = z
        self._set_PtEtaPhi_from_XYZ (x, y, z)
        
        return


class Fourvec_NoM (Fourvec_Base, Fourvec_NoM_Mixin):
    pass


class Fourvec_PtEtaPhi_Base (Fourvec_Base):
    pt       = treeinfo.float
    eta      = treeinfo.float
    phi      = treeinfo.float



    def set_xyz (self, x, y, z):
        return self._set_PtEtaPhi_from_XYZ (x, y, z)


class Fourvec_PtEtaPhiM (Fourvec_PtEtaPhi_Base):
    m = treeinfo.float

    @property
    def E (self):
        return hypot (self.m, self.mag3())


class Fourvec_PtEtaPhiE (Fourvec_PtEtaPhi_Base):
    E = treeinfo.float

    def __init__ (self, a=0, b=0, c=0, d=0,
                  inittype = Fourvec_Base.PT_ETA_PHI_E):
        if isinstance (a, Fourvec_Base):
            self.pt = a.pt
            self.eta = a.eta
            self.phi = a.phi
            self.E = a.E
        elif inittype == Fourvec_Base.PT_ETA_PHI_E:
            self.pt  = a
            self.eta = b
            self.phi = c
            self.E = d
        elif inittype == Fourvec_Base.PT_ETA_PHI_M:
            self.pt  = a
            self.eta = max(min(b,50),-50)
            self.phi = c
            p = a*cosh(self.eta)
            self.E = sqrt(p*p + d*d)
        elif inittype == Fourvec_Base.PT_PZ_PHI_E:
            self.pt  = a
            self.eta = asinh (b/a)
            self.phi = c
            self.E = d
        elif inittype == Fourvec_Base.X_Y_Z_E:
            self.set_xyze (a, b, c, d)
        else:
            raise "Bad init"
        return


    def set_xyze (self, x, y, z, e):
        self.set_xyz (x, y, z)
        self.E = e
        return
    def set_pt_eta_phi_e (self, pt, eta, phi, e):
        self.pt = pt
        self.eta = eta
        self.phi = phi
        self.E = e
        return
    def set_pt_eta_phi_m (self, pt, eta, phi, m):
        self.pt = pt
        self.eta = eta
        self.phi = phi
        p = a*cosh(eta)
        self.E = sqrt(p*p + m*m)
        return

    def get_m (self):
        p = self.pt * cosh(self.eta)
        e = self.E
        mm = e*e - p*p
        if mm < 0: return -sqrt(-mm)
        return sqrt(mm)
    def set_m (self, m):
        p = self.pt * cosh(self.eta)
        self.E = sqrt (p*p + m*m)
        return

    m = property (get_m, set_m)
        

    def __add__ (self, other):
        return Fourvec (self.px + other.px,
                        self.py + other.py,
                        self.pz + other.pz,
                        self.E  + other.E,
                        Fourvec.X_Y_Z_E)

    def __sub__ (self, other):
        return Fourvec (self.px - other.px,
                        self.py - other.py,
                        self.pz - other.pz,
                        self.E  - other.E,
                        Fourvec.X_Y_Z_E)

    def __imul__ (self, c):
        self.pt *= c
        self.E *= c
        return self



class Fourvec_PtEtaPhiEM (Fourvec_PtEtaPhi_Base):
    E = treeinfo.float
    m = treeinfo.float



class Fourvec_PxPyPzEM (Fourvec_Base):
    px = treeinfo.float
    py = treeinfo.float
    pz = treeinfo.float
    e = treeinfo.float
    m = treeinfo.float

    def get_E (self):
        return self.e
    def set_E (self, e):
        self.e = e
    E = property (get_E, set_E)

    def get_pt (self):
        return hypot (self.px, self.py)
    pt = property (get_pt)



# Default four-vector, for now.
Fourvec = Fourvec_PtEtaPhiE



def deltaR (a, b):
    return sqrt((a.eta - b.eta)**2 + normphi (a.phi - b.phi)**2)
def deltaR2 (a, b):
    return (a.eta - b.eta)**2 + normphi (a.phi - b.phi)**2
