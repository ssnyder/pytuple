#include "Python.h"
#include "TPython.h"
#include <vector>
#include <new>
#include <string>
#include <sstream>

#ifdef __GNUC__
// We get this warnings from Py_INCREF.
# pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif


namespace {


template <class T>
struct ttraits
{
  static const char* name();
  static const char* code();
  static PyObject* to_python (T x);
};


template <>
struct ttraits<double>
{
  static const char* name() { return "double"; }
  static const char* code() { return "d"; }
  static PyObject* to_python (double x) { return PyFloat_FromDouble (x); }
};

template <>
struct ttraits<float>
{
  static const char* name() { return "float"; }
  static const char* code() { return "f"; }
  static PyObject* to_python (float x) { return PyFloat_FromDouble (x); }
};


template <>
struct ttraits<int>
{
  static const char* name() { return "int"; }
  static const char* code() { return "i"; }
  static PyObject* to_python (int x) { return PyLong_FromLong (x); }
};

template <>
struct ttraits<unsigned int>
{
  static const char* name() { return "unsigned int"; }
  static const char* code() { return "I"; }
  static PyObject* to_python (unsigned int x) { return PyLong_FromLong(x);}
};


template <>
struct ttraits<short>
{
  static const char* name() { return "short"; }
  static const char* code() { return "h"; }
  static PyObject* to_python (short x) { return PyLong_FromLong (x); }
};

template <>
struct ttraits<unsigned short>
{
  static const char* name() { return "unsigned short"; }
  static const char* code() { return "H"; }
  static PyObject* to_python (unsigned short x) { return PyLong_FromLong(x);}
};


template <>
struct ttraits<long>
{
  static const char* name() { return "long"; }
  static const char* code() { return "l"; }
  static PyObject* to_python (long x) { return PyLong_FromLong (x); }
};

template <>
struct ttraits<unsigned long>
{
  static const char* name() { return "unsigned long"; }
  static const char* code() { return "L"; }
  static PyObject* to_python (unsigned long x) { return PyLong_FromLong(x);}
};


template <>
struct ttraits<char>
{
  static const char* name() { return "char"; }
  static const char* code() { return "b"; }
  static PyObject* to_python (char x) { return PyLong_FromLong (x); }
};

template <>
struct ttraits<unsigned char>
{
  static const char* name() { return "unsigned char"; }
  static const char* code() { return "B"; }
  static PyObject* to_python (unsigned char x) { return PyLong_FromLong(x);}
};


template <class T>
bool from_python (PyObject* obj, T& x)
{
  if (!PyArg_Parse (obj, (char*)ttraits<T>::code(), &x))
    return false;
  return true;
}


template <>
struct ttraits<bool>
{
  static const char* name() { return "bool"; }
  static const char* code() { return "@"; }
  static PyObject* to_python (bool x) { return PyBool_FromLong(x);}
};

bool from_python (PyObject* obj, bool& x)
{
  unsigned int tmp;
  if (!PyArg_Parse (obj, "I", &tmp))
    return false;
  x = (tmp != 0);
  return true;
}


class Vecop_Base
{
public:
  virtual ~Vecop_Base() {}
  virtual void dtor (void* p) const = 0;
  virtual void ctor (void* p, unsigned int n = 0) const = 0;
  virtual int size (void* p) const = 0;
  virtual PyObject* getitem (void* p, int i) const = 0;
  virtual int setitem (void* p, int i, PyObject* v) const = 0;
  virtual bool push_back (void* p, PyObject* v) const = 0;
  virtual PyObject* repr() const = 0;
  virtual void resize (void* p, unsigned int sz) = 0;
  virtual void reserve (void* p, unsigned int sz) = 0;
  virtual int itemsize() const = 0;
  virtual const char* classname() const = 0;
};


template <class T>
class Vecop
  : public Vecop_Base
{
public:
  typedef std::vector<T> vec_t;

  virtual void dtor (void* p) const;
  virtual void ctor (void* p, unsigned int n = 0) const;
  virtual int size(void* p) const;
  virtual PyObject* getitem (void* p, int i) const;
  virtual int setitem (void* p, int i, PyObject* v) const;
  virtual bool push_back (void* p, PyObject* v) const;
  virtual PyObject* repr() const;
  virtual int itemsize() const ;
  virtual void resize (void* p, unsigned int sz);
  virtual void reserve (void* p, unsigned int sz);
  virtual const char* classname() const;

  static Vecop_Base* instance();
};


template <class T>
void Vecop<T>::dtor (void* p) const
{
  reinterpret_cast<vec_t*>(p) -> ~vec_t();
}


template <class T>
void Vecop<T>::ctor (void* p, unsigned int n) const
{
  new (p) vec_t (n);
}


template <class T>
PyObject* Vecop<T>::repr() const
{
  static PyObject* pyname = 0;
  if (!pyname) {
    std::string name;
    name += "<vecbuf(";
    name += ttraits<T>::name();
    name += ")>";
    pyname = PyUnicode_FromString (name.c_str());
    Py_INCREF (pyname);
  }
  Py_INCREF (pyname);
  return pyname;
}


template <class T>
const char* Vecop<T>::classname() const
{
  static std::string name;
  if (name.empty()) {
    name = "std::vector<";
    name += ttraits<T>::name();
    name += ">";
  }
  return name.c_str();
}


template <class T>
int Vecop<T>::size (void* p) const
{
  return reinterpret_cast<vec_t*>(p)->size();
}


template <class T>
PyObject* Vecop<T>::getitem (void* p, int i) const
{
  vec_t* vec = reinterpret_cast<vec_t*> (p);
  if (i < 0 || i >= (int)vec->size()) {
    PyErr_SetString (PyExc_IndexError, "vector index out of range");
    return NULL;
  }

  return ttraits<T>::to_python ((*vec)[i]);
}


template <class T>
int Vecop<T>::setitem (void* p, int i, PyObject* v) const
{
  vec_t* vec = reinterpret_cast<vec_t*> (p);
  if (i < 0 || i >= (int)vec->size()) {
    PyErr_SetString (PyExc_IndexError, "vector index out of range");
    return -1;
  }

  T x;
  if (!from_python (v, x))
    return -1;
  (*vec)[i] = x;
  return 0;
}


template <class T>
bool Vecop<T>::push_back (void* p, PyObject* v) const
{
  vec_t* vec = reinterpret_cast<vec_t*> (p);
  T x;
  if (!from_python (v, x))
    return false;
  vec->push_back (x);
  return true;
}


template <class T>
int Vecop<T>::itemsize() const
{
  return sizeof (T);
}


template <class T>
void Vecop<T>::resize (void* p, unsigned int sz)
{
  reinterpret_cast<vec_t*>(p)->resize (sz);
}


template <class T>
void Vecop<T>::reserve (void* p, unsigned int sz)
{
  reinterpret_cast<vec_t*>(p)->reserve (sz);
}


template <class T>
Vecop_Base* Vecop<T>::instance ()
{
  static Vecop<T> op;
  return &op;
}


}


typedef struct {
  PyObject_HEAD
  Vecop_Base* m_ops;
  // Needs to be large enough to hold any std::vector specialization.
  // Watch out for vectr<bool>!
  unsigned char m_vec[6*sizeof(void*)];
  void* m_vecptr;

  PyObject* m_enable_cb; // xxx should be a weak ref
  bool m_enabled;
} vecbuf;


static void
vecbuf_dealloc (vecbuf* self)
{
  PyObject* err_type, *err_value, *err_traceback;
  int have_error = PyErr_Occurred() ? 1 : 0;
  if (have_error)
    PyErr_Fetch (&err_type, &err_value, &err_traceback);

  Py_XDECREF (self->m_enable_cb);

  if (have_error)
    PyErr_Fetch (&err_type, &err_value, &err_traceback);

  self->m_ops->dtor (&self->m_vec);
  Py_TYPE(self)->tp_free (self);
}

static PyObject*
vecbuf_new (PyTypeObject* type, PyObject* args, PyObject* /*kw*/)
{
  unsigned int n = 0;
  char typ;
#if PY_VERSION_HEX < 0x03000000
  if (!PyArg_ParseTuple (args, "c|I", &typ, &n))
#else
  if (!PyArg_ParseTuple (args, "C|I", &typ, &n))
#endif
    return 0;

  Vecop_Base* ops = 0;
  switch (typ) {
  case 'd':
    ops = Vecop<double>::instance();
    break;
  case 'f':
    ops = Vecop<float>::instance();
    break;
  case 'i':
    ops = Vecop<int>::instance();
    break;
  case 'u':
  case 'I':
    ops = Vecop<unsigned int>::instance();
    break;
  case 'h':
    ops = Vecop<short>::instance();
    break;
  case 'H':
    ops = Vecop<unsigned short>::instance();
    break;
  case 'l':
    ops = Vecop<long>::instance();
    break;
  case 'L':
    ops = Vecop<unsigned long>::instance();
    break;
  case 'b':
    ops = Vecop<char>::instance();
    break;
  case 'B':
    ops = Vecop<unsigned char>::instance();
    break;
  case '@':
    ops = Vecop<bool>::instance();
    break;
  }

  if (!ops) {
    PyErr_SetString (PyExc_TypeError, "Bad argument for vecbuf");
    return 0;
  }

  vecbuf* self = (vecbuf*)type->tp_alloc (type, 0);
  if (self) {
    self->m_ops = ops;
    ops->ctor (&self->m_vec, n);
    self->m_vecptr = &self->m_vec;
    self->m_enable_cb = 0;
    self->m_enabled = false;
  }
  return (PyObject*) self;
}


static PyObject*
vecbuf_repr (vecbuf* self)
{
  return self->m_ops->repr();
}


static bool
vecbuf_enable (vecbuf* self)
{
  self->m_enabled = true;
  if (self->m_enable_cb) {
    PyObject* nulltup = PyTuple_New (0);
    PyObject* ret = PyObject_Call (self->m_enable_cb, nulltup, 0);
    Py_DECREF (nulltup);
    if (!ret) return false;
    Py_DECREF(ret);
  }
  return true;
}


static long
vecbuf_length (vecbuf* self)
{
  return self->m_ops->size (&self->m_vec);
}


static PyObject*
vecbuf_item (vecbuf* self, Py_ssize_t i)
{
  if (!self->m_enabled) {
    if (!vecbuf_enable (self))
      return 0;
  }
  return self->m_ops->getitem (&self->m_vec, i);
}


static int
vecbuf_ass_item (vecbuf* self, Py_ssize_t i, PyObject* v)
{
  if (!self->m_enabled) {
    if (!vecbuf_enable (self))
      return 0;
  }
  return self->m_ops->setitem (&self->m_vec, i, v);
}


static
PyObject* vecbuf_resize (vecbuf* self, PyObject* pysize)
{
  unsigned int sz;
  if (!from_python (pysize, sz))
    return 0;
  self->m_ops->resize (&self->m_vec, sz);
  Py_INCREF (Py_None);
  return Py_None;
}


#if 0
static
PyObject* vecbuf_reserve (vecbuf* self, PyObject* pysize)
{
  unsigned int sz;
  if (!from_python (pysize, sz))
    return 0;
  self->m_ops->reserve (&self->m_vec, sz);
  Py_INCREF (Py_None);
  return Py_None;
}
#endif


static
PyObject* vecbuf_push_back (vecbuf* self, PyObject* obj)
{
  if (self->m_ops->push_back (&self->m_vec, obj)) {
    Py_INCREF (Py_None);
    return Py_None;
  }
  return 0;
}


static
PyObject* vecbuf_set_enable_cb (vecbuf* self, PyObject* obj)
{
  Py_XDECREF (self->m_enable_cb);
  self->m_enable_cb = obj;
  Py_XINCREF (self->m_enable_cb);
  Py_INCREF (Py_None);
  return Py_None;
}


static
PyObject* vecbuf_reset (vecbuf* self, PyObject* /*obj*/)
{
  self->m_enabled = false;
  return Py_None;
}


static
PyObject* vecbuf_as_vec (vecbuf* self, PyObject* /*obj*/)
{
#if ROOT_VERSION_CODE >= ROOT_VERSION(6,22,0)
  return TPython::CPPInstance_FromVoidPtr
#else
  return TPython::ObjectProxy_FromVoidPtr
#endif
    (self->m_vecptr,
     self->m_ops->classname());
}


static PyMethodDef vecbuf_methods[] = {
  {"resize", (PyCFunction)vecbuf_resize, METH_O, "resize"},
  {"reserve", (PyCFunction)vecbuf_resize, METH_O, "reserve"},
  {"push_back", (PyCFunction)vecbuf_push_back, METH_O, "push_back"},
  {"set_enable_cb", (PyCFunction)vecbuf_set_enable_cb, METH_O, "set_enable_cb"},
  {"reset", (PyCFunction)vecbuf_reset, METH_NOARGS, "reset"},
  {"as_vec", (PyCFunction)vecbuf_as_vec, METH_NOARGS, "as_vec"},
  {NULL, NULL, 0, NULL}
};

static PySequenceMethods vecbuf_as_sequence = {
  (lenfunc)vecbuf_length,                  /*sq_length*/
  0,/*(binaryfunc)array_concat,*/               /*sq_concat*/
  0,/*(intargfunc)array_repeat,*/               /*sq_repeat*/
  (ssizeargfunc)vecbuf_item,                 /*sq_item*/
  0,/*(intintargfunc)array_slice,*/             /*sq_slice*/
  (ssizeobjargproc)vecbuf_ass_item,          /*sq_ass_item*/
  0,/*(intintobjargproc)array_ass_slice,*/      /*sq_ass_slice*/
  0,/*(objobjproc)array_contains,*/             /*sq_contains*/
  0,/*(binaryfunc)array_inplace_concat,*/       /*sq_inplace_concat*/
  0,/*(intargfunc)array_inplace_repeat*/        /*sq_inplace_repeat*/
};


#if PY_VERSION_HEX < 0x03000000
static int
vecbuf_buffer_getreadbuf(vecbuf *self, long index, const void **ptr)
{
  if ( index != 0 ) {
    PyErr_SetString(PyExc_SystemError,
                    "Accessing non-existent vecbuf segment");
    return -1;
  }
#if 0
  *ptr = (void *)&self->m_vec;
  return sizeof(self->m_vec);
#endif
  *ptr = (void *)&self->m_vecptr;
  return sizeof(self->m_vecptr);
}


static int
vecbuf_buffer_getwritebuf(vecbuf *self, int index, const void **ptr)
{
  if ( index != 0 ) {
    PyErr_SetString(PyExc_SystemError,
                    "Accessing non-existent vecbuf segment");
    return -1;
  }
  *ptr = (void *)&self->m_vecptr;
  return sizeof(self->m_vecptr);
}


static int
vecbuf_buffer_getsegcount(vecbuf *self, int *lenp)
{
  if ( lenp )
    *lenp = self->m_ops->itemsize();
  return 1;
}


static PyBufferProcs vecbuf_as_buffer = {
  (readbufferproc)vecbuf_buffer_getreadbuf,   /* bf_getreadbuffer */
  (writebufferproc)vecbuf_buffer_getwritebuf, /* bf_getwritebuffer */
  (segcountproc)vecbuf_buffer_getsegcount,    /* bf_getsegcount */
  0,                                          /* bf_getcharbuffer */
  0,                                          /* bf_getbuffer */
  0,                                          /* bf_releasebuffer */
};
#endif


static PyTypeObject vecbuf_vecbufType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "vecbuf.vecbuf",              /*tp_name*/
    sizeof(vecbuf),              /*tp_basicsize*/
    0,                            /*tp_itemsize*/
    (destructor)vecbuf_dealloc,  /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    (reprfunc)vecbuf_repr,    /*tp_repr*/
    0,                         /*tp_as_number*/
    &vecbuf_as_sequence,       /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
#if PY_VERSION_HEX < 0x03000000
    &vecbuf_as_buffer,         /*tp_as_buffer*/
#else
    0,       /*tp_as_buffer*/
#endif
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,        /*tp_flags*/
    "std::vector buffer objects",           /* tp_doc */
    0,		               /* tp_traverse */
    0,		               /* tp_clear */
    0,		               /* tp_richcompare */
    0,		               /* tp_weaklistoffset */
    0,		               /* tp_iter */
    0,		               /* tp_iternext */
    vecbuf_methods,            /* tp_methods */
    0,                         /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    0,                         /* tp_init */
    0,                         /* tp_alloc */
    vecbuf_new,                /* tp_new */
    0,                         /* tp_free */
    0,                         /* tp_is_gc */
    0,                         /* tp_bases */
    0,                         /* tp_mro */
    0,                         /* tp_cache */
    0,                         /* tp_subclasses */
    0,                         /* tp_weaklist */
    0,                         /* tp_del */
    0                          /* tp_version_tag */
#ifdef Py_TPFLAGS_HAVE_FINALIZE
   ,0                          /* tp_finalize */
#ifdef _Py_TPFLAGS_HAVE_VECTORCALL
   ,0                          /* tp_vectorcall */
#if PY_VERSION_HEX < 0x03090000
    ,0                         /* tp_print */
#endif
#endif
#endif
#if PY_VERSION_HEX >= 0x030c0000
    ,0                         /* tp_watched */
#endif
};

/****************************************************************************/


typedef struct {
  PyObject_HEAD
  Py_ssize_t m_index;
  PyObject* m_bufobj;
} vecbufGetterSetter;


static void
vecbufGetterSetter_dealloc (vecbufGetterSetter* self)
{
  PyObject* err_type, *err_value, *err_traceback;
  int have_error = PyErr_Occurred() ? 1 : 0;
  if (have_error)
    PyErr_Fetch (&err_type, &err_value, &err_traceback);

  Py_DECREF (self->m_bufobj);
  if (have_error)
    PyErr_Restore (err_type, err_value, err_traceback);

  Py_TYPE(self)->tp_free (self);
}


static PyObject*
vecbufGetter_call (PyObject* self_in, PyObject* /*args*/, PyObject* /*kw*/)
{
  vecbufGetterSetter* self = reinterpret_cast<vecbufGetterSetter*> (self_in);
  vecbuf* bufobj = reinterpret_cast<vecbuf*> (self->m_bufobj);
  if (!bufobj->m_enabled) {
    if (!vecbuf_enable (bufobj))
      return 0;
  }
  return bufobj->m_ops->getitem (&bufobj->m_vec, self->m_index);
}


static PyObject*
vecbufSetter_call (PyObject* self_in, PyObject* args, PyObject* /*kw*/)
{
  if (PyTuple_Size (args) != 2) {
    PyErr_SetString (PyExc_TypeError, "bad args");
    return 0;
  }

  PyObject* arg = PyTuple_GetItem (args, 1);
  if (!arg) {
    PyErr_SetString (PyExc_TypeError, "bad args");
    return 0;
  }

  vecbufGetterSetter* self = reinterpret_cast<vecbufGetterSetter*> (self_in);
  vecbuf* bufobj = reinterpret_cast<vecbuf*> (self->m_bufobj);
  if (!bufobj->m_enabled) {
    if (!vecbuf_enable (bufobj))
      return 0;
  }
  bufobj->m_ops->setitem (&bufobj->m_vec, self->m_index, arg);
  Py_RETURN_NONE;
}


static PyObject*
vecbufGetterSetter_new (PyTypeObject* type, PyObject* args, PyObject* /*kw*/)
{
  Py_ssize_t index = 0;
  PyObject* buf;
  if (!PyArg_ParseTuple (args, "O!n", &vecbuf_vecbufType, &buf, &index))
    return 0;

  vecbufGetterSetter* self = (vecbufGetterSetter*)type->tp_alloc (type, 0);
  if (self) {
    self->m_bufobj = buf;
    self->m_index = index;
    Py_INCREF (buf);
  }
  return (PyObject*) self;
}


static PyObject*
vecbufGetterSetter_repr (vecbufGetterSetter* self, const char* typ)
{
  std::ostringstream os;
  os << "<vecbuf " << typ << "(" << self->m_index << ")>";
  PyObject* name = PyUnicode_FromString (os.str().c_str());
  Py_INCREF (name);
  return name;
}
static PyObject*
vecbufGetter_repr (vecbufGetterSetter* self)
{
  return vecbufGetterSetter_repr (self, "getter");
}
static PyObject*
vecbufSetter_repr (vecbufGetterSetter* self)
{
  return vecbufGetterSetter_repr (self, "setter");
}


static PyTypeObject vecbuf_vecbufGetterType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "vecbuf.vecbufGetter",       /*tp_name*/
    sizeof(vecbufGetterSetter),   /*tp_basicsize*/
    0,                            /*tp_itemsize*/
    (destructor)vecbufGetterSetter_dealloc,  /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    (reprfunc)vecbufGetter_repr,  /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    &vecbufGetter_call,        /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,        /*tp_flags*/
    "std::vector buffer getter objects",           /* tp_doc */
    0,		               /* tp_traverse */
    0,		               /* tp_clear */
    0,		               /* tp_richcompare */
    0,		               /* tp_weaklistoffset */
    0,		               /* tp_iter */
    0,		               /* tp_iternext */
    0,                         /* tp_methods */
    0,                         /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    0,                         /* tp_init */
    0,                         /* tp_alloc */
    vecbufGetterSetter_new,    /* tp_new */
    0,                         /* tp_free */
    0,                         /* tp_is_gc */
    0,                         /* tp_bases */
    0,                         /* tp_mro */
    0,                         /* tp_cache */
    0,                         /* tp_subclasses */
    0,                         /* tp_weaklist */
    0,                         /* tp_del */
    0                          /* tp_version_tag */
#ifdef Py_TPFLAGS_HAVE_FINALIZE
   ,0                          /* tp_finalize */
#ifdef _Py_TPFLAGS_HAVE_VECTORCALL
   ,0                          /* tp_vectorcall */
#if PY_VERSION_HEX < 0x03090000
    ,0                         /* tp_print */
#endif
#endif
#endif
#if PY_VERSION_HEX >= 0x030c0000
    ,0                         /* tp_watched */
#endif
};


static PyTypeObject vecbuf_vecbufSetterType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "vecbuf.vecbufSetter",       /*tp_name*/
    sizeof(vecbufGetterSetter),   /*tp_basicsize*/
    0,                            /*tp_itemsize*/
    (destructor)vecbufGetterSetter_dealloc,  /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    (reprfunc)vecbufSetter_repr,  /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    &vecbufSetter_call,        /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,        /*tp_flags*/
    "std::vector buffer setter objects",           /* tp_doc */
    0,		               /* tp_traverse */
    0,		               /* tp_clear */
    0,		               /* tp_richcompare */
    0,		               /* tp_weaklistoffset */
    0,		               /* tp_iter */
    0,		               /* tp_iternext */
    0,                         /* tp_methods */
    0,                         /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    0,                         /* tp_init */
    0,                         /* tp_alloc */
    vecbufGetterSetter_new,    /* tp_new */
    0,                         /* tp_free */
    0,                         /* tp_is_gc */
    0,                         /* tp_bases */
    0,                         /* tp_mro */
    0,                         /* tp_cache */
    0,                         /* tp_subclasses */
    0,                         /* tp_weaklist */
    0,                         /* tp_del */
    0                          /* tp_version_tag */
#ifdef Py_TPFLAGS_HAVE_FINALIZE
   ,0                          /* tp_finalize */
#ifdef _Py_TPFLAGS_HAVE_VECTORCALL
   ,0                          /* tp_vectorcall */
#if PY_VERSION_HEX < 0x03090000
    ,0                         /* tp_print */
#endif
#endif
#endif
#if PY_VERSION_HEX >= 0x030c0000
    ,0                         /* tp_watched */
#endif
};


/****************************************************************************/



#include "TH1.h"
#include "TH2.h"
#include "TProfile.h"
#include "TPython.h"


typedef struct {
  PyObject_HEAD
  TH1* m_hist;
} hist1Filler;


static PyObject*
hist1Filler_repr (hist1Filler* self)
{
  std::string name = "<hist1Filler ";
  name += self->m_hist->GetName();
  name += ">";
  return PyUnicode_FromString (name.c_str());
}



static PyObject*
hist1Filler_new (PyTypeObject* type, PyObject* args, PyObject* /*kw*/)
{
  if (PyTuple_Size (args) != 1) {
    PyErr_SetString (PyExc_TypeError, "bad args");
    return 0;
  }
  PyObject* arg = PyTuple_GetItem (args, 0);
  if (!arg) {
    PyErr_SetString (PyExc_TypeError, "bad args");
    return 0;
  }
  
#if ROOT_VERSION_CODE >= ROOT_VERSION(6,22,0)
  TObject* ptr = (TObject*)TPython::CPPInstance_AsVoidPtr (arg);
#else
  TObject* ptr = (TObject*)TPython::ObjectProxy_AsVoidPtr (arg);
#endif
  if (!ptr) {
    PyErr_SetString (PyExc_TypeError, "bad args");
    return 0;
  }
  TH1* hist = dynamic_cast<TH1*> (ptr);
  if (!hist) {
    PyErr_SetString (PyExc_TypeError, "bad args");
    return 0;
  }

  hist1Filler* self = (hist1Filler*)type->tp_alloc (type, 0);
  if (self) {
    self->m_hist = hist;
  }
  return (PyObject*) self;
}

static PyObject*
hist1Filler_call (PyObject* self_in, PyObject* args, PyObject* /*kw*/)
{
  hist1Filler* self = reinterpret_cast<hist1Filler*> (self_in);
  float val = 0;
  float weight = 1;
  if (!PyArg_ParseTuple (args, "f|f", &val, &weight))
    return 0;
  self->m_hist->Fill (val, weight);
  Py_RETURN_NONE;
}


static PyObject*
hist1Filler_fillList (hist1Filler* self, PyObject* args)
{
  PyObject* list;
  float weight = 1;
  if (!PyArg_ParseTuple (args, "O!|f", &PyList_Type, &list, &weight))
    return 0;
  Py_ssize_t sz = PyList_Size (list);
  for (Py_ssize_t i = 0; i < sz; i++) {
    PyObject* o = PyList_GetItem (list, i);
    double val = PyFloat_AsDouble (o);
    self->m_hist->Fill (val, weight);
  }
  Py_RETURN_NONE;
}


static PyMethodDef hist1Filler_methods[] = {
  {"fillList", (PyCFunction)hist1Filler_fillList, METH_VARARGS, "fillList"},
  {NULL, NULL, 0, NULL}
};

static PyTypeObject vecbuf_hist1FillerType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "vecbuf.hist1Filler",         /*tp_name*/
    sizeof(hist1Filler),           /*tp_basicsize*/
    0,                            /*tp_itemsize*/
    0,                         /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    (reprfunc)hist1Filler_repr,    /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    &hist1Filler_call,         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,        /*tp_flags*/
    "1D histogram filler",     /* tp_doc */
    0,		               /* tp_traverse */
    0,		               /* tp_clear */
    0,		               /* tp_richcompare */
    0,		               /* tp_weaklistoffset */
    0,		               /* tp_iter */
    0,		               /* tp_iternext */
    hist1Filler_methods,       /* tp_methods */
    0,                         /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    0,                         /* tp_init */
    0,                         /* tp_alloc */
    hist1Filler_new,           /* tp_new */
    0,                         /* tp_free */
    0,                         /* tp_is_gc */
    0,                         /* tp_bases */
    0,                         /* tp_mro */
    0,                         /* tp_cache */
    0,                         /* tp_subclasses */
    0,                         /* tp_weaklist */
    0,                         /* tp_del */
    0                          /* tp_version_tag */
#ifdef Py_TPFLAGS_HAVE_FINALIZE
   ,0                          /* tp_finalize */
#ifdef _Py_TPFLAGS_HAVE_VECTORCALL
   ,0                          /* tp_vectorcall */
#if PY_VERSION_HEX < 0x03090000
    ,0                         /* tp_print */
#endif
#endif
#endif
#if PY_VERSION_HEX >= 0x030c0000
    ,0                         /* tp_watched */
#endif
};


//****


typedef struct {
  PyObject_HEAD
  TProfile* m_hist;
} histProfFiller;


static PyObject*
histProfFiller_repr (histProfFiller* self)
{
  std::string name = "<histProfFiller ";
  name += self->m_hist->GetName();
  name += ">";
  return PyUnicode_FromString (name.c_str());
}



static PyObject*
histProfFiller_new (PyTypeObject* type, PyObject* args, PyObject* /*kw*/)
{
  if (PyTuple_Size (args) != 1) {
    PyErr_SetString (PyExc_TypeError, "bad args");
    return 0;
  }
  PyObject* arg = PyTuple_GetItem (args, 0);
  if (!arg) {
    PyErr_SetString (PyExc_TypeError, "bad args");
    return 0;
  }
  
#if ROOT_VERSION_CODE >= ROOT_VERSION(6,22,0)
  TObject* ptr = (TObject*)TPython::CPPInstance_AsVoidPtr (arg);
#else
  TObject* ptr = (TObject*)TPython::ObjectProxy_AsVoidPtr (arg);
#endif
  if (!ptr) {
    PyErr_SetString (PyExc_TypeError, "bad args");
    return 0;
  }
  TProfile* hist = dynamic_cast<TProfile*> (ptr);
  if (!hist) {
    PyErr_SetString (PyExc_TypeError, "bad args");
    return 0;
  }

  histProfFiller* self = (histProfFiller*)type->tp_alloc (type, 0);
  if (self) {
    self->m_hist = hist;
  }
  return (PyObject*) self;
}

static PyObject*
histProfFiller_call (PyObject* self_in, PyObject* args, PyObject* /*kw*/)
{
  histProfFiller* self = reinterpret_cast<histProfFiller*> (self_in);
  float val_x = 0;
  float val_y = 0;
  float weight = 1;
  if (!PyArg_ParseTuple (args, "ff|f", &val_x, &val_y, &weight))
    return 0;
  self->m_hist->Fill (val_x, val_y, weight);
  Py_RETURN_NONE;
}


static PyObject*
histProfFiller_fillList (histProfFiller* self, PyObject* args)
{
  PyObject* list;
  float weight = 1;
  if (!PyArg_ParseTuple (args, "O!|f", &PyList_Type, &list, &weight))
    return 0;
  Py_ssize_t sz = PyList_Size (list);
  for (Py_ssize_t i = 0; i < sz; i++) {
    PyObject* o = PyList_GetItem (list, i);
    if (PySequence_Size(o) < 2) {
      PyErr_SetString (PyExc_TypeError, "bad type");
      return 0;
    }
    PyObject* ox = PySequence_GetItem (o, 0);
    PyObject* oy = PySequence_GetItem (o, 1);
    self->m_hist->Fill (PyFloat_AsDouble(ox), PyFloat_AsDouble(oy), weight);
    Py_DECREF (ox);
    Py_DECREF (oy);
  }
  Py_RETURN_NONE;
}


static PyMethodDef histProfFiller_methods[] = {
  {"fillList", (PyCFunction)histProfFiller_fillList, METH_VARARGS, "fillList"},
  {NULL, NULL, 0, NULL}
};

static PyTypeObject vecbuf_histProfFillerType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "vecbuf.histProfFiller",         /*tp_name*/
    sizeof(histProfFiller),           /*tp_basicsize*/
    0,                            /*tp_itemsize*/
    0,                         /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    (reprfunc)histProfFiller_repr,    /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    &histProfFiller_call,         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,        /*tp_flags*/
    "Profile histogram filler",     /* tp_doc */
    0,		               /* tp_traverse */
    0,		               /* tp_clear */
    0,		               /* tp_richcompare */
    0,		               /* tp_weaklistoffset */
    0,		               /* tp_iter */
    0,		               /* tp_iternext */
    histProfFiller_methods,       /* tp_methods */
    0,                         /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    0,                         /* tp_init */
    0,                         /* tp_alloc */
    histProfFiller_new,           /* tp_new */
    0,                         /* tp_free */
    0,                         /* tp_is_gc */
    0,                         /* tp_bases */
    0,                         /* tp_mro */
    0,                         /* tp_cache */
    0,                         /* tp_subclasses */
    0,                         /* tp_weaklist */
    0,                         /* tp_del */
    0                          /* tp_version_tag */
#ifdef Py_TPFLAGS_HAVE_FINALIZE
   ,0                          /* tp_finalize */
#ifdef _Py_TPFLAGS_HAVE_VECTORCALL
   ,0                          /* tp_vectorcall */
#if PY_VERSION_HEX < 0x03090000
    ,0                         /* tp_print */
#endif
#endif
#endif
#if PY_VERSION_HEX >= 0x030c0000
    ,0                         /* tp_watched */
#endif
};


//****


typedef struct {
  PyObject_HEAD
  TH2* m_hist;
} hist2Filler;


static PyObject*
hist2Filler_repr (hist2Filler* self)
{
  std::string name = "<hist2Filler ";
  name += self->m_hist->GetName();
  name += ">";
  return PyUnicode_FromString (name.c_str());
}



static PyObject*
hist2Filler_new (PyTypeObject* type, PyObject* args, PyObject* /*kw*/)
{
  if (PyTuple_Size (args) != 1) {
    PyErr_SetString (PyExc_TypeError, "bad args");
    return 0;
  }
  PyObject* arg = PyTuple_GetItem (args, 0);
  if (!arg) {
    PyErr_SetString (PyExc_TypeError, "bad args");
    return 0;
  }
  
#if ROOT_VERSION_CODE >= ROOT_VERSION(6,22,0)
  TObject* ptr = (TObject*)TPython::CPPInstance_AsVoidPtr (arg);
#else
  TObject* ptr = (TObject*)TPython::ObjectProxy_AsVoidPtr (arg);
#endif
  if (!ptr) {
    PyErr_SetString (PyExc_TypeError, "bad args");
    return 0;
  }
  TH2* hist = dynamic_cast<TH2*> (ptr);
  if (!hist) {
    PyErr_SetString (PyExc_TypeError, "bad args");
    return 0;
  }

  hist2Filler* self = (hist2Filler*)type->tp_alloc (type, 0);
  if (self) {
    self->m_hist = hist;
  }
  return (PyObject*) self;
}

static PyObject*
hist2Filler_call (PyObject* self_in, PyObject* args, PyObject* /*kw*/)
{
  hist2Filler* self = reinterpret_cast<hist2Filler*> (self_in);
  float val_x = 0;
  float val_y = 0;
  float weight = 1;
  if (!PyArg_ParseTuple (args, "ff|f", &val_x, &val_y, &weight))
    return 0;
  self->m_hist->Fill (val_x, val_y, weight);
  Py_RETURN_NONE;
}


static PyObject*
hist2Filler_fillList (hist2Filler* self, PyObject* args)
{
  PyObject* list;
  float weight = 1;
  if (!PyArg_ParseTuple (args, "O!|f", &PyList_Type, &list, &weight))
    return 0;
  Py_ssize_t sz = PyList_Size (list);
  for (Py_ssize_t i = 0; i < sz; i++) {
    PyObject* o = PyList_GetItem (list, i);
    if (PySequence_Size(o) < 2) {
      PyErr_SetString (PyExc_TypeError, "bad type");
      return 0;
    }
    PyObject* ox = PySequence_GetItem (o, 0);
    PyObject* oy = PySequence_GetItem (o, 1);
    self->m_hist->Fill (PyFloat_AsDouble(ox), PyFloat_AsDouble(oy), weight);
    Py_DECREF (ox);
    Py_DECREF (oy);
  }
  Py_RETURN_NONE;
}


static PyMethodDef hist2Filler_methods[] = {
  {"fillList", (PyCFunction)hist2Filler_fillList, METH_VARARGS, "fillList"},
  {NULL, NULL, 0, NULL}
};

static PyTypeObject vecbuf_hist2FillerType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "vecbuf.hist2Filler",         /*tp_name*/
    sizeof(hist2Filler),           /*tp_basicsize*/
    0,                            /*tp_itemsize*/
    0,                         /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    (reprfunc)hist2Filler_repr,    /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    &hist2Filler_call,         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,        /*tp_flags*/
    "2D histogram filler",     /* tp_doc */
    0,		               /* tp_traverse */
    0,		               /* tp_clear */
    0,		               /* tp_richcompare */
    0,		               /* tp_weaklistoffset */
    0,		               /* tp_iter */
    0,		               /* tp_iternext */
    hist2Filler_methods,       /* tp_methods */
    0,                         /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    0,                         /* tp_init */
    0,                         /* tp_alloc */
    hist2Filler_new,           /* tp_new */
    0,                         /* tp_free */
    0,                         /* tp_is_gc */
    0,                         /* tp_bases */
    0,                         /* tp_mro */
    0,                         /* tp_cache */
    0,                         /* tp_subclasses */
    0,                         /* tp_weaklist */
    0,                         /* tp_del */
    0                          /* tp_version_tag */
#ifdef Py_TPFLAGS_HAVE_FINALIZE
   ,0                          /* tp_finalize */
#ifdef _Py_TPFLAGS_HAVE_VECTORCALL
   ,0                          /* tp_vectorcall */
#if PY_VERSION_HEX < 0x03090000
    ,0                         /* tp_print */
#endif
#endif
#endif
#if PY_VERSION_HEX >= 0x030c0000
    ,0                         /* tp_watched */
#endif
};


//***************************************************************************


static
PyObject* munge_consts (PyObject* /*self*/, PyObject* args)
{
  PyCodeObject* code;
  PyObject* consts;
  if (!PyArg_ParseTuple (args, "O!O!",
                         &PyCode_Type, &code,
                         &PyTuple_Type, &consts))
    return NULL;

  Py_DECREF (code->co_consts);
  Py_INCREF(consts);
  code->co_consts = consts;

  Py_RETURN_NONE;
}


static PyMethodDef vecbuf_module_methods[] = {
  {"munge_consts", munge_consts, METH_VARARGS, ""},
  {NULL, NULL, 0, NULL}
};


#if PY_VERSION_HEX >= 0x03000000
static struct PyModuleDef vecbufmodule = {
        PyModuleDef_HEAD_INIT,
        "vecbuf",  // name
        "std::vector buffer object module",      // doc
        -1,                    // size
        vecbuf_module_methods,        // methods
        NULL,  // slots
        NULL,  // traverse
        NULL,  // clear
        NULL   // free
};
#endif


PyMODINIT_FUNC
#if PY_VERSION_HEX < 0x03000000
initvecbuf()
#else
PyInit_vecbuf()
#endif
{
#if PY_VERSION_HEX < 0x03000000
# define RETURN return
#else
# define RETURN return m
#endif

  PyObject* m = nullptr;
  if (PyType_Ready (&vecbuf_vecbufType) < 0)
    RETURN;
  if (PyType_Ready (&vecbuf_vecbufGetterType) < 0)
    RETURN;
  if (PyType_Ready (&vecbuf_vecbufSetterType) < 0)
    RETURN;
  if (PyType_Ready (&vecbuf_hist1FillerType) < 0)
    RETURN;
  if (PyType_Ready (&vecbuf_hist2FillerType) < 0)
    RETURN;
  if (PyType_Ready (&vecbuf_histProfFillerType) < 0)
    RETURN;

#if PY_VERSION_HEX < 0x03000000
  m = Py_InitModule3 ("vecbuf", vecbuf_module_methods,
                      "std::vector buffer object module");
#else
  m = PyModule_Create (&vecbufmodule);
#endif
  Py_INCREF (&vecbuf_vecbufType);
  Py_INCREF (&vecbuf_hist1FillerType);
  PyModule_AddObject (m, "vecbuf", (PyObject*)&vecbuf_vecbufType);
  PyModule_AddObject (m, "vecbufGetter", (PyObject*)&vecbuf_vecbufGetterType);
  PyModule_AddObject (m, "vecbufSetter", (PyObject*)&vecbuf_vecbufSetterType);
  PyModule_AddObject (m, "hist1Filler", (PyObject*)&vecbuf_hist1FillerType);
  PyModule_AddObject (m, "hist2Filler", (PyObject*)&vecbuf_hist2FillerType);
  PyModule_AddObject (m, "histProfFiller", (PyObject*)&vecbuf_histProfFillerType);

  RETURN;
#undef RETURN
}
