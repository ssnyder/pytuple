# Example of using Treereader.

import ROOT
import PyCintex
import sys
sys.path.append ('.')

# Calling this speeds up PyROOT's accesses to TTree branches,
# and also sets up to automatically enable branches when they're referenced.
from RootUtils.PyROOTFixes import enable_tree_speedups
enable_tree_speedups()

# Read the tuple from the file.
# Fill in your actual file name here.
file = ROOT.TFile.Open ('d3pd.root')
tree = file.Get ('physics')

from pytuple.Treereader import treereader

# Set up some readers.
reader = treereader (tree)
el_reader = reader.subreader ('el_%s')
mu_reader = reader.subreader ('mu_%s')

h_el = ROOT.TH1F ('h_el', 'h_el', 50, 0, 200000)
h_mu = ROOT.TH1F ('h_mu', 'h_mu', 50, 0, 200000)

# Plot the E's of particles described by reader to h.
def eplot (h, reader):
    for k in range (reader.n):
        h.Fill (reader.E[k])
    return

# Loop over events.
def doit():
    tree.SetBranchStatus('*', 0)
    for i in range(tree.GetEntries()):
        reader.GetEntry (i)
        eplot (h_el, el_reader)
        eplot (h_mu, mu_reader)
    return
