import six


class Treereader (object):
    if six.PY2:
        buftypes = (buffer,)
    else:
        buftypes = ()
    leaftypes = (float, complex) + six.integer_types + buftypes
    def __init__ (*self):
        raise Exception ("Use treereader() rather than creating Treereader directly.")

    def real_init (self, tree, parent = None, subpattern = None):
        self.parent = parent
        self.tree = tree
        self.subpattern = subpattern
        self._subreaders = []
        if self.subpattern == None:
            tree.SetNotify (self)
        return

    def __getattr__ (self, attr):
        if attr[0] == '_':
            raise AttributeError
        if self.subpattern:
            lname = self.subpattern % attr
        else:
            lname = attr
        ret = getattr (self.tree, lname)
        if (isinstance (ret, Treereader.leaftypes) or
            str(type(ret)) == "<type 'buffer'>"):
            def getter (self, tree = self.tree, lname = lname):
                return getattr (tree, lname)
            setattr (self.__class__, attr, property (getter))
        else:
            setattr (self, attr, ret)
        return ret
    
    def Notify (self):
        newdict = { 'tree' : self.tree,
                    'subpattern' : self.subpattern,
                    'parent' : self.parent,
                    '_subreaders' : self._subreaders}
        for (k, v) in self.__dict__.items():
            if isinstance (v, Treereader):
                newdict[k] = v
        self.__dict__ = newdict
        #print ('notify!', self.__dict__)
        for s in self._subreaders:
            s.Notify()
        return
    

    def subreader (self, subpattern):
        sub = treereader (self.tree, self, subpattern)
        self._subreaders.append (sub)
        return sub


    def GetEntry (self, i):
        return self.tree.GetEntry (i)


def treereader (tree, parent = None, subpattern = None):
    class Treereader_Specific (Treereader):
        def __init__ (self, tree, subpattern = None):
            Treereader.real_init (self, tree, parent, subpattern)
            return
    return Treereader_Specific (tree, subpattern)
    

#tr=treereader(tt)

#tr55=tr.subreader('cl_%s')
#tr35=tr.subreader('cl_%s_35')
